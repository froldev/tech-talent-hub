#---Symfony-And-Docker-Makefile---------------#
# License: MIT
#---------------------------------------------#

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE = $(DOCKER) compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#------------#

#---PHP--#
PHP = php
PHP_CONSOLE = $(PHP) bin/console
#------------#

#---SYMFONY--#
SYMFONY = symfony
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
SYMFONY_LINT = $(SYMFONY_CONSOLE) lint:
#------------#

#---COMPOSER-#
COMPOSER = composer
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
COMPOSER_DUMP_DEV = $(COMPOSER) dump-env dev
COMPOSER_DUMP_PROD = $(COMPOSER) dump-env prod
#------------#

#---YARN-----#
YARN = yarn
YARN_INSTALL = $(YARN) install --force
YARN_UPDATE = $(YARN) update
YARN_BUILD = $(YARN) run build
YARN_DEV = $(YARN) run dev
YARN_WATCH = $(YARN) run watch
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.2
PHPQA_RUN = $(DOCKER_RUN) --init --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#

GITLAB_CI=froldev/gitlabci-postgresql:v1.2
PROJECT_DIR=$(shell pwd)
#---------------------------------------------#

## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

## === 🐋  DOCKER ================================================
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)

bash: ## Open bash in php container.
	$(DOCKER) exec -it techtalenthub_php bash
#---------------------------------------------#

## === 🎛️  SYMFONY ===============================================
sf: ## List and Use All Symfony commands (make sf command="commande-name").
	$(SYMFONY_CONSOLE) $(command)

sf-start: ## Start symfony server.
	$(SYMFONY_SERVER_START)

sf-stop: ## Stop symfony server.
	$(SYMFONY_SERVER_STOP)

sf-cc: ## Clear symfony cache.
	$(DOCKER) sh -c "rm -rf var/cache"
	$(SYMFONY_CONSOLE) cache:clear
	$(DOCKER) sh -c "chmod -R 777 var/cache"

sf-log: ## Show symfony logs.
	$(SYMFONY) server:log

sf-dc: ## Create symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists

sf-dd: ## Drop symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:drop --if-exists --force

sf-su: ## Update symfony schema database.
	$(SYMFONY_CONSOLE) doctrine:schema:update --force

sf-mm: ## Make migrations.
	$(SYMFONY_CONSOLE) make:migration

sf-dmm: ## Migrate.
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction

sf-fixtures: ## Load fixtures.
	$(SYMFONY_CONSOLE) doctrine:fixtures:load --no-interaction

sf-fixtures-users: ## Load users fixtures.
	$(SYMFONY_CONSOLE) doctrine:fixtures:load --group=UserFixtures --no-interaction

sf-me: ## Make symfony entity
	$(SYMFONY_CONSOLE) make:entity

sf-mc: ## Make symfony controller
	$(SYMFONY_CONSOLE) make:controller

sf-mr: ## Make symfony repository
	$(SYMFONY_CONSOLE) make:repository

sf-mv: ## Make symfony validator
	$(SYMFONY_CONSOLE) make:validator

sf-crud: ## Make symfony crud
	$(SYMFONY_CONSOLE) make:crud

sf-mf: ## Make symfony Form
	$(SYMFONY_CONSOLE) make:form

sf-message: ## make message and handler.
	$(SYMFONY_CONSOLE) make:message

sf-consume: ## Consume messages.
	$(SYMFONY_CONSOLE) messenger:consume async

sf-failed: ## Show failed messages.
	$(SYMFONY_CONSOLE) messenger:failed:show

sf-perm-var: ## Fix permissions.
	chmod -R 777 var

sf-dump-env: ## Dump env.
	$(SYMFONY_CONSOLE) debug:dotenv

sf-dump-env-container: ## Dump Env container.
	$(SYMFONY_CONSOLE) debug:container --env-vars

sf-dump-routes: ## Dump routes.
	$(SYMFONY_CONSOLE) debug:router

sf-open: ## Open project in a browser.
	$(SYMFONY) open:local

sf-open-email: ## Open Email catcher.
	$(SYMFONY) open:local:webmail

sf-check-requirements: ## Check requirements.
	$(SYMFONY) check:requirements
#---------------------------------------------#

## === 🐋  COMMAND ================================================

sf-install: ## Command to install the project.
	$(PHP_CONSOLE) app:install

create-admin:
	$(PHP_CONSOLE) create:admin

import-all:
	$(MAKE) import-tags
	$(MAKE) import-departments
	$(MAKE) import-jobs
	$(MAKE) create-admin

import-tags:
	$(PHP_CONSOLE) import:tags

import-departments:
	$(PHP_CONSOLE) import:departments

import-jobs:
	$(PHP_CONSOLE) import:jobs

ft-analysis:
	$(PHP_CONSOLE) ft:analysis

ft-analysis-jobs:
	$(PHP_CONSOLE) ft:analysis-jobs

ft-analysis-partners:
	$(PHP_CONSOLE) ft:analysis-partners

ft-analysis-offers:
	$(PHP_CONSOLE) ft:analysis-offers

ft-all:
	$(PHP_CONSOLE) ft:jobs
	$(PHP_CONSOLE) ft:offers

ft-jobs:
	$(PHP_CONSOLE) ft:jobs

ft-offers:
	$(PHP_CONSOLE) ft:offers

#---------------------------------------------#

## === 🐋  TAILWIND ================================================

tailwind: ## Build tailwind watch.
	$(SYMFONY_CONSOLE) tailwind:build --watch
#---------------------------------------------#

## === 🐋  ASSETS ================================================

compile-assets: ## Build assets.
	$(PHP_CONSOLE) asset-map:compile
#---------------------------------------------#

## === 🐋  TAILWIND ================================================

sf-tailwind: ## Command to install the project.
	$(SYMFONY_CONSOLE) tailwind:build --watch
#---------------------------------------------#

## === 📦  COMPOSER ==============================================
composer-install: ## Install composer dependencies.
	$(COMPOSER_INSTALL)

composer-update: ## Update composer dependencies.
	$(COMPOSER_UPDATE)

composer-dump-dev: ## Dump dev env.
	$(COMPOSER_DUMP_DEV)

composer-dump-prod: ## Dump prod env.
	$(COMPOSER_DUMP_PROD)

composer-validate: ## Validate composer.json file.
	$(COMPOSER) validate

composer-validate-deep: ## Validate composer.json and composer.lock files in strict mode.
	$(COMPOSER) validate --strict --check-lock
#---------------------------------------------#

## === 📦  YARN ===================================================
yarn-install: ## Install npm dependencies.
	$(YARN_INSTALL)

yarn-update: ## Update npm dependencies.
	$(YARN_UPDATE)

yarn-build: ## Build assets.
	$(YARN_BUILD)

yarn-dev: ## Build assets in dev mode.
	$(YARN_DEV)

yarn-watch: ## Watch assets.
	$(YARN_WATCH)
#---------------------------------------------#

## === 🐛  PHPQA =================================================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) phpstan analyse ./src --level=3

qa-security-checker: ## Run security-checker.
	$(SYMFONY) security:check

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src

qa-lint-twigs: ## Lint twig files.
	$(SYMFONY_LINT)twig ./templates

qa-lint-yaml: ## Lint yaml files.
	$(SYMFONY_LINT)yaml ./config

qa-lint-container: ## Lint container.
	$(SYMFONY_LINT)container

qa-lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction

qa-audit: ## Run composer audit.
	$(COMPOSER) audit
#---------------------------------------------#

## === 🔎  TESTS =================================================
.PHONY: tests
tests: ## Run all tests
	$(MAKE) database-init-test
	$(PHP) bin/phpunit --testdox tests/Unit/
#$(PHP) bin/phpunit --testdox tests/E2E/

database-init-test: ## Init database for test
	$(SYMFONY_CONSOLE) d:d:d --force --if-exists --env=test
	$(SYMFONY_CONSOLE) d:d:c --env=test
	$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test
	$(SYMFONY_CONSOLE) d:f:l --no-interaction --env=test

unit-test: ## Run unit tests
	$(MAKE) database-init-test
	$(PHP) bin/phpunit --testdox tests/Unit/

functional-test: ## Run functional tests
	$(MAKE) database-init-test
	$(PHP) bin/phpunit --testdox tests/Functional/

# PANTHER_NO_HEADLESS=1 ./bin/phpunit --filter LikeTest --debug to debug with Chrome
# e2e-test: ## Run E2E tests
# 	$(MAKE) database-init-test
# 	$(PHP) bin/phpunit --testdox tests/E2E/

tests-coverage: ## Run tests with coverage.
	XDEBUG_MODE=coverage $(PHPUNIT) --coverage-html var/coverage
#---------------------------------------------#

## === ⭐  OTHERS =================================================
before-commit: qa-cs-fixer qa-phpstan qa-security-checker qa-phpcpd qa-lint-twigs qa-lint-yaml qa-lint-container qa-lint-schema tests ## Run before commit.

install: composer-install yarn-install yarn-build sf-perm-var sf-dd sf-dc sf-dmm sf-install sf-cc ## Install.

start: docker-up sf-start sf-open ## Start project.

stop: docker-stop sf-stop ## Stop project.

reset: ## Reset database.
	$(eval CONFIRM := $(shell read -p "Etes vous sûr(e) de vouloir tout remettre à zéro? [y/N] " CONFIRM && echo $${CONFIRM:-N}))
	@if [ "$(CONFIRM)" = "y" ]; then \
		$(MAKE) sf-dd; \
		$(MAKE) sf-dc; \
		$(MAKE) sf-dmm; \
		$(MAKE) import-all; \
	fi
.PHONY: reset

# ------------------------------------------------
# GITLAB CI TASKS
# ------------------------------------------------
# Helper function to run commands in Docker
define docker_run
	@timeout 300 docker run --rm -v $(PROJECT_DIR):/var/www -w /var/www $(GITLAB_CI) $(1)
endef

# ------------------------------------------------
# TASKS
# ------------------------------------------------
# Run PHPUnit
php-unit:
	@echo "Running PHPUnit Tests"
	$(call docker_run, ./vendor/bin/phpunit --testdox)
	$(call docker_run, ./vendor/bin/phpunit --coverage-html var/coverage/html --coverage-clover var/coverage/clover.xml)
	$(call docker_run, ./vendor/bin/php-coveralls -v --config .coveralls.yml)

# Run full CI pipeline
pre-commit: pre-check security-checker php-code-standard linter php-unit
	@echo "Running full CI pipeline"

# Run pre-checks
pre-check:
	@echo "Running GitLab CI Pre Check"
	$(call docker_run, composer install --no-scripts --no-progress --no-interaction)
	$(call docker_run, yarn install)

# Run security checks
security-checker:
	@echo "Running GitLab CI Security Checker"
	$(call docker_run, symfony check:requirements)
	$(call docker_run, ./bin/console about)
	$(call docker_run, ./bin/console assets:install --symlink --relative)
	$(call docker_run, composer audit)

# Run PHP code standards
php-code-standard:
	@echo "Running GitLab CI PHP Code Standards"
	$(call docker_run, ./vendor/bin/php-cs-fixer fix ./src)
	$(call docker_run, ./vendor/bin/phpstan analyse --memory-limit=1G)
	$(call docker_run, ./vendor/bin/phpcbf src/ --standard=PSR12 --ignore=./src/Kernel.php)
	$(call docker_run, ./vendor/bin/phpcs src/ --standard=PSR12 --ignore=./src/Kernel.php)

# Run linter
linter:
	@echo "Running GitLab CI Linter"
	$(call docker_run, ./bin/console lint:twig templates --env=prod)
	$(call docker_run, ./bin/console lint:yaml config --parse-tags)
	$(call docker_run, ./bin/console doctrine:schema:validate --skip-sync -vvv --no-interaction)

# Build Docker image
build-docker-image:
	@echo "Building Docker Image"
	@if [ "$(shell docker images -q $(GITLAB_CI))" ]; then \
		echo "Docker image already exists: $(GITLAB_CI)"; \
	else \
		docker pull $(GITLAB_CI); \
	fi

.PHONY: pre-check security-checker php-code-standard linter php-unit build-docker-image pre-commit
