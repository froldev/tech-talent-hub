<?php

namespace App\Tests\Unit;

use App\Entity\Offer;
use App\Entity\SoftSkill;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SoftSkillTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): SoftSkill
    {
        return (new SoftSkill())
            ->setName(self::TEST)
            ->setDescription(self::TEST);
    }

    public function testGetId(): void
    {
        $softSkill = $this->getEntity();
        $this->assertNull($softSkill->getId());
    }

    public function testValidEntity(): void
    {
        $softSkill = $this->getEntity();

        self::assertEquals(self::TEST, $softSkill->getName());
        self::assertEquals(self::TEST, $softSkill->getDescription());
    }

    public function testUnvalidEntity(): void
    {
        $softSkill = $this->getEntity();

        self::assertNotEquals(self::ERROR, $softSkill->getName());
        self::assertNotEquals(self::ERROR, $softSkill->getDescription());
    }

    public function testEmptyNameEntity(): void
    {
        $softSkill = (new SoftSkill())->setName('');

        self::assertEmpty($softSkill->getName());
    }

    public function testEmptyDescriptionEntity(): void
    {
        $softSkill = (new SoftSkill())->setDescription('');

        self::assertEmpty($softSkill->getDescription());
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
