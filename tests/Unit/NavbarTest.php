<?php

namespace App\Tests\Unit;

use App\Entity\Navbar;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NavbarTest extends KernelTestCase
{
    private const TEST  = "test";
    private const DATE  = "2021-01-01";
    private const TRUE  = true;
    private const ERROR = "Error";

    public function getEntity(): Navbar
    {
        return (new Navbar())
            ->setName(self::TEST)
            ->setPath(self::TEST)
            ->setPosition(1)
            ->setActive(self::TRUE)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $navbar = $this->getEntity();
        $this->assertNull($navbar->getId());
    }

    public function testValidEntity(): void
    {
        $navbar = $this->getEntity();

        self::assertEquals(self::TEST, $navbar->getName());
        self::assertEquals(self::TEST, $navbar->getPath());
        self::assertEquals(1, $navbar->getPosition());
        self::assertEquals(self::TRUE, $navbar->isActive());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $navbar->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $navbar = $this->getEntity();

        self::assertNotEquals(self::ERROR, $navbar->getName());
        self::assertNotEquals(self::ERROR, $navbar->getPath());
        self::assertNotEquals(2, $navbar->getPosition());
        self::assertNotEquals(false, $navbar->isActive());
        self::assertNotEquals(new \DateTimeImmutable('2021-01-02'), $navbar->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $navbar = (new Navbar())
            ->setName('')
            ->setPath('');

        self::assertEmpty($navbar->getName());
        self::assertEmpty($navbar->getPath());
    }
}
