<?php

namespace App\Tests\Unit;

use App\Entity\City;
use App\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DepartementTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): Department
    {
        return (new Department())
            ->setName(self::TEST)
            ->setCode(self::TEST);
    }

    public function testGetId(): void
    {
        $entity = $this->getEntity();
        $this->assertNull($entity->getId());
    }

    public function testValidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertEquals(self::TEST, $entity->getName());
        self::assertEquals(self::TEST, $entity->getCode());
    }

    public function testUnvalidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertNotEquals(self::ERROR, $entity->getName());
        self::assertNotEquals(self::ERROR, $entity->getCode());
    }

    public function testEmptyEntity(): void
    {
        $entity = (new Department())
            ->setName('')
            ->setCode('');

        self::assertEmpty($entity->getName());
        self::assertEmpty($entity->getCode());
    }

    public function testAddAndRemoveCities(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new City();
        $relation2 = new City();
        $entity->addCity($relation1);
        $entity->addCity($relation2);

        $this->assertCount(2, $entity->getCities());
        $this->assertTrue($entity->getCities()->contains($relation1));
        $this->assertTrue($entity->getCities()->contains($relation2));
        $entity->removeCity($relation1);
        $this->assertCount(1, $entity->getCities());
        $this->assertFalse($entity->getCities()->contains($relation1));
        $this->assertTrue($entity->getCities()->contains($relation2));
    }
}
