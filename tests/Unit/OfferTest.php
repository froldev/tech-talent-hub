<?php

namespace App\Tests\Unit;

use App\Entity\Formation;
use App\Entity\HardSkill;
use App\Entity\Language;
use App\Entity\Offer;
use App\Entity\OfferCandidacy;
use App\Entity\OfferLike;
use App\Entity\Partner;
use App\Entity\SoftSkill;
use App\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfferTest extends KernelTestCase
{
    private const TEST     = "test";
    private const ERROR    = 'error';
    private const TEST_URL = 'https://www.test.com';
    private const DATE     = "2021-01-01";

    public function getEntity(): Offer
    {
        $offer =  (new Offer())
            ->setChannel(self::TEST)
            ->setReference(self::TEST)
            ->setTitle(self::TEST)
            ->setCandidacyUrl(self::TEST_URL)
            ->setDescription(self::TEST)
            ->setContact(self::TEST)
            ->setContract(self::TEST)
            ->setContractType(self::TEST)
            ->setContractNature(self::TEST)
            ->setExperience(self::TEST)
            ->setSalary(self::TEST)
            ->setSalaryCommentary(self::TEST)
            ->setSalaryComplement(self::TEST)
            ->setSalaryOther(self::TEST)
            ->setWorkTravel(self::TEST)
            ->setQualification(self::TEST)
            ->setAlternation(true)
            ->setTelework(true)
            ->setActive(true)
            ->setValidation(true)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));

        return $offer;
    }

    public function testGetId(): void
    {
        $offer = new Offer();
        $this->assertNull($offer->getId());
    }

    public function testValidEntity(): void
    {
        $offer = $this->getEntity();

        self::assertEquals(self::TEST, $offer->getChannel());
        self::assertEquals(self::TEST, $offer->getReference());
        self::assertEquals(self::TEST, $offer->getTitle());
        self::assertEquals(self::TEST_URL, $offer->getCandidacyUrl());
        self::assertEquals(self::TEST, $offer->getDescription());
        self::assertEquals(self::TEST, $offer->getContact());
        self::assertEquals(self::TEST, $offer->getContract());
        self::assertEquals(self::TEST, $offer->getContractType());
        self::assertEquals(self::TEST, $offer->getContractNature());
        self::assertEquals(self::TEST, $offer->getExperience());
        self::assertEquals(self::TEST, $offer->getSalary());
        self::assertEquals(self::TEST, $offer->getSalaryCommentary());
        self::assertEquals(self::TEST, $offer->getSalaryComplement());
        self::assertEquals(self::TEST, $offer->getSalaryOther());
        self::assertEquals(self::TEST, $offer->getWorkTravel());
        self::assertEquals(self::TEST, $offer->getQualification());
        self::assertTrue($offer->isAlternation());
        self::assertTrue($offer->isTelework());
        self::assertTrue($offer->isActive());
        self::assertTrue($offer->isValidation());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $offer->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $offer = $this->getEntity();

        self::assertNotEquals(self::ERROR, $offer->getChannel());
        self::assertNotEquals(self::ERROR, $offer->getReference());
        self::assertNotEquals(self::ERROR, $offer->getTitle());
        self::assertNotEquals('https://www.test2.com', $offer->getCandidacyUrl());
        self::assertNotEquals(self::ERROR, $offer->getDescription());
        self::assertNotEquals(self::ERROR, $offer->getContact());
        self::assertNotEquals(self::ERROR, $offer->getContract());
        self::assertNotEquals(self::ERROR, $offer->getContractType());
        self::assertNotEquals(self::ERROR, $offer->getContractNature());
        self::assertNotEquals(self::ERROR, $offer->getExperience());
        self::assertNotEquals(self::ERROR, $offer->getSalary());
        self::assertNotEquals(self::ERROR, $offer->getSalaryCommentary());
        self::assertNotEquals(self::ERROR, $offer->getSalaryComplement());
        self::assertNotEquals(self::ERROR, $offer->getSalaryOther());
        self::assertNotEquals(self::ERROR, $offer->getWorkTravel());
        self::assertNotEquals(self::ERROR, $offer->getQualification());
        self::assertNotFalse($offer->isAlternation());
        self::assertNotFalse($offer->isTelework());
        self::assertNotFalse($offer->isActive());
        self::assertNotFalse($offer->isValidation());
        self::assertNotEquals(new \DateTimeImmutable('2021-02-01'), $offer->getCreatedAt());
    }

    public function testEmptyEntity()
    {
        $offer = (new Offer())
            ->setChannel('')
            ->setTitle('')
            ->setCandidacyUrl('')
            ->setDescription('')
            ->setContact('')
            ->setContract('')
            ->setContractType('')
            ->setExperience('')
            ->setSalary('')
            ->setSalaryCommentary('')
            ->setSalaryComplement('')
            ->setSalaryOther('')
            ->setWorkTravel('')
            ->setQualification('')
            ->setAlternation('')
            ->setTelework('')
            ->setActive('')
            ->setValidation('');

        self::assertEmpty($offer->getChannel());
        self::assertEmpty($offer->getTitle());
        self::assertEmpty($offer->getCandidacyUrl());
        self::assertEmpty($offer->getDescription());
        self::assertEmpty($offer->getContact());
        self::assertEmpty($offer->getContract());
        self::assertEmpty($offer->getContractType());
        self::assertEmpty($offer->getExperience());
        self::assertEmpty($offer->getSalary());
        self::assertEmpty($offer->getSalaryCommentary());
        self::assertEmpty($offer->getSalaryComplement());
        self::assertEmpty($offer->getSalaryOther());
        self::assertEmpty($offer->getWorkTravel());
        self::assertEmpty($offer->getQualification());
        self::assertEmpty($offer->isAlternation());
        self::assertEmpty($offer->isTelework());
        self::assertEmpty($offer->isActive());
        self::assertEmpty($offer->isValidation());
    }

    public function testAddAndRemoveOfferLikes(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new OfferLike();
        $relation2 = new OfferLike();
        $entity->addOfferLike($relation1);
        $entity->addOfferLike($relation2);

        $this->assertCount(2, $entity->getOfferLikes());
        $this->assertTrue($entity->getOfferLikes()->contains($relation1));
        $this->assertTrue($entity->getOfferLikes()->contains($relation2));
        $entity->removeOfferLike($relation1);
        $this->assertCount(1, $entity->getOfferLikes());
        $this->assertFalse($entity->getOfferLikes()->contains($relation1));
        $this->assertTrue($entity->getOfferLikes()->contains($relation2));
    }

    public function testAddAndRemoveTags(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Tag();
        $relation2 = new Tag();
        $entity->addTag($relation1);
        $entity->addTag($relation2);

        $this->assertCount(2, $entity->getTags());
        $this->assertTrue($entity->getTags()->contains($relation1));
        $this->assertTrue($entity->getTags()->contains($relation2));
        $entity->removeTag($relation1);
        $this->assertCount(1, $entity->getTags());
        $this->assertFalse($entity->getTags()->contains($relation1));
        $this->assertTrue($entity->getTags()->contains($relation2));
    }

    public function testAddAndRemoveOfferCandidacies(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new OfferCandidacy();
        $relation2 = new OfferCandidacy();
        $entity->addOfferCandidacy($relation1);
        $entity->addOfferCandidacy($relation2);

        $this->assertCount(2, $entity->getOfferCandidacy());
        $this->assertTrue($entity->getOfferCandidacy()->contains($relation1));
        $this->assertTrue($entity->getOfferCandidacy()->contains($relation2));
        $entity->removeOfferCandidacy($relation1);
        $this->assertCount(1, $entity->getOfferCandidacy());
        $this->assertFalse($entity->getOfferCandidacy()->contains($relation1));
        $this->assertTrue($entity->getOfferCandidacy()->contains($relation2));
    }

    public function testAddAndRemoveHardSkills(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new HardSkill();
        $relation2 = new HardSkill();
        $entity->addHardSkill($relation1);
        $entity->addHardSkill($relation2);

        $this->assertCount(2, $entity->getHardSkills());
        $this->assertTrue($entity->getHardSkills()->contains($relation1));
        $this->assertTrue($entity->getHardSkills()->contains($relation2));
        $entity->removeHardSkill($relation1);
        $this->assertCount(1, $entity->getHardSkills());
        $this->assertFalse($entity->getHardSkills()->contains($relation1));
        $this->assertTrue($entity->getHardSkills()->contains($relation2));
    }

    public function testAddAndRemoveSoftSkills(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new SoftSkill();
        $relation2 = new SoftSkill();
        $entity->addSoftSkill($relation1);
        $entity->addSoftSkill($relation2);

        $this->assertCount(2, $entity->getSoftSkills());
        $this->assertTrue($entity->getSoftSkills()->contains($relation1));
        $this->assertTrue($entity->getSoftSkills()->contains($relation2));
        $entity->removeSoftSkill($relation1);
        $this->assertCount(1, $entity->getSoftSkills());
        $this->assertFalse($entity->getSoftSkills()->contains($relation1));
        $this->assertTrue($entity->getSoftSkills()->contains($relation2));
    }

    public function testAddAndRemovePartners(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Partner();
        $relation2 = new Partner();
        $entity->addPartner($relation1);
        $entity->addPartner($relation2);

        $this->assertCount(2, $entity->getPartners());
        $this->assertTrue($entity->getPartners()->contains($relation1));
        $this->assertTrue($entity->getPartners()->contains($relation2));
        $entity->removePartner($relation1);
        $this->assertCount(1, $entity->getPartners());
        $this->assertFalse($entity->getPartners()->contains($relation1));
        $this->assertTrue($entity->getPartners()->contains($relation2));
    }

    public function testAddAndRemoveLanguages(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Language();
        $relation2 = new Language();
        $entity->addLanguage($relation1);
        $entity->addLanguage($relation2);

        $this->assertCount(2, $entity->getLanguages());
        $this->assertTrue($entity->getLanguages()->contains($relation1));
        $this->assertTrue($entity->getLanguages()->contains($relation2));
        $entity->removeLanguage($relation1);
        $this->assertCount(1, $entity->getLanguages());
        $this->assertFalse($entity->getLanguages()->contains($relation1));
        $this->assertTrue($entity->getLanguages()->contains($relation2));
    }

    public function testAddAndRemoveFormations(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Formation();
        $relation2 = new Formation();
        $entity->addFormation($relation1);
        $entity->addFormation($relation2);

        $this->assertCount(2, $entity->getFormations());
        $this->assertTrue($entity->getFormations()->contains($relation1));
        $this->assertTrue($entity->getFormations()->contains($relation2));
        $entity->removeFormation($relation1);
        $this->assertCount(1, $entity->getFormations());
        $this->assertFalse($entity->getFormations()->contains($relation1));
        $this->assertTrue($entity->getFormations()->contains($relation2));
    }
}
