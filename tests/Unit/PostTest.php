<?php

namespace App\Tests\Unit;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PostTest extends KernelTestCase
{
    private const TEST  = "test";
    private const TRUE  = true;
    private const DATE  = "2021-01-01";
    private const ERROR = "Error";
    private const FALSE = false;
    private const URL   = "https://techtalenthub.fredolive.fr/";

    public function getEntity(): Post
    {
        return (new Post())
            ->setType(self::TEST)
            ->setTitle(self::TEST)
            ->setContent(self::TEST)
            ->setImageName(self::TEST)
            ->setActive(self::TRUE)
            ->setUrl(self::URL)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $post = $this->getEntity();
        $this->assertNull($post->getId());
    }

    public function testValidEntity(): void
    {
        $post = $this->getEntity();

        self::assertEquals(self::TEST, $post->getType());
        self::assertEquals(self::TEST, $post->getTitle());
        self::assertEquals(self::TEST, $post->getContent());
        self::assertEquals(self::TEST, $post->getImageName());
        self::assertEquals(self::TRUE, $post->isActive());
        self::assertEquals(self::URL, $post->getUrl());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $post->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $job = $this->getEntity();

        self::assertNotEquals(self::ERROR, $job->getType());
        self::assertNotEquals(self::ERROR, $job->getTitle());
        self::assertNotEquals(self::ERROR, $job->getContent());
        self::assertNotEquals(self::ERROR, $job->getImageName());
        self::assertNotEquals(self::FALSE, $job->isActive());
        self::assertNotEquals(self::ERROR, $job->getUrl());
        self::assertNotEquals(new \DateTimeImmutable('2021-01-02'), $job->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $job = (new Post())
            ->setContent('')
            ->setType('')
            ->setTitle('')
            ->setImageName('')
            ->setActive(self::FALSE);

        self::assertEmpty($job->getContent());
        self::assertEmpty($job->getType());
        self::assertEmpty($job->getTitle());
        self::assertEmpty($job->getImageName());
        self::assertEmpty($job->isActive());
    }
}
