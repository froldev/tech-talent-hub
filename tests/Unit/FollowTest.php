<?php

namespace App\Tests\Unit;

use App\Entity\Follow;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FollowTest extends KernelTestCase
{
    private const COMMENT = "Ceci est un commentaire";
    private const DATE    = "2021-01-01";

    public function getEntity(): Follow
    {
        return (new Follow())
            ->setComment(self::COMMENT)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $follow = $this->getEntity();
        $this->assertNull($follow->getId());
    }

    public function testValidEntity(): void
    {
        $follow = $this->getEntity();

        self::assertEquals(self::COMMENT, $follow->getComment());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $follow->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $follow = $this->getEntity();

        self::assertNotEquals('Ceci est un autre commentaire', $follow->getComment());
        self::assertNotEquals(new \DateTimeImmutable('2021-02-01'), $follow->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $follow = (new Follow())
            ->setComment('');

        self::assertEmpty($follow->getComment());
    }
}
