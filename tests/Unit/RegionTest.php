<?php

namespace App\Tests\Unit;

use App\Entity\Department;
use App\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RegionTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): Region
    {
        return (new Region())
            ->setName(self::TEST)
            ->setCode(self::TEST);
    }

    public function testGetId(): void
    {
        $entity = $this->getEntity();
        $this->assertNull($entity->getId());
    }

    public function testValidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertEquals(self::TEST, $entity->getName());
        self::assertEquals(self::TEST, $entity->getCode());
    }

    public function testUnvalidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertNotEquals(self::ERROR, $entity->getName());
        self::assertNotEquals(self::ERROR, $entity->getCode());
    }

    public function testEmptyEntity(): void
    {
        $entity = (new Region())
            ->setName('')
            ->setCode('');

        self::assertEmpty($entity->getName());
        self::assertEmpty($entity->getCode());
    }

    public function testAddAndRemoveDepartements(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Department();
        $relation2 = new Department();
        $entity->addDepartment($relation1);
        $entity->addDepartment($relation2);

        $this->assertCount(2, $entity->getDepartments());
        $this->assertTrue($entity->getDepartments()->contains($relation1));
        $this->assertTrue($entity->getDepartments()->contains($relation2));
        $entity->removeDepartment($relation1);
        $this->assertCount(1, $entity->getDepartments());
        $this->assertFalse($entity->getDepartments()->contains($relation1));
        $this->assertTrue($entity->getDepartments()->contains($relation2));
    }
}
