<?php

namespace App\Tests\Unit;

use App\Entity\Company;
use App\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CompanyTest extends KernelTestCase
{
    private const TEST  = "test";
    private const DATE  = "2021-01-01";
    private const ERROR = "Error";

    public function getEntity(): Company
    {
        return (new Company())
            ->setName(self::TEST)
            ->setDescription(self::TEST)
            ->setActivitySector(self::TEST)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $company = $this->getEntity();
        $this->assertNull($company->getId());
    }

    public function testValidEntity(): void
    {
        $company = $this->getEntity();

        self::assertEquals(self::TEST, $company->getName());
        self::assertEquals(self::TEST, $company->getDescription());
        self::assertEquals(self::TEST, $company->getActivitySector());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $company->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $company = $this->getEntity();

        self::assertNotEquals(self::ERROR, $company->getName());
        self::assertNotEquals(self::ERROR, $company->getDescription());
        self::assertNotEquals(self::ERROR, $company->getActivitySector());
        self::assertNotEquals(new \DateTimeImmutable('2021-01-02'), $company->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $company = (new Company())
            ->setname('')
            ->setDescription('')
            ->setActivitySector('');

        self::assertEmpty($company->getName());
        self::assertEmpty($company->getDescription());
        self::assertEmpty($company->getActivitySector());
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
