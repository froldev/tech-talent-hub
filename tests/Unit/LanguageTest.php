<?php

namespace App\Tests\Unit;

use App\Entity\Language;
use App\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LanguageTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): Language
    {
        return (new Language())
            ->setName(self::TEST);
    }

    public function testGetId(): void
    {
        $language = $this->getEntity();
        $this->assertNull($language->getId());
    }

    public function testValidEntity(): void
    {
        $language = $this->getEntity();

        self::assertEquals(self::TEST, $language->getName());
    }

    public function testUnvalidEntity(): void
    {
        $language = $this->getEntity();

        self::assertNotEquals(self::ERROR, $language->getName());
    }

    public function testEmptyEntity(): void
    {
        $language = (new Language())
            ->setName('');

        self::assertEmpty($language->getName());
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
