<?php

namespace App\Tests\Unit;

use App\Entity\Follow;
use App\Entity\Offer;
use App\Entity\OfferCandidacy;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfferCandidacyTest extends KernelTestCase
{
    private const RESPONSE = "true";
    private const DATE     = "2021-01-01";

    public function getEntity(): OfferCandidacy
    {
        return (new OfferCandidacy())
            ->setResponse(self::RESPONSE)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $candidacy = $this->getEntity();
        $this->assertNull($candidacy->getId());
    }

    public function testValidEntity(): void
    {
        $candidacy = $this->getEntity();
        $user      = new User();
        $candidacy->setUserOfferCandidacy($user);

        self::assertEquals(self::RESPONSE, $candidacy->getResponse());
        self::assertEquals($user, $candidacy->getUserOfferCandidacy());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $candidacy->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $candidacy = $this->getEntity();
        $user      = new User();
        $candidacy->setUserOfferCandidacy($user);
        $newUser = new User();

        self::assertNotEquals('false', $candidacy->getResponse());
        // self::assertNotEquals($newUser, $candidacy->getUserOfferCandidacy());
        self::assertNotEquals(new \DateTimeImmutable('2021-02-01'), $candidacy->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $candidacy = (new OfferCandidacy())
            ->setResponse('');

        self::assertEmpty($candidacy->getResponse());
    }

    public function testAddAndRemoveFollows(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Follow();
        $relation2 = new Follow();
        $entity->addFollow($relation1);
        $entity->addFollow($relation2);

        $this->assertCount(2, $entity->getFollow());
        $this->assertTrue($entity->getFollow()->contains($relation1));
        $this->assertTrue($entity->getFollow()->contains($relation2));
        $entity->removeFollow($relation1);
        $this->assertCount(1, $entity->getFollow());
        $this->assertFalse($entity->getFollow()->contains($relation1));
        $this->assertTrue($entity->getFollow()->contains($relation2));
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
