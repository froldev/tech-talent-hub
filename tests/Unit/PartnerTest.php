<?php

namespace App\Tests\Unit;

use App\Entity\Offer;
use App\Entity\Partner;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PartnerTest extends KernelTestCase
{
    private const TEST        = "test";
    private const POSITION    = 1;
    private const ERROR       = "Error";
    private const URL         = 'https://www.test.com';
    private const COUNT       = 1;
    private const DESCRIPTION = "Description";

    public function getEntity(): Partner
    {
        return (new Partner())
            ->setName(self::TEST)
            ->setUrl(self::URL)
            ->setLogo(self::TEST)
            ->setDescription(self::DESCRIPTION)
            ->setActive(true)
            ->setPosition(self::POSITION)
            ->setCountOffer(self::COUNT)
            ->setSlug(self::TEST);
    }

    public function testGetId(): void
    {
        $partner = $this->getEntity();
        $this->assertNull($partner->getId());
    }

    public function testValidEntity(): void
    {
        $partner = $this->getEntity();

        self::assertEquals(self::TEST, $partner->getName());
        self::assertEquals(self::URL, $partner->getUrl());
        self::assertEquals(self::DESCRIPTION, $partner->getDescription());
        self::assertEquals(self::TEST, $partner->getLogo());
        self::assertTrue($partner->isActive());
        self::assertEquals(self::POSITION, $partner->getPosition());
        self::assertEquals(self::COUNT, $partner->getCountOffer());
        self::assertEquals(self::TEST, $partner->getSlug());
    }

    public function testUnvalidEntity(): void
    {
        $partner = $this->getEntity();

        self::assertNotEquals(self::ERROR, $partner->getName());
        self::assertNotEquals('https://www.test.fr', $partner->getUrl());
        self::assertNotEquals(self::ERROR, $partner->getDescription());
        self::assertNotEquals(self::ERROR, $partner->getLogo());
        self::assertNotFalse($partner->isActive());
        self::assertNotEquals(2, $partner->getPosition());
        self::assertNotEquals(0, $partner->getCountOffer());
        self::assertNotEquals(self::ERROR, $partner->getSlug());
    }

    public function testEmptyEntity(): void
    {
        $partner = (new Partner())
            ->setName('')
            ->setUrl('')
            ->setDescription('')
            ->setLogo('')
            ->setActive('')
            ->setPosition(0)
            ->setCountOffer(0)
            ->setSlug('');

        self::assertEmpty($partner->getName());
        self::assertEmpty($partner->getUrl());
        self::assertEmpty($partner->getDescription());
        self::assertEmpty($partner->getLogo());
        self::assertEmpty($partner->isActive());
        self::assertEquals(0, $partner->getPosition());
        self::assertEquals(0, $partner->getCountOffer());
        self::assertEmpty($partner->getSlug());
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
