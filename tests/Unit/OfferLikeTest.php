<?php

namespace App\Tests\Unit;

use App\Entity\Offer;
use App\Entity\OfferLike;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfferLikeTest extends KernelTestCase
{
    private const DATE = "2021-01-01";

    public function testGetId(): void
    {
        $offerLike = new OfferLike();
        $this->assertNull($offerLike->getId());
    }

    public function testValidEntity(): void
    {
        $offerLike = new OfferLike();
        $user      = new User();
        $offer     = new Offer();
        $offerLike->setCreatedAt(new \DateTimeImmutable(self::DATE));
        $offerLike->setUserLike($user);
        $offerLike->setLikes($offer);

        self::assertEquals($user, $offerLike->getUserLike());
        self::assertEquals($offer, $offerLike->getLikes());
        self::assertEquals(new \DateTimeImmutable(self::DATE), $offerLike->getCreatedAt());
    }

    public function testUnvalidEntity(): void
    {
        $offerLike = new OfferLike();
        $user      = new User();
        $offer     = new Offer();
        $offerLike->setCreatedAt(new \DateTimeImmutable(self::DATE));
        $offerLike->setUserLike($user);
        $offerLike->setLikes($offer);
        $newUser  = new User();
        $newOffer = new Offer();

        self::assertNotEquals($newUser, $offerLike->getUserLike());
        self::assertNotEquals($newOffer, $offerLike->getLikes());
        self::assertNotEquals(new \DateTimeImmutable('2021-02-01'), $offerLike->getCreatedAt());
    }

    public function testEmptyEntity(): void
    {
        $offerLike = (new OfferLike())
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));

        self::assertEmpty($offerLike->getUserLike());
        self::assertEmpty($offerLike->getLikes());
    }
}
