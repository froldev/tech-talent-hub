<?php

namespace App\Tests\Unit;

use App\Entity\Offer;
use App\Entity\Search;
use App\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TagTest extends KernelTestCase
{
    private const TEST        = "test";
    private const ERROR       = "Error";
    private const TRUE        = true;
    private const COUNT       = 1;
    private const DESCRIPTION = "Description";

    public function getEntity(): Tag
    {
        return (new Tag())
            ->setName(self::TEST)
            ->setColor(self::TEST)
            ->setDescription(self::DESCRIPTION)
            ->setActive(self::TRUE)
            ->setCountOffer(self::COUNT);
    }

    public function testGetId(): void
    {
        $tag = new Tag();
        $this->assertNull($tag->getId());
    }

    public function testValidEntity(): void
    {
        $tag = $this->getEntity();

        self::assertEquals(self::TEST, $tag->getName());
        self::assertEquals(self::TEST, $tag->getColor());
        self::assertEquals(self::DESCRIPTION, $tag->getDescription());
        self::assertEquals(self::TRUE, $tag->isActive());
        self::assertEquals(self::COUNT, $tag->getCountOffer());
    }

    public function testUnvalidEntity(): void
    {
        $tag = $this->getEntity();

        self::assertNotEquals(self::ERROR, $tag->getName());
        self::assertNotEquals(self::ERROR, $tag->getColor());
        self::assertNotEquals(self::ERROR, $tag->getDescription());
        self::assertNotEquals(false, $tag->isActive());
        self::assertNotEquals(0, $tag->getCountOffer());
    }

    public function testEmptyEntity(): void
    {
        $tag = (new Tag())
            ->setname('')
            ->setColor('')
            ->setDescription('')
            ->setActive(false)
            ->setCountOffer(0);

        self::assertEmpty($tag->getName());
        self::assertEmpty($tag->getColor());
        self::assertEmpty($tag->getDescription());
        self::assertFalse($tag->isActive());
        self::assertEmpty($tag->getCountOffer());
    }

    public function testAddAndRemoveOffer(): void
    {
        $tag    = $this->getEntity();
        $offer1 = new Offer();
        $offer2 = new Offer();
        $tag->addOffer($offer1);
        $tag->addOffer($offer2);

        $this->assertCount(2, $tag->getOffers());
        $this->assertTrue($tag->getOffers()->contains($offer1));
        $this->assertTrue($tag->getOffers()->contains($offer2));
        $tag->removeOffer($offer1);
        $this->assertCount(1, $tag->getOffers());
        $this->assertFalse($tag->getOffers()->contains($offer1));
        $this->assertTrue($tag->getOffers()->contains($offer2));
    }

    public function testAddAndRemoveSearch(): void
    {
        $tag     = $this->getEntity();
        $search1 = new Search();
        $search2 = new Search();
        $tag->addSearch($search1);
        $tag->addSearch($search2);

        $this->assertCount(2, $tag->getSearch());
        $this->assertTrue($tag->getSearch()->contains($search1));
        $this->assertTrue($tag->getSearch()->contains($search2));
        $tag->removeSearch($search1);
        $this->assertCount(1, $tag->getSearch());
        $this->assertFalse($tag->getSearch()->contains($search1));
        $this->assertTrue($tag->getSearch()->contains($search2));
    }
}
