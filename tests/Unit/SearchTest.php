<?php

namespace App\Tests\Unit;

use App\Entity\Search;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SearchTest extends KernelTestCase
{
    private const TEST  = "test";
    private const FALSE = 'false';
    private const DATE  = '2021-01-01';

    public function getEntity(): Search
    {
        return (new Search())
            ->setName(self::TEST)
            ->setContract(self::TEST)
            ->setTelework(true)
            ->setNewsletter(true)
            ->setCreatedAt(new \DateTimeImmutable(self::DATE));
    }

    public function testGetId(): void
    {
        $search = $this->getEntity();
        $this->assertNull($search->getId());
    }

    public function testEmptyEntity(): void
    {
        $search = (new Search())
            ->setName('')
            ->setContract('')
            ->setTelework('')
            ->setNewsletter('');

        self::assertEmpty($search->getName());
        self::assertEmpty($search->getContract());
        self::assertEmpty($search->isTelework());
        self::assertEmpty($search->isNewsletter());
    }
}
