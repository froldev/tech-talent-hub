<?php

namespace App\Tests\Unit;

use App\Entity\Parameter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ParameterTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): Parameter
    {
        return (new Parameter())
            ->setKey(self::TEST)
            ->setValue(self::TEST)
            ->setType(self::TEST)
            ->setSection(self::TEST);
    }

    public function testGetId(): void
    {
        $parameter = $this->getEntity();
        $this->assertNull($parameter->getId());
    }

    public function testValidEntity(): void
    {
        $partner = $this->getEntity();

        self::assertEquals(self::TEST, $partner->getKey());
        self::assertEquals(self::TEST, $partner->getValue());
        self::assertEquals(self::TEST, $partner->getType());
        self::assertEquals(self::TEST, $partner->getSection());
    }

    public function testUnvalidEntity(): void
    {
        $partner = $this->getEntity();

        self::assertNotEquals(self::ERROR, $partner->getKey());
        self::assertNotEquals(self::ERROR, $partner->getValue());
        self::assertNotEquals(self::ERROR, $partner->getType());
        self::assertNotEquals(self::ERROR, $partner->getSection());
    }

    public function testEmptyEntity(): void
    {
        $parameter = (new Parameter())
            ->setKey('')
            ->setValue('')
            ->setType('')
            ->setSection('');

        self::assertEmpty($parameter->getKey());
        self::assertEmpty($parameter->getValue());
        self::assertEmpty($parameter->getType());
        self::assertEmpty($parameter->getSection());
    }
}
