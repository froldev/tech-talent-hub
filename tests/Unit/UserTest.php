<?php

namespace App\Tests\Unit;

use App\Entity\OfferCandidacy;
use App\Entity\OfferLike;
use App\Entity\Search;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    private const EMAIL    = "test@test.fr";
    private const ROLES    = ['ROLE_USER'];
    private const PASSWORD = "password";
    private const FULLNAME = "test";
    private const ACTIVE   = true;

    public function getEntity(): User
    {
        return (new User())
            ->setEmail(self::EMAIL)
            ->setRoles(self::ROLES)
            ->setPassword(self::PASSWORD)
            ->setFullname(self::FULLNAME)
            ->setRgpd(self::ACTIVE)
            ->setActive(self::ACTIVE);
    }

    public function testGetId(): void
    {
        $user = new User();
        $this->assertNull($user->getId());
    }

    public function testValidEntity(): void
    {
        $user = $this->getEntity();

        self::assertEquals(self::EMAIL, $user->getEmail());
        self::assertEquals(self::ROLES, $user->getRoles());
        self::assertEquals(self::PASSWORD, $user->getPassword());
        self::assertEquals(self::FULLNAME, $user->getFullname());
        self::assertEquals(self::ACTIVE, $user->isRgpd());
        self::assertEquals(self::ACTIVE, $user->isActive());
    }

    public function testEmptyEntity(): void
    {
        $user = (new User())
        ->setEmail('')
        ->setPassword('')
        ->setFullname('')
        ->setRgpd('')
        ->setActive('');

        self::assertEmpty($user->getEmail());
        self::assertEmpty($user->getPassword());
        self::assertEmpty($user->getFullname());
        self::assertEmpty($user->isRgpd());
        self::assertEmpty($user->isActive());
    }

    public function testAddAndRemoveSearch(): void
    {
        $user    = $this->getEntity();
        $search1 = new Search();
        $search2 = new Search();
        $user->addSearch($search1);
        $user->addSearch($search2);

        $this->assertCount(2, $user->getSearches());
        $this->assertTrue($user->getSearches()->contains($search1));
        $this->assertTrue($user->getSearches()->contains($search2));
        $user->removeSearch($search1);
        $this->assertCount(1, $user->getSearches());
        $this->assertFalse($user->getSearches()->contains($search1));
        $this->assertTrue($user->getSearches()->contains($search2));
    }

    public function testAddAndRemoveOfferCandidacy(): void
    {
        $user            = $this->getEntity();
        $offerCandidacy1 = new OfferCandidacy();
        $offerCandidacy2 = new OfferCandidacy();
        $user->addOfferCandidacy($offerCandidacy1);
        $user->addOfferCandidacy($offerCandidacy2);

        $this->assertCount(2, $user->getOfferCandidacies());
        $this->assertTrue($user->getOfferCandidacies()->contains($offerCandidacy1));
        $this->assertTrue($user->getOfferCandidacies()->contains($offerCandidacy2));
        $user->removeOfferCandidacy($offerCandidacy1);
        $this->assertCount(1, $user->getOfferCandidacies());
        $this->assertFalse($user->getOfferCandidacies()->contains($offerCandidacy1));
        $this->assertTrue($user->getOfferCandidacies()->contains($offerCandidacy2));
    }

    public function testAddAndRemoveOfferLike(): void
    {
        $user       = $this->getEntity();
        $offerLike1 = new OfferLike();
        $offerLike2 = new OfferLike();
        $user->addOfferLike($offerLike1);
        $user->addOfferLike($offerLike2);

        $this->assertCount(2, $user->getOfferLikes());
        $this->assertTrue($user->getOfferLikes()->contains($offerLike1));
        $this->assertTrue($user->getOfferLikes()->contains($offerLike2));
        $user->removeOfferLike($offerLike1);
        $this->assertCount(1, $user->getOfferLikes());
        $this->assertFalse($user->getOfferLikes()->contains($offerLike1));
        $this->assertTrue($user->getOfferLikes()->contains($offerLike2));
    }
}
