<?php

namespace App\Tests\Unit;

use App\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JobTest extends KernelTestCase
{
    private const TEST  = "test";
    private const TRUE  = true;
    private const COUNT = 100;
    private const ERROR = "Error";
    private const FALSE = false;

    public function getEntity(): Job
    {
        return (new Job())
            ->setName(self::TEST)
            ->setActive(self::TRUE)
            ->setCountOffer(self::COUNT);
    }

    public function testGetId(): void
    {
        $job = $this->getEntity();
        $this->assertNull($job->getId());
    }

    public function testValidEntity(): void
    {
        $job    = $this->getEntity();
        $parent = new Job();
        $job->setParent($parent);

        self::assertEquals(self::TEST, $job->getName());
        self::assertEquals(self::TRUE, $job->isActive());
        self::assertEquals(self::COUNT, $job->getCountOffer());
        self::assertEquals($parent, $job->getParent());
    }

    public function testUnvalidEntity(): void
    {
        $job    = $this->getEntity();
        $parent = new Job();

        self::assertNotEquals(self::ERROR, $job->getName());
        self::assertNotEquals(self::FALSE, $job->isActive());
        self::assertNotEquals(10, $job->getCountOffer());
        self::assertNotEquals($parent, $job->getParent());
    }

    public function testEmptyEntity(): void
    {
        $job = (new Job())
            ->setName('')
            ->setParent(null);

        self::assertEmpty($job->getName());
        self::assertEmpty($job->getParent());
    }
}
