<?php

namespace App\Tests\Unit;

use App\Entity\City;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CityTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): City
    {
        return (new City())
            ->setLabel(self::TEST)
            ->setName(self::TEST)
            ->setZipCode(self::TEST);
    }

    public function testGetId(): void
    {
        $entity = $this->getEntity();
        $this->assertNull($entity->getId());
    }

    public function testValidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertEquals(self::TEST, $entity->getLabel());
        self::assertEquals(self::TEST, $entity->getName());
        self::assertEquals(self::TEST, $entity->getZipCode());
    }

    public function testUnvalidEntity(): void
    {
        $entity = $this->getEntity();

        self::assertNotEquals(self::ERROR, $entity->getLabel());
        self::assertNotEquals(self::ERROR, $entity->getName());
        self::assertNotEquals(self::ERROR, $entity->getZipCode());
    }

    public function testEmptyEntity(): void
    {
        $entity = (new City())
            ->setLabel('')
            ->setName('')
            ->setZipCode('');

        self::assertEmpty($entity->getLabel());
        self::assertEmpty($entity->getName());
        self::assertEmpty($entity->getZipCode());
    }
}
