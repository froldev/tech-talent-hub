<?php

namespace App\Tests\Unit;

use App\Entity\Formation;
use App\Entity\HardSkill;
use App\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FormationTest extends KernelTestCase
{
    private const TEST  = "test";
    private const ERROR = "Error";

    public function getEntity(): Formation
    {
        return (new Formation())
            ->setName(self::TEST);
    }

    public function testGetId(): void
    {
        $hardSkill = $this->getEntity();
        $this->assertNull($hardSkill->getId());
    }

    public function testValidEntity(): void
    {
        $hardSkill = $this->getEntity();

        self::assertEquals(self::TEST, $hardSkill->getName());
    }

    public function testUnvalidEntity(): void
    {
        $hardSkill = $this->getEntity();

        self::assertNotEquals(self::ERROR, $hardSkill->getName());
    }

    public function testEmptyNameEntity(): void
    {
        $hardSkill = (new HardSkill())->setName('');

        self::assertEmpty($hardSkill->getName());
    }

    public function testAddAndRemoveOffers(): void
    {
        $entity    = $this->getEntity();
        $relation1 = new Offer();
        $relation2 = new Offer();
        $entity->addOffer($relation1);
        $entity->addOffer($relation2);

        $this->assertCount(2, $entity->getOffers());
        $this->assertTrue($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
        $entity->removeOffer($relation1);
        $this->assertCount(1, $entity->getOffers());
        $this->assertFalse($entity->getOffers()->contains($relation1));
        $this->assertTrue($entity->getOffers()->contains($relation2));
    }
}
