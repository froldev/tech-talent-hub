<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241210204532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE city_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE company_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE department_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE follow_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE formation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hard_skill_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE job_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE language_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE navbar_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE offer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE offer_candidacy_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE offer_like_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE parameter_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE partner_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE region_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE search_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE soft_skill_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE city (id INT NOT NULL, department_id INT NOT NULL, name VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, latitude VARCHAR(255) DEFAULT NULL, longitude VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D5B0234EA750E8 ON city (label)');
        $this->addSql('CREATE INDEX IDX_2D5B0234AE80F5DF ON city (department_id)');
        $this->addSql('COMMENT ON COLUMN city.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, description TEXT DEFAULT NULL, activity_sector VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F5E237E06 ON company (name)');
        $this->addSql('COMMENT ON COLUMN company.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE department (id INT NOT NULL, region_id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(10) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CD1DE18A5E237E06 ON department (name)');
        $this->addSql('CREATE INDEX IDX_CD1DE18A98260155 ON department (region_id)');
        $this->addSql('CREATE TABLE follow (id INT NOT NULL, offer_candidacy_id INT DEFAULT NULL, comment TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_68344470BE8B2B8F ON follow (offer_candidacy_id)');
        $this->addSql('COMMENT ON COLUMN follow.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE formation (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_404021BF5E237E06 ON formation (name)');
        $this->addSql('CREATE TABLE formation_offer (formation_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(formation_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_5F5D194B5200282E ON formation_offer (formation_id)');
        $this->addSql('CREATE INDEX IDX_5F5D194B53C674EE ON formation_offer (offer_id)');
        $this->addSql('CREATE TABLE hard_skill (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9EE9EE465E237E06 ON hard_skill (name)');
        $this->addSql('CREATE TABLE hard_skill_offer (hard_skill_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(hard_skill_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_B1F16025B7DB062 ON hard_skill_offer (hard_skill_id)');
        $this->addSql('CREATE INDEX IDX_B1F1602553C674EE ON hard_skill_offer (offer_id)');
        $this->addSql('CREATE TABLE job (id INT NOT NULL, parent_id INT DEFAULT NULL, count_offer INT NOT NULL, name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBD8E0F85E237E06 ON job (name)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F8727ACA70 ON job (parent_id)');
        $this->addSql('CREATE TABLE language (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4DB71B55E237E06 ON language (name)');
        $this->addSql('CREATE TABLE language_offer (language_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(language_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_568E836F82F1BAF4 ON language_offer (language_id)');
        $this->addSql('CREATE INDEX IDX_568E836F53C674EE ON language_offer (offer_id)');
        $this->addSql('CREATE TABLE navbar (id INT NOT NULL, path VARCHAR(255) NOT NULL, position INT NOT NULL, name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E0843B8CB548B0F ON navbar (path)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E0843B8C5E237E06 ON navbar (name)');
        $this->addSql('COMMENT ON COLUMN navbar.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE offer (id INT NOT NULL, city_id INT DEFAULT NULL, job_id INT DEFAULT NULL, company_id INT DEFAULT NULL, channel VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, reference VARCHAR(255) NOT NULL, candidacy_url TEXT DEFAULT NULL, description TEXT DEFAULT NULL, contact VARCHAR(255) DEFAULT NULL, contract VARCHAR(255) DEFAULT NULL, contract_type VARCHAR(255) DEFAULT NULL, contract_nature VARCHAR(255) DEFAULT NULL, experience VARCHAR(255) DEFAULT NULL, salary VARCHAR(255) DEFAULT NULL, salary_commentary VARCHAR(255) DEFAULT NULL, salary_complement VARCHAR(255) DEFAULT NULL, salary_other VARCHAR(255) DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL, work_travel VARCHAR(255) DEFAULT NULL, qualification VARCHAR(255) DEFAULT NULL, alternation BOOLEAN NOT NULL, telework BOOLEAN NOT NULL, validation BOOLEAN NOT NULL, active BOOLEAN NOT NULL, slug VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_29D6873EAEA34913 ON offer (reference)');
        $this->addSql('CREATE INDEX IDX_29D6873E8BAC62AF ON offer (city_id)');
        $this->addSql('CREATE INDEX IDX_29D6873EBE04EA9 ON offer (job_id)');
        $this->addSql('CREATE INDEX IDX_29D6873E979B1AD6 ON offer (company_id)');
        $this->addSql('COMMENT ON COLUMN offer.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE offer_offer_candidacy (offer_id INT NOT NULL, offer_candidacy_id INT NOT NULL, PRIMARY KEY(offer_id, offer_candidacy_id))');
        $this->addSql('CREATE INDEX IDX_6B63AE8853C674EE ON offer_offer_candidacy (offer_id)');
        $this->addSql('CREATE INDEX IDX_6B63AE88BE8B2B8F ON offer_offer_candidacy (offer_candidacy_id)');
        $this->addSql('CREATE TABLE offer_candidacy (id INT NOT NULL, user_offer_candidacy_id INT NOT NULL, response VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7AF52B5912CFCD74 ON offer_candidacy (user_offer_candidacy_id)');
        $this->addSql('COMMENT ON COLUMN offer_candidacy.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE offer_like (id INT NOT NULL, user_like_id INT DEFAULT NULL, likes_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_529B6EA4DD96E438 ON offer_like (user_like_id)');
        $this->addSql('CREATE INDEX IDX_529B6EA42F23775F ON offer_like (likes_id)');
        $this->addSql('COMMENT ON COLUMN offer_like.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE parameter (id INT NOT NULL, key VARCHAR(255) NOT NULL, value JSON NOT NULL, type VARCHAR(255) NOT NULL, section VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE partner (id INT NOT NULL, url TEXT DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, position INT DEFAULT NULL, count_offer INT NOT NULL, name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_312B3E165E237E06 ON partner (name)');
        $this->addSql('CREATE TABLE partner_offer (partner_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(partner_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_3AEA2B5B9393F8FE ON partner_offer (partner_id)');
        $this->addSql('CREATE INDEX IDX_3AEA2B5B53C674EE ON partner_offer (offer_id)');
        $this->addSql('CREATE TABLE post (id INT NOT NULL, type VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, content TEXT NOT NULL, website VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, image_name VARCHAR(255) DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN post.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE region (id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(10) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F1765E237E06 ON region (name)');
        $this->addSql('CREATE TABLE search (id INT NOT NULL, user_search_id INT DEFAULT NULL, tag_id INT NOT NULL, city_id INT NOT NULL, contract VARCHAR(255) DEFAULT NULL, telework BOOLEAN NOT NULL, newsletter BOOLEAN NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B4F0DBA75E237E06 ON search (name)');
        $this->addSql('CREATE INDEX IDX_B4F0DBA765E7ED4D ON search (user_search_id)');
        $this->addSql('CREATE INDEX IDX_B4F0DBA7BAD26311 ON search (tag_id)');
        $this->addSql('CREATE INDEX IDX_B4F0DBA78BAC62AF ON search (city_id)');
        $this->addSql('COMMENT ON COLUMN search.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE soft_skill (id INT NOT NULL, description TEXT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_164AECD45E237E06 ON soft_skill (name)');
        $this->addSql('CREATE TABLE soft_skill_offer (soft_skill_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(soft_skill_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_2B6778DE88034CA4 ON soft_skill_offer (soft_skill_id)');
        $this->addSql('CREATE INDEX IDX_2B6778DE53C674EE ON soft_skill_offer (offer_id)');
        $this->addSql('CREATE TABLE tag (id INT NOT NULL, parent_id INT DEFAULT NULL, description TEXT DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, count_offer INT NOT NULL, name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_389B7835E237E06 ON tag (name)');
        $this->addSql('CREATE INDEX IDX_389B783727ACA70 ON tag (parent_id)');
        $this->addSql('CREATE TABLE tag_offer (tag_id INT NOT NULL, offer_id INT NOT NULL, PRIMARY KEY(tag_id, offer_id))');
        $this->addSql('CREATE INDEX IDX_B3A9E38BAD26311 ON tag_offer (tag_id)');
        $this->addSql('CREATE INDEX IDX_B3A9E3853C674EE ON tag_offer (offer_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, fullname VARCHAR(255) NOT NULL, rgpd BOOLEAN NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('COMMENT ON COLUMN messenger_messages.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN messenger_messages.available_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN messenger_messages.delivered_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE department ADD CONSTRAINT FK_CD1DE18A98260155 FOREIGN KEY (region_id) REFERENCES region (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE follow ADD CONSTRAINT FK_68344470BE8B2B8F FOREIGN KEY (offer_candidacy_id) REFERENCES offer_candidacy (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE formation_offer ADD CONSTRAINT FK_5F5D194B5200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE formation_offer ADD CONSTRAINT FK_5F5D194B53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hard_skill_offer ADD CONSTRAINT FK_B1F16025B7DB062 FOREIGN KEY (hard_skill_id) REFERENCES hard_skill (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hard_skill_offer ADD CONSTRAINT FK_B1F1602553C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F8727ACA70 FOREIGN KEY (parent_id) REFERENCES job (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE language_offer ADD CONSTRAINT FK_568E836F82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE language_offer ADD CONSTRAINT FK_568E836F53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873EBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer_offer_candidacy ADD CONSTRAINT FK_6B63AE8853C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer_offer_candidacy ADD CONSTRAINT FK_6B63AE88BE8B2B8F FOREIGN KEY (offer_candidacy_id) REFERENCES offer_candidacy (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer_candidacy ADD CONSTRAINT FK_7AF52B5912CFCD74 FOREIGN KEY (user_offer_candidacy_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer_like ADD CONSTRAINT FK_529B6EA4DD96E438 FOREIGN KEY (user_like_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer_like ADD CONSTRAINT FK_529B6EA42F23775F FOREIGN KEY (likes_id) REFERENCES offer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partner_offer ADD CONSTRAINT FK_3AEA2B5B9393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partner_offer ADD CONSTRAINT FK_3AEA2B5B53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE search ADD CONSTRAINT FK_B4F0DBA765E7ED4D FOREIGN KEY (user_search_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE search ADD CONSTRAINT FK_B4F0DBA7BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE search ADD CONSTRAINT FK_B4F0DBA78BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE soft_skill_offer ADD CONSTRAINT FK_2B6778DE88034CA4 FOREIGN KEY (soft_skill_id) REFERENCES soft_skill (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE soft_skill_offer ADD CONSTRAINT FK_2B6778DE53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783727ACA70 FOREIGN KEY (parent_id) REFERENCES tag (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tag_offer ADD CONSTRAINT FK_B3A9E38BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tag_offer ADD CONSTRAINT FK_B3A9E3853C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE city_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE company_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE department_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE follow_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE formation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hard_skill_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE job_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE language_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE navbar_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE offer_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE offer_candidacy_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE offer_like_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE parameter_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE partner_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE region_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE search_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE soft_skill_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE city DROP CONSTRAINT FK_2D5B0234AE80F5DF');
        $this->addSql('ALTER TABLE department DROP CONSTRAINT FK_CD1DE18A98260155');
        $this->addSql('ALTER TABLE follow DROP CONSTRAINT FK_68344470BE8B2B8F');
        $this->addSql('ALTER TABLE formation_offer DROP CONSTRAINT FK_5F5D194B5200282E');
        $this->addSql('ALTER TABLE formation_offer DROP CONSTRAINT FK_5F5D194B53C674EE');
        $this->addSql('ALTER TABLE hard_skill_offer DROP CONSTRAINT FK_B1F16025B7DB062');
        $this->addSql('ALTER TABLE hard_skill_offer DROP CONSTRAINT FK_B1F1602553C674EE');
        $this->addSql('ALTER TABLE job DROP CONSTRAINT FK_FBD8E0F8727ACA70');
        $this->addSql('ALTER TABLE language_offer DROP CONSTRAINT FK_568E836F82F1BAF4');
        $this->addSql('ALTER TABLE language_offer DROP CONSTRAINT FK_568E836F53C674EE');
        $this->addSql('ALTER TABLE offer DROP CONSTRAINT FK_29D6873E8BAC62AF');
        $this->addSql('ALTER TABLE offer DROP CONSTRAINT FK_29D6873EBE04EA9');
        $this->addSql('ALTER TABLE offer DROP CONSTRAINT FK_29D6873E979B1AD6');
        $this->addSql('ALTER TABLE offer_offer_candidacy DROP CONSTRAINT FK_6B63AE8853C674EE');
        $this->addSql('ALTER TABLE offer_offer_candidacy DROP CONSTRAINT FK_6B63AE88BE8B2B8F');
        $this->addSql('ALTER TABLE offer_candidacy DROP CONSTRAINT FK_7AF52B5912CFCD74');
        $this->addSql('ALTER TABLE offer_like DROP CONSTRAINT FK_529B6EA4DD96E438');
        $this->addSql('ALTER TABLE offer_like DROP CONSTRAINT FK_529B6EA42F23775F');
        $this->addSql('ALTER TABLE partner_offer DROP CONSTRAINT FK_3AEA2B5B9393F8FE');
        $this->addSql('ALTER TABLE partner_offer DROP CONSTRAINT FK_3AEA2B5B53C674EE');
        $this->addSql('ALTER TABLE search DROP CONSTRAINT FK_B4F0DBA765E7ED4D');
        $this->addSql('ALTER TABLE search DROP CONSTRAINT FK_B4F0DBA7BAD26311');
        $this->addSql('ALTER TABLE search DROP CONSTRAINT FK_B4F0DBA78BAC62AF');
        $this->addSql('ALTER TABLE soft_skill_offer DROP CONSTRAINT FK_2B6778DE88034CA4');
        $this->addSql('ALTER TABLE soft_skill_offer DROP CONSTRAINT FK_2B6778DE53C674EE');
        $this->addSql('ALTER TABLE tag DROP CONSTRAINT FK_389B783727ACA70');
        $this->addSql('ALTER TABLE tag_offer DROP CONSTRAINT FK_B3A9E38BAD26311');
        $this->addSql('ALTER TABLE tag_offer DROP CONSTRAINT FK_B3A9E3853C674EE');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE follow');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE formation_offer');
        $this->addSql('DROP TABLE hard_skill');
        $this->addSql('DROP TABLE hard_skill_offer');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE language_offer');
        $this->addSql('DROP TABLE navbar');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE offer_offer_candidacy');
        $this->addSql('DROP TABLE offer_candidacy');
        $this->addSql('DROP TABLE offer_like');
        $this->addSql('DROP TABLE parameter');
        $this->addSql('DROP TABLE partner');
        $this->addSql('DROP TABLE partner_offer');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE search');
        $this->addSql('DROP TABLE soft_skill');
        $this->addSql('DROP TABLE soft_skill_offer');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tag_offer');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
