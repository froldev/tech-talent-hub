<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__ . '/src')    // Répertoire source
    ->in(__DIR__ . '/tests')  // Répertoire des tests
    ->exclude('var')          // Exclut le répertoire var
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR1' => true,        // Applique les normes PSR-1
        '@PSR2' => true,        // Applique les normes PSR-2
        '@PSR12' => true,       // Applique les normes PSR-12

        // Personnalisations supplémentaires
        'array_syntax' => ['syntax' => 'short'], // Syntaxe courte pour les tableaux
        'binary_operator_spaces' => [
            'default' => 'align_single_space',   // Aligne les opérateurs binaires avec un espace
        ],
        'blank_line_before_statement' => [
            'statements' => ['return', 'if', 'foreach', 'while', 'switch', 'try'],
        ],
        'concat_space' => ['spacing' => 'one'], // Un espace avant et après les concaténations (.)
        'no_unused_imports' => true,           // Supprime les imports inutilisés
        'single_quote' => false,               // Utilise des guillemets doubles
        'ordered_imports' => ['sort_algorithm' => 'alpha'], // Trie les imports
        'no_trailing_whitespace' => true,      // Supprime les espaces en fin de ligne
        'trailing_comma_in_multiline' => ['elements' => ['arrays', 'arguments']],
        // Désactiver les vérifications des commentaires
        'phpdoc_to_comment' => false,
        'phpdoc_types' => false,
    ])
    ->setIndent("    ")          // Utilise des espaces (4) pour l'indentation
    ->setLineEnding("\n")        // Utilise des sauts de ligne Unix
    ->setFinder($finder)
;
