//import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app/choices.min.css';
import './styles/line.css';
import './styles/materialdesignicons.min.css';
import './styles/app/tailwind.css';
import './styles/app/background.css';
import './styles/app.css';

import './js/app/choices.min.js';
import './js/feather.min.js';
import './js/plugins.init.js';
import './js/app/app.js';

console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');

import Alpine from 'alpinejs';
window.Alpine = Alpine;
Alpine.start();
