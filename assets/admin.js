//import './bootstrap.js';
import { startStimulusApp } from '@symfony/stimulus-bundle';

const app = startStimulusApp();
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/admin/simplebar.min.css';
import './styles/line.css';
import './styles/materialdesignicons.min.css';
import './styles/admin/tailwind.css';
import './styles/app.css';

import './js/feather.min.js';
import './js/admin/simplebar.min.js';
import './js/plugins.init.js';
import './js/admin/app.js';

console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');

import Alpine from 'alpinejs';
window.Alpine = Alpine;
Alpine.start();
