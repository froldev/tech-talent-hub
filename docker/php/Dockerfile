# Start with the official PHP 8.2 image
FROM php:8.2-fpm

# Set name of folder
ARG FOLDER=techtalenthub

# Install system dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-utils \
        git \
        libpq-dev \
        libicu-dev \
        libpng-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        libxslt-dev \
        librabbitmq-dev \
        unzip \
        wget \
        zip \
        curl \
        npm \
        supervisor \
        libmcrypt-dev \
        libc-client-dev \
        libkrb5-dev \
        libssl-dev \
        build-essential \
        autoconf \
        zlib1g-dev \
        libpcre3-dev \
        libmemcached-dev \
        libz-dev \
        libjpeg-dev \
        libfreetype6-dev \
        libssl-dev \
        libreadline-dev \
        libmagickwand-dev \
        libmagickcore-dev \
        openssl \
        libcurl4-openssl-dev \
        pkg-config \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Set timezone and locale
ENV TZ=Europe/Paris
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen fr_FR.UTF-8 \
    && update-locale LANG=fr_FR.UTF-8 \
    && dpkg-reconfigure --frontend noninteractive tzdata

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Symfony CLI
RUN curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony*/bin/symfony /usr/local/bin/symfony

# Install PHP extensions
RUN docker-php-ext-configure intl \
    && docker-php-ext-install zip opcache intl calendar dom mbstring gd xsl pdo pdo_pgsql pgsql \
    && pecl install amqp \
    && docker-php-ext-enable amqp opcache

# Install Xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

# Comment out zend_extension in docker-php-ext-xdebug.ini
RUN sed -i 's/^zend_extension=xdebug/#zend_extension=xdebug/' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Copy Xdebug configuration file
COPY ./docker/php/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# Create the configuration directory
RUN mkdir -p /usr/local/etc/php/conf.d

# Create the session directory with correct permissions
RUN mkdir -p /var/lib/php/sessions && chown -R www-data:www-data /var/lib/php/sessions

# Copy the php.ini file into the PHP configuration directory
COPY ./docker/php/php.ini /usr/local/etc/php/php.ini

# Copy the opcache.ini file into the PHP configuration directory
COPY ./docker/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# Install Yarn globally
RUN npm install --global yarn@latest

# Install and configure Caddy as the web server
RUN curl -fsSL https://getcaddy.com | bash -s personal

# Configure Supervisor to manage services
COPY ./docker/php/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Set the working directory
WORKDIR /var/www/${FOLDER}

# Copy source files
COPY . /var/www/${FOLDER}

# Expose port and start services
EXPOSE 9000 80

# Start Supervisor
CMD ["supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
