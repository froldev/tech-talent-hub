<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Company;
use App\Entity\Formation;
use App\Entity\HardSkill;
use App\Entity\Job;
use App\Entity\Language;
use App\Entity\Offer;
use App\Entity\Partner;
use App\Entity\SoftSkill;
use App\Entity\Tag;
use App\Util\OfferChoice;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminOfferType extends AbstractType
{
    public function __construct(
        private readonly OfferChoice $offerChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'channel',
                ChoiceType::class,
                [
                    'placeholder' => 'Choisissez un type de canal',
                    'choices'     => $this->offerChoice->getSelectChannels(),
                    'label'       => 'Type de canal: *',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'autocomplete' => true,
                    'required'     => true,
                ],
            )
            ->add(
                'reference',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Référence: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'title',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Titre: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'candidacyUrl',
                UrlType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'URL pour postuler:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label'      => 'Description:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'contract',
                ChoiceType::class,
                [
                    'placeholder' => 'Choisissez un type de contrat',
                    'choices'     => $this->offerChoice->getSelectOfContracts(),
                    'label'       => 'Contrat:',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'contractType',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Type de Contrat:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'contractNature',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Nature du Contrat:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'experience',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Expérience:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'salary',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Salaire à l\'année en euros:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'salaryCommentary',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Commentaire:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'salaryComplement',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Complément:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'salaryOther',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Autre:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'duration',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Durée de Travail:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'workTravel',
                TextType::class,
                [
                    'attr' => [
                        'class'       => 'ts-control',
                        'placeholder' => 'Indiquez les déplacements à prévoir',
                    ],
                    'label'      => 'Déplacement:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'alternation',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'label'       => 'Alternance: *',
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            )
            ->add(
                'telework',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'label'       => 'Télétravail: *',
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'label'       => 'Offre active: *',
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            )
            ->add(
                'validation',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'label'       => 'Offre validée: *',
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            )
            ->add(
                'city',
                EntityType::class,
                [
                    'class'        => City::class,
                    'choice_label' => 'name',
                    'label'        => 'Ville:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez une ville',
                    ],
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'partners',
                EntityType::class,
                [
                    'class'         => Partner::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('p')
                            ->orderBy('p.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Partenaires:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez un ou plusieurs partenaires',
                    ],
                    'multiple'     => true,
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'company',
                EntityType::class,
                [
                    'class'         => Company::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Entreprise:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez une entreprise',
                    ],
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'job',
                EntityType::class,
                [
                    'class'         => Job::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('j')
                            ->where('j.active = true')
                            ->orderBy('j.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Métier: *',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez un métier',
                    ],
                    'required'     => true,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'tags',
                EntityType::class,
                [
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('t')
                            ->orderBy('t.name', 'ASC');
                    },
                    'class'        => Tag::class,
                    'choice_label' => 'name',
                    'label'        => 'Technologies:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez une ou plusieurs technologies',
                    ],
                    'autocomplete' => true,
                    'multiple'     => true,
                    'required'     => false,
                ],
            )
            ->add(
                'hardSkills',
                EntityType::class,
                [
                    'class'         => HardSkill::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('h')
                            ->orderBy('h.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Savoir Faire:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez un ou plusieurs savoir-faire',
                    ],
                    'multiple'     => true,
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'softSkills',
                EntityType::class,
                [
                    'class'         => SoftSkill::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('s')
                            ->orderBy('s.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Savoir Être:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez un ou plusieurs savoir-être',
                    ],
                    'multiple'     => true,
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'formations',
                EntityType::class,
                [
                    'class'         => Formation::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('l')
                            ->orderBy('l.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Formations:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez une ou plusieurs formations',
                    ],
                    'multiple'     => true,
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'languages',
                EntityType::class,
                [
                    'class'         => Language::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('l')
                            ->orderBy('l.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Langues:',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Choisissez une ou plusieurs langues',
                    ],
                    'multiple'     => true,
                    'required'     => false,
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Offer::class,
            ],
        );
    }
}
