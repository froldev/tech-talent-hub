<?php

namespace App\Form;

use App\Entity\Tag;
use App\Form\Type\ActiveType;
use App\Repository\TagRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminTagType extends AbstractType
{
    public function __construct(
        private readonly TagRepository $tagRepository,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'label'      => 'Nom du tag: *',
                'label_attr' => [
                    'class' => 'class-label',
                ],
                'attr' => [
                    'class' => 'ts-control',
                ],
                'required' => true,
            ],
        )
            ->add(
                'parent',
                ChoiceType::class,
                [
                    'label'      => 'Métier: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'choices'      => $this->tagRepository->getParentChoices(),
                    'choice_label' => 'name',
                    'required'     => true,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'active',
                ActiveType::class,
                [
                    'data_class' => Tag::class,
                    'label'      => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Tag::class,
            ],
        );
    }
}
