<?php

namespace App\Form;

use App\Entity\Post;
use App\Form\Type\ActiveType;
use App\Util\PostChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AdminPostType extends AbstractType
{
    public function __construct(
        private readonly PostChoice $postChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Titre: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'website',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Website: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices'     => $this->postChoice->getListOfTypes(),
                    'placeholder' => 'Choisir un type d\'information',
                    'label'       => 'Type: *',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'required'     => true,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'url',
                UrlType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'URL du site: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'content',
                TextareaType::class,
                [
                    'label'      => 'Contenu: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'imageFile',
                VichImageType::class,
                [
                    'label'      => 'Image:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required'     => false,
                    'download_uri' => false,
                    'image_uri'    => true,
                    'asset_helper' => true,
                ],
            )
            ->add(
                'active',
                ActiveType::class,
                [
                    'data_class' => Post::class,
                    'label'      => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Post::class,
            ],
        );
    }
}
