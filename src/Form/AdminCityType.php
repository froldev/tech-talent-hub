<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Department;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AdminCityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'zipCode',
                TextType::class,
                [
                    'label'      => 'Code Postal: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'constraints' => [
                        new Assert\Length(
                            [
                                'min'        => 5,
                                'max'        => 5,
                                'minMessage' => 'Le Code Postal doit faire {{ limit }} chiffres',
                            ],
                        ),
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'name',
                TextType::class,
                [
                    'label'      => 'Nom de la Commune: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'department',
                EntityType::class,
                [
                    'class'        => Department::class,
                    'choice_label' => 'name',
                    'label'        => 'Département: *',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'placeholder'  => 'Choisir un département',
                    'required'     => true,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'latitude',
                TextType::class,
                [
                    'label'      => 'Latitude:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'longitude',
                TextType::class,
                [
                    'label'      => 'Longitude:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => City::class,
            ],
        );
    }
}
