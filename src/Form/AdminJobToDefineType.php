<?php

namespace App\Form;

use App\Entity\Job;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminJobToDefineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'parent',
            EntityType::class,
            [
                'class'         => Job::class,
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('j')
                        ->where('j.parent IS NULL')
                        ->orderBy('j.name', 'ASC');
                },
                'choice_label' => 'name',
                'label'        => 'Catégorie: *',
                'label_attr'   => [
                    'class' => 'class-label',
                ],
                'placeholder'  => 'Choisir une catégorie',
                'autocomplete' => true,
                'required'     => true,
            ],
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
            ],
        );
    }
}
