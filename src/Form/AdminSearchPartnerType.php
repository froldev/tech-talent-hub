<?php

namespace App\Form;

use App\Model\AdminSearchPartnerData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchPartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add(
            'q',
            TextType::class,
            [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'Votre recherche',
                    'class'       => 'ts-control',
                ],
                'required' => false,
            ],
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchPartnerData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
