<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Url;

class AdminPartnerToDefineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add(
            'url',
            UrlType::class,
            [
                'label'      => 'Url: *',
                'label_attr' => [
                    'class' => 'class-label',
                ],
                'attr' => [
                    'class' => 'ts-control',
                ],
                'constraints' => [
                    new Url(
                        [
                            'message' => 'L\'url "{{ value }}" n\'est pas valide',
                        ],
                    ),
                ],
                'required' => true,
            ],
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
            ],
        );
    }
}
