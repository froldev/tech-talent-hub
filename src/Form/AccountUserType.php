<?php

namespace App\Form;

use App\Constant\ParameterConstants;
use App\Constant\RoleConstants;
use App\Entity\User;
use App\Util\DepartmentChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AccountUserType extends AbstractType
{
    public function __construct(
        private readonly DepartmentChoice $departmentChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user    = $options['data'] ?? null;
        $isAdmin = $user ? \in_array(RoleConstants::ADMIN, $user->getRoles(), true) : false;

        $builder
            ->add(
                'fullname',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Nom complet: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Email: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'plainPassword',
                PasswordType::class,
                [
                    'hash_property_path' => 'password',
                    'mapped'             => false,
                    'attr'               => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Mot de Passe :',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'help'      => 'Le mot de passe doit contenir ' .
                    'au moins ' . ParameterConstants::PASSWORD_MIN .
                    ' caractères.',
                    'help_attr' => [
                        'class' => 'text-sm mt-1 text-gray-500',
                    ],
                    'required'    => false,
                    'constraints' => [
                        new Assert\Length(
                            [
                                'min'        => ParameterConstants::PASSWORD_MIN,
                                'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères',
                                'max'        => 4096,
                            ],
                        ),
                    ],
                ],
            )
            ->add(
                'rgpd',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label'      => 'RGPD: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            );

        if ($isAdmin) {
            $builder
                ->add(
                    'department',
                    ChoiceType::class,
                    [
                        'choices'     => $this->departmentChoice->getDepartments(),
                        'choice_attr' => function ($choice, $key, $value) {
                            return ['class' => 'me-2'];
                        },
                        'label'      => 'Département: *',
                        'label_attr' => [
                            'class' => 'class-label',
                        ],
                        'required' => true,
                        'expanded' => true,
                    ],
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'user'       => null,
            ],
        );
    }
}
