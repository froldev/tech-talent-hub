<?php

namespace App\Form;

use App\Constant\ParameterConstants;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'password',
                PasswordType::class,
                [
                    'label'      => false,
                    'label_attr' => [
                        'class' => 'font-semibold',
                    ],
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'placeholder'  => '••••••',
                        'class'        => 'ts-control',
                    ],
                    'required'    => true,
                    'constraints' => [
                        new Length(
                            [
                                'min'        => ParameterConstants::PASSWORD_MIN,
                                'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères',
                                'max'        => 4096,
                            ],
                        ),
                    ],
                    'help'      => 'Votre mot de passe doit contenir'
                    . ' au moins ' . ParameterConstants::PASSWORD_MIN
                    . ' caractères.',
                    'help_attr' => [
                        'class' => 'text-xs text-gray-500',
                    ],
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
            ],
        );
    }
}
