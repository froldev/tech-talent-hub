<?php

namespace App\Form;

use App\Model\AdminSearchJobData;
use App\Util\JobChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchJobType extends AbstractType
{
    public function __construct(
        private readonly JobChoice $jobChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'q',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Votre recherche',
                        'class'       => 'ts-control',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'parent',
                ChoiceType::class,
                [
                    'label'        => false,
                    'placeholder'  => 'Veuillez choisir une Catégorie',
                    'required'     => false,
                    'choices'      => $this->jobChoice->getSelectOfParentForAdmin(),
                    'autocomplete' => true,
                ],
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label'    => false,
                    'required' => false,
                    'choices'  => [
                        'Tous les métiers' => null,
                        'Actifs'           => true,
                        'Inactifs'         => false,
                    ],
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchJobData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
