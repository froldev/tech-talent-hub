<?php

namespace App\Form;

use App\Entity\Tag;
use App\Form\Type\ActiveType;
use App\Util\ColorChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminTagTypeType extends AbstractType
{
    public function __construct(
        private readonly ColorChoice $colorChoice,
    ) {
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'label'      => 'Nom du tag: *',
                'label_attr' => [
                    'class' => 'class-label',
                ],
                'attr' => [
                    'class' => 'ts-control',
                ],
                'required' => true,
            ],
        )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label'      => 'Description:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'color',
                ChoiceType::class,
                [
                    'label'      => 'Couleur: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'choices'  => $this->colorChoice->getColors(),
                    'required' => true,
                ],
            )
            ->add(
                'active',
                ActiveType::class,
                [
                    'data_class' => Tag::class,
                    'label'      => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Tag::class,
            ],
        );
    }
}
