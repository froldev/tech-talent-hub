<?php

namespace App\Form;

use App\Model\AdminSearchCityData;
use App\Util\DepartmentChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchCityType extends AbstractType
{
    public function __construct(
        private readonly DepartmentChoice $departmentChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'q',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Votre recherche',
                        'class'       => 'ts-control',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'department',
                ChoiceType::class,
                [
                    'label'        => false,
                    'placeholder'  => 'Choisissez un Département',
                    'required'     => false,
                    'choices'      => $this->departmentChoice->getDepartments(),
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchCityData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
