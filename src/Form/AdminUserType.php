<?php

namespace App\Form;

use App\Constant\ParameterConstants;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AdminUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'fullname',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Nom complet: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Email: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'plainPassword',
                PasswordType::class,
                [
                    'hash_property_path' => 'password',
                    'mapped'             => false,
                    'attr'               => [
                        'class' => 'ts-control',
                    ],
                    'label'      => 'Mot de Passe:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'help'      => 'Le mot de passe doit contenir ' .
                    'au moins ' . ParameterConstants::PASSWORD_MIN .
                    ' caractères',
                    'help_attr' => [
                        'class' => 'text-sm mt-1 text-gray-500',
                    ],
                    'required'    => false,
                    'constraints' => [
                        new Assert\Length(
                            [
                                'min'        => ParameterConstants::PASSWORD_MIN,
                                'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères',
                                'max'        => 4096,
                            ],
                        ),
                    ],
                ],
            )
            ->add(
                'roles',
                ChoiceType::class,
                [
                    'choices' => [
                        'Administrateur' => 'ROLE_ADMIN',
                        'Utilisateur'    => 'ROLE_USER',
                    ],
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label'      => 'Rôles: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => true,
                ],
            )
            ->add(
                'rgpd',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label'      => 'RGPD: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
                    'choice_attr' => function ($choice, $key, $value) {
                        return ['class' => 'me-2'];
                    },
                    'label'      => 'Compte actif: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                    'expanded' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ],
        );
    }
}
