<?php

namespace App\Form;

use App\Model\AdminSearchTagData;
use App\Repository\TagRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchTagType extends AbstractType
{
    public function __construct(
        private readonly TagRepository $tagRepository,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'q',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Votre recherche',
                        'class'       => 'ts-control',
                    ],
                    'required'     => false,
                    'autocomplete' => false,
                ],
            )
            ->add(
                'parent',
                ChoiceType::class,
                [
                    'label'      => false,
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'choices'      => $this->tagRepository->getParentChoices(),
                    'choice_label' => 'name',
                    'placeholder'  => 'Choisissez une Technologie',
                    'required'     => false,
                    'autocomplete' => true,
                ],
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label'    => false,
                    'required' => false,
                    'choices'  => [
                        'Toutes les Technologies Actives et Inactives' => null,
                        'Technologies Actives'                         => true,
                        'Technologies Inactives'                       => false,
                    ],
                    'placeholder'  => 'Choisissez une Visibilité',
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchTagData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
