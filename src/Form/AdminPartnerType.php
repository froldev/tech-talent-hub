<?php

namespace App\Form;

use App\Entity\Partner;
use App\Form\Type\ActiveType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Url;

class AdminPartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $position = $options['position'];

        if (!empty($position)) {
            $builder->add(
                'position',
                ChoiceType::class,
                [
                    'placeholder' => 'Choisir une position',
                    'choices'     => $position,
                    'label'       => 'Position:',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'autocomplete' => true,
                    'required'     => false,
                ],
            );
        }

        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label'      => 'Nom du Partenaire: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label'      => 'Description:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'url',
                UrlType::class,
                [
                    'label'      => 'Url:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'constraints' => [
                        new Url(
                            [
                                'message' => 'L\'url "{{ value }}" n\'est pas valide',
                            ],
                        ),
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'logo',
                UrlType::class,
                [
                    'label'      => 'Logo:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'constraints' => [
                        new Url(
                            [
                                'message' => 'L\'url "{{ value }}" n\'est pas valide',
                            ],
                        ),
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'active',
                ActiveType::class,
                [
                    'data_class' => Partner::class,
                    'label'      => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Partner::class,
                'position'   => [],
            ],
        );
    }
}
