<?php

namespace App\Form;

use App\Entity\Tag;
use App\Model\AdminSearchOfferData;
use App\Service\TagService;
use App\Util\DepartmentChoice;
use App\Util\OfferChoice;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchOfferType extends AbstractType
{
    public function __construct(
        private readonly TagService $tagService,
        private readonly DepartmentChoice $departmentChoice,
        private readonly OfferChoice $offerChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'q',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Votre Recherche',
                        'class'       => 'ts-control',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'tags',
                EntityType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Choisissez une ou plusieurs Technologie(s)',
                    ],
                    'class'         => Tag::class,
                    'choice_label'  => 'name',
                    'required'      => false,
                    'autocomplete'  => true,
                    'multiple'      => true,
                    'query_builder' => function () {
                        return $this->tagService->getChildActiveTagsQueryBuilder();
                    },
                ],
            )
            ->add(
                'department',
                ChoiceType::class,
                [
                    'label'        => false,
                    'placeholder'  => 'Choisissez un Département',
                    'required'     => false,
                    'choices'      => $this->departmentChoice->getDepartments(),
                    'autocomplete' => true,
                ],
            )
            ->add(
                'contract',
                ChoiceType::class,
                [
                    'label'        => false,
                    'placeholder'  => 'Choisissez un Contrat',
                    'required'     => false,
                    'choices'      => $this->offerChoice->getSelectOfContracts(),
                    'autocomplete' => true,
                ],
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label'    => false,
                    'required' => false,
                    'choices'  => [
                        'Toutes les Offres Actives et Inactives' => null,
                        'Offres Actives'                         => true,
                        'Offres Inactives'                       => false,
                    ],
                    'placeholder'  => 'Choisissez une Visibilité',
                    'autocomplete' => true,
                ],
            )
            ->add(
                'validation',
                ChoiceType::class,
                [
                    'label'    => false,
                    'required' => false,
                    'choices'  => [
                        'Toutes les Offres Validées et Non Validées' => null,
                        'Offres Validées'                            => true,
                        'Offres Non Validées'                        => false,
                    ],
                    'placeholder'  => 'Choisissez une Validation',
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchOfferData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
