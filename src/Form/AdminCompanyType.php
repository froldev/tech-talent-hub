<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label'      => 'Nom de la Société: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'attr' => [
                        'class' => 'form-input w-full h-28 bg-transparent rounded'
                        . ' outline-none border border-gray-200 '
                        . 'focus:border-indigo-600 focus:ring-0',
                    ],
                    'label'      => 'Description:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'activitySector',
                TextType::class,
                [
                    'label'      => 'Secteur d"Activité:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'required' => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Company::class,
            ],
        );
    }
}
