<?php

namespace App\Form;

use App\Constant\JobConstants;
use App\Entity\Job;
use App\Form\Type\ActiveType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminJobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $isChild = (JobConstants::LEVEL_CHILD === $options['level']);

        if ($isChild) {
            $builder->add(
                'parent',
                EntityType::class,
                [
                    'class'         => Job::class,
                    'query_builder' => function (EntityRepository $er): QueryBuilder {
                        return $er->createQueryBuilder('j')
                            ->where('j.parent IS NULL')
                            ->orderBy('j.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'label'        => 'Catégorie: *',
                    'label_attr'   => [
                        'class' => 'class-label',
                    ],
                    'placeholder'  => 'Choisir une catégorie',
                    'autocomplete' => true,
                    'required'     => true,
                ],
            );
        }

        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'ts-control',
                    ],
                    'label'      => $isChild ? 'Métier: *' : 'Catégorie: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'countOffer',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'w-full ts-control bg-indigo-600 text-white opacity-50',
                    ],
                    'label'      => 'Total:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'disabled' => true,
                    'required' => false,
                ],
            )
            ->add(
                'active',
                ActiveType::class,
                [
                    'data_class' => Job::class,
                    'label'      => false,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Job::class,
                'level'      => null,
            ],
        );
        $resolver->setAllowedTypes('level', 'string');
    }
}
