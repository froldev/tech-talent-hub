<?php

namespace App\Form;

use App\Model\AdminSearchPostData;
use App\Util\PostChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminSearchPostType extends AbstractType
{
    public function __construct(
        private readonly PostChoice $postChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'q',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'Votre recherche',
                        'class'       => 'ts-control',
                    ],
                    'required' => false,
                ],
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label'        => false,
                    'placeholder'  => 'Veuillez choisir un type de Billet',
                    'required'     => false,
                    'choices'      => $this->postChoice->getListOfTypes(),
                    'autocomplete' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class'      => AdminSearchPostData::class,
                'method'          => 'GET',
                'crsf_protection' => false,
            ],
        );
    }
}
