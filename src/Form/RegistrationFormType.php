<?php

namespace App\Form;

use App\Constant\ParameterConstants;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    private const CHECKBOX = 'form-checkbox rounded border-gray-400'
        . ' text-indigo-600 focus:border-indigo-300 focus:ring focus:ring-offset-0'
        . ' focus:ring-indigo-200 focus:ring-opacity-50 me-2';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'fullname',
                TextType::class,
                [
                    'label'      => 'Nom complet: *',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'attr' => [
                        'placeholder' => 'Nom et Prénom ou Pseudo',
                        'class'       => 'ts-control',
                    ],
                    'required'    => true,
                    'constraints' => [
                        new Length(
                            [
                                'min'        => 3,
                                'minMessage' => 'Votre nom complet doit contenir au moins {{ limit }} caractères',
                                'max'        => 255,
                            ],
                        ),
                    ],
                    'help'      => 'Votre nom complet doit contenir au moins 3 caractères.',
                    'help_attr' => [
                        'class' => 'text-xs text-gray-500',
                    ],
                ],
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label'      => 'Email: *',
                    'label_attr' => [
                        'class' => 'font-semibold',
                    ],
                    'attr' => [
                        'placeholder' => 'Email',
                        'class'       => 'ts-control',
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'plainPassword',
                PasswordType::class,
                [
                    'mapped'     => false,
                    'label'      => 'Mot de Passe: *',
                    'label_attr' => [
                        'class' => 'font-semibold',
                    ],
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'placeholder'  => '••••••',
                        'class'        => 'ts-control',
                    ],
                    'required'    => true,
                    'constraints' => [
                        new Length(
                            [
                                'min'        => ParameterConstants::PASSWORD_MIN,
                                'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères',
                                'max'        => 4096,
                            ],
                        ),
                        new NotBlank(
                            [
                                'message' => 'Veuillez entrer un mot de passe',
                            ],
                        ),
                    ],
                    'help'      => 'Votre mot de passe doit '
                    . 'contenir au moins ' . ParameterConstants::PASSWORD_MIN
                    . ' caractères.',
                    'help_attr' => [
                        'class' => 'text-xs text-gray-500',
                    ],
                ],
            )
            ->add(
                'rgpd',
                CheckboxType::class,
                [
                    'label' => 'J"autorise ce site à conserver mes données'
                    . ' personnelles transmises via ce formulaire. Aucune'
                    . ' exploitation commerciale ne sera faite des données'
                    . ' conservées. *',
                    'label_attr' => [
                        'class' => 'form-check-label text-slate-400',
                    ],
                    'attr' => [
                        'class' => self::CHECKBOX,
                    ],
                    'required' => true,
                ],
            )
            ->add(
                'terms',
                CheckboxType::class,
                [
                    'mapped'     => false,
                    'label'      => 'J"accepte les Termes et Conditions. *',
                    'label_attr' => [
                        'class' => 'form-check-label text-slate-400',
                    ],
                    'attr' => [
                        'class' => self::CHECKBOX,
                    ],
                    'required' => true,
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ],
        );
    }
}
