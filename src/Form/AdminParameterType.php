<?php

namespace App\Form;

use App\Constant\ParameterConstants;
use App\Entity\Parameter as EntityParameter;
use App\Util\ParameterChoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminParameterType extends AbstractType
{
    public function __construct(
        private readonly ParameterChoice $parameterChoice,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $isEdit = $options['data'] && null !== $options['data']->getId();

        $builder
            ->add(
                'key',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'w-full ts-control bg-indigo-600 text-white opacity-50',
                    ],
                    'label'      => 'Nom du Paramètre:',
                    'label_attr' => [
                        'class' => 'class-label',
                    ],
                    'disabled' => true,
                    'required' => false,
                ],
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'placeholder' => 'Choisissez un type',
                    'choices'     => $this->parameterChoice->getListOfParameters(),
                    'label'       => 'Type: *',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'required'     => true,
                    'autocomplete' => true,
                    'disabled'     => $isEdit, // Désactiver le champ si en mode édition
                ],
            )
            ->add(
                'section',
                ChoiceType::class,
                [
                    'placeholder' => 'Choisissez une section',
                    'choices'     => $this->parameterChoice->getListOfSections(),
                    'label'       => 'Section: *',
                    'label_attr'  => [
                        'class' => 'class-label',
                    ],
                    'required'     => true,
                    'autocomplete' => true,
                ],
            )
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data && null !== $data->getType()) {
                        $type = $data->getType();

                        if (ParameterConstants::PARAMETER_STRING === $type) {
                            $form->add(
                                'value',
                                TextType::class,
                                [
                                    'required' => false,
                                ],
                            );
                        } elseif (ParameterConstants::PARAMETER_BOOLEAN === $type) {
                            $form->add(
                                'value',
                                ChoiceType::class,
                                [
                                    'placeholder' => 'Choisissez une valeur',
                                    'choices'     => [
                                        'actif'   => true,
                                        'inactif' => false,
                                    ],
                                    'label'      => 'Valeur: *',
                                    'label_attr' => [
                                        'class' => 'class-label',
                                    ],
                                    'required'     => false,
                                    'autocomplete' => true,
                                ],
                            );
                        } elseif (ParameterConstants::PARAMETER_INTEGER === $type) {
                            $form->add(
                                'value',
                                IntegerType::class,
                                [
                                    'required' => false,
                                ],
                            );
                        } elseif (ParameterConstants::PARAMETER_DATETIME === $type) {
                            $form->add(
                                'value',
                                DateTimeType::class,
                                [
                                    'required' => false,
                                    'widget'   => 'single_text',
                                    'input'    => 'string', // Stocker l'heure comme une chaîne
                                ],
                            );
                        } elseif (ParameterConstants::PARAMETER_TIME === $type) {
                            $form->add(
                                'value',
                                TimeType::class,
                                [
                                    'required' => false,
                                    'widget'   => 'single_text',
                                    'input'    => 'string', // Stocker le temps comme une chaîne
                                ],
                            );
                        }
                    }
                },
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => EntityParameter::class,
            ],
        );
    }
}
