<?php

namespace App\Trait;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(fields: ['name'], message: 'Ce nom existe déjà !')]
trait NameTrait
{
    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank(
        message: 'Le Nom ne doit pas être vide',
    )]
    private ?string $name = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }
}
