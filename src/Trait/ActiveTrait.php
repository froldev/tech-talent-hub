<?php

namespace App\Trait;

use Doctrine\ORM\Mapping as ORM;

trait ActiveTrait
{
    #[ORM\Column]
    private ?bool $active = false;

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active = false): static
    {
        $this->active = $active;

        return $this;
    }
}
