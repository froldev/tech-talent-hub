<?php

namespace App\Trait;

use Doctrine\ORM\Mapping as ORM;

trait CodeTrait
{
    #[ORM\Column(length: 10)]
    private ?string $code = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }
}
