<?php

namespace App\Scheduler;

use App\Constant\ParameterConstants;
use App\Message\AnalysisAndFetchOffersMessage;
use App\Repository\ParameterRepository;
use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;

#[AsSchedule('francetravailoffers')]
class AnalysisAndFetchOffersSchedulerProvider implements ScheduleProviderInterface
{
    public function __construct(
        private readonly ParameterRepository $parameterRepository,
    ) {
    }

    public function getSchedule(): Schedule
    {
        $parameter    = $this->parameterRepository->getValue(ParameterConstants::SCHEDULER_FETCH_OFFERS);
        $scheduleTime = $parameter ? $parameter : ParameterConstants::SCHEDULER_DEFAULT;

        return (new Schedule())->add(
            RecurringMessage::every('1 day', new AnalysisAndFetchOffersMessage(), from: $scheduleTime),
        );
    }
}
