<?php

namespace App\DataFixtures;

use App\Entity\OfferLike;
use App\Repository\OfferRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OfferLikeFixtures extends Fixture implements DependentFixtureInterface
{
    protected const NBR_OFFERSLIKES = 50;

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly OfferRepository $offerRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // OfferLikes
        for ($i = 1; $i <= self::NBR_OFFERSLIKES; ++$i) {
            $offerLike = (new OfferLike())
                ->setUserLike($faker->randomElement($this->userRepository->findAll()))
                ->setLikes($faker->randomElement($this->offerRepository->findAll()));
            $manager->persist($offerLike);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            SearchFixtures::class,
        ];
    }
}
