<?php

namespace App\DataFixtures;

use App\Service\NavbarService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class NavbarFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly NavbarService $navbarService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $this->navbarService->importLinksInNavbar();
    }

    public function getDependencies(): array
    {
        return [
            OfferLikeFixtures::class,
        ];
    }
}
