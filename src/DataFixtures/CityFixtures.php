<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Service\DepartmentService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CityFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly DepartmentService $departmentService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $nantes = [
            'code_departement' => '44',
            'zip_code'         => '44000',
            'name'             => 'Nantes',
            'label'            => '44 - Nantes',
        ];
        $paris = [
            'code_departement' => '75',
            'zip_code'         => '75000',
            'name'             => 'Paris',
            'label'            => '75 - Paris',
        ];
        $lyon = [
            'code_departement' => '69',
            'zip_code'         => '69000',
            'name'             => 'Lyon',
            'label'            => '69 - Lyon',
        ];
        $marseille = [
            'code_departement' => '13',
            'zip_code'         => '13000',
            'name'             => 'Marseille',
            'label'            => '13 - Marseille',
        ];
        $bordeaux = [
            'code_departement' => '33',
            'zip_code'         => '33000',
            'name'             => 'Bordeaux',
            'label'            => '33 - Bordeaux',
        ];

        $this->createCityEntity($nantes, $manager);
        $this->createCityEntity($paris, $manager);
        $this->createCityEntity($lyon, $manager);
        $this->createCityEntity($marseille, $manager);
        $this->createCityEntity($bordeaux, $manager);
    }

    private function createCityEntity(array $datas, ObjectManager $manager): City
    {
        $departement = $this->departmentService->getDepartmentByCode($datas['code_departement']);

        if (null === $departement) {
            throw new \Exception('Department not found');
        }
        $city = (new City())
            ->setDepartment($departement)
            ->setZipCode($datas['zip_code'])
            ->setName($datas['name'])
            ->setLabel($datas['label']);
        $manager->persist($city);
        $manager->flush();

        return $city;
    }

    public function getDependencies(): array
    {
        return [
            DepartmentFixtures::class,
        ];
    }
}
