<?php

namespace App\DataFixtures;

use App\Service\RegionService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RegionFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly RegionService $regionService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        // create regions
        $this->regionService->verifyRegionsAreImportedInDatabase();
    }

    public function getDependencies(): array
    {
        return [
            TagFixtures::class,
        ];
    }
}
