<?php

namespace App\DataFixtures;

use App\Entity\SoftSkill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SoftSkillFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->createSkillEntity(
            'Travailler en équipe',
            'Capacité à travailler et à se coordonner avec les autres au sein '
            . "de l'entreprise pour réaliser les objectifs fixés.",
            $manager,
        );
        $this->createSkillEntity(
            "Faire preuve d'autonomie",
            'Capacité à prendre en charge son activité sans devoir être encadré'
            . ' de façon continue (le cas échéant, à solliciter les autres'
            . " acteurs de l'entreprise).",
            $manager,
        );
        $this->createSkillEntity(
            'Faire preuve de rigueur et de précision',
            'Capacité à réaliser des tâches en suivant avec exactitude '
            . 'les règles, les procédures, les instructions qui ont été'
            . " fournies, sans réaliser d'erreur et à transmettre clairement"
            . ' des informations. Se montrer ponctuel et respectueux des'
            . ' règles de savoir-vivre usuelles.',
            $manager,
        );
    }

    private function createSkillEntity(string $name, string $description, ObjectManager $manager): SoftSkill
    {
        $skill = new SoftSkill();
        $skill->setName($name)
            ->setDescription($description);
        $manager->persist($skill);
        $manager->flush();

        return $skill;
    }

    public function getDependencies(): array
    {
        return [
            HardSkillFixtures::class,
        ];
    }
}
