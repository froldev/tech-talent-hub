<?php

namespace App\DataFixtures;

use App\Service\TagService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly TagService $tagService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        // create parent tags
        $this->tagService->importParentTag();

        // create children
        $this->tagService->importChildrenTag();
    }

    public function getDependencies(): array
    {
        return [
            FollowFixtures::class,
        ];
    }
}
