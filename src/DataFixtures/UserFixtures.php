<?php

namespace App\DataFixtures;

use App\Service\UserDataService;
use App\Service\UserService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function __construct(
        private readonly UserService $userService,
        private readonly UserDataService $userDataService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->userDataService->getListOfusers() as $userData) {
            $this->userService->createUser(
                $userData['email'],
                $userData['password'],
                $userData['fullname'],
                $userData['role'],
            );
        }
    }
}
