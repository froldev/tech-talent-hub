<?php

namespace App\DataFixtures;

use App\Entity\Offer;
use App\Repository\CityRepository;
use App\Repository\JobRepository;
use App\Util\OfferChoice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\String\Slugger\SluggerInterface;

class OfferFixtures extends Fixture implements DependentFixtureInterface
{
    public const NBR_OFFERS = 15;

    public function __construct(
        private readonly CityRepository $cityRepository,
        private readonly JobRepository $jobRepository,
        private readonly SluggerInterface $slugger,
        private readonly OfferChoice $choiceOffer,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Offers
        for ($i = 1; $i <= self::NBR_OFFERS; ++$i) {
            $title = 'A SUPPRIMER ' . $faker->sentence(rand(2, 5));
            $offer = (new Offer())
                ->setChannel($faker->randomElement($this->choiceOffer->getSelectChannels()))
                ->setReference($faker->word())
                ->setTitle($title)
                ->setSlug($this->slugger->slug($title))
                ->setCandidacyUrl($faker->url())
                ->setDescription(1 == rand(0, 1) ? $faker->paragraph() : null)
                ->setContract($faker->randomElement($this->choiceOffer->getSelectOfContracts()))
                ->setContractType($faker->word())
                ->setContractNature($faker->word())
                ->setExperience($faker->words(3, true))
                ->setSalary($faker->word())
                ->setSalaryCommentary(1 == rand(0, 1) ? $faker->sentence() : null)
                ->setSalaryComplement(1 == rand(0, 1) ? $faker->sentence() : null)
                ->setSalaryOther(1 == rand(0, 1) ? $faker->sentence() : null)
                ->setDuration($faker->word())
                ->setWorkTravel(1 == rand(0, 1) ? $faker->word() : null)
                ->setQualification(1 == rand(0, 1) ? $faker->word() : null)
                ->setAlternation(1 == rand(0, 1) ? true : false)
                ->setTelework(1 == rand(0, 1) ? true : false)
                ->setActive(1 == rand(0, 1) ? true : false)
                ->setValidation(1 == rand(0, 1) ? true : false)
                ->setCity($faker->randomElement($this->cityRepository->findAll()))
                ->setJob($faker->randomElement($this->jobRepository->findAll()));
            $manager->persist($offer);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            JobFixtures::class,
        ];
    }
}
