<?php

namespace App\DataFixtures;

use App\Entity\Partner;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PartnerFixtures extends Fixture implements DependentFixtureInterface
{
    public const NBR_PARTNER = 10;

    public function load(ObjectManager $manager): void
    {
        $franceTravail = [
            'name'        => 'France Travail',
            'url'         => 'https://www.francetravail.fr/',
            'description' => "France Travail est un site d'emploi généraliste "
            . "qui propose des offres d'emploi dans tous les secteurs d'activité.",
            'logo'     => 'https://www.francetravail.fr/logos/img/partenaires/francetravail.svg',
            'position' => 1,
            'count'    => mt_rand(0, 100),
        ];
        $apec = [
            'name'        => 'Apec',
            'url'         => 'https://www.apec.fr/',
            'description' => "l'Apec est le partenaire de référence des "
            . 'entreprises et des cadres en France. Elle leur propose des '
            . 'services et des conseils pour recruter et accompagner les'
            . ' cadres tout au long de leur carrière.',
            'logo'     => 'https://www.francetravail.fr/logos/img/partenaires/apec80.png',
            'position' => 2,
            'count'    => mt_rand(0, 100),
        ];
        $jobijoba = [
            'name'        => 'Jobijoba',
            'url'         => 'https://www.jobijoba.com/',
            'description' => "Jobijoba est un moteur de recherche d'emploi"
            . " qui permet de trouver des offres d'emploi en France et à"
            . " l'étranger. Il propose également des services pour les"
            . ' recruteurs.',
            'logo'     => 'https://www.francetravail.fr/logos/img/partenaires/jobijoba80.png',
            'position' => 3,
        ];
        $helloWork = [
            'name'        => 'HelloWork',
            'url'         => 'https://www.hellowork.com/',
            'description' => 'HelloWork est un groupe spécialisé dans les '
            . "services de l'emploi et de la formation. Il propose des "
            . "solutions  pour les recruteurs, les demandeurs d'emploi et"
            . ' les salariés.',
            'logo'     => 'https://www.pole-emploi.fr/logos/img/partenaires/hellowork80.png',
            'position' => 4,
            'count'    => mt_rand(0, 100),
        ];
        $monster = [
            'name'        => 'Monster',
            'url'         => 'https://www.monster.fr/',
            'description' => "Monster est un site d'emploi qui propose des "
            . "offres d'emploi dans tous les secteurs d'activité en France"
            . " et à l'étranger. Il propose également des services pour les"
            . ' recruteurs.',
            'logo'     => 'https://www.francetravail.fr/logos/img/partenaires/monster80.png',
            'position' => 5,
            'count'    => mt_rand(0, 100),
        ];

        $this->createPartnerEntity($franceTravail, $manager);
        $this->createPartnerEntity($apec, $manager);
        $this->createPartnerEntity($jobijoba, $manager);
        $this->createPartnerEntity($helloWork, $manager);
        $this->createPartnerEntity($monster, $manager);
    }

    private function createPartnerEntity(array $datas, ObjectManager $manager): Partner
    {
        $partner = (new Partner())
            ->setName($datas['name'])
            ->setUrl($datas['url'])
            ->setActive(true)
            ->setLogo($datas['logo'])
            ->setPosition($datas['position'])
            ->setCountOffer($datas['count'] ?? 0);
        $manager->persist($partner);
        $manager->flush();

        return $partner;
    }

    public function getDependencies(): array
    {
        return [
            OfferFixtures::class,
        ];
    }
}
