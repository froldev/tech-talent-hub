<?php

namespace App\DataFixtures;

use App\Entity\Follow;
use App\Repository\OfferCandidacyRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class FollowFixtures extends Fixture implements DependentFixtureInterface
{
    protected const NBR_FOLLOWS = 15;

    public function __construct(
        private readonly OfferCandidacyRepository $offerCandidacyRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Follows
        for ($i = 1; $i <= self::NBR_FOLLOWS; ++$i) {
            $follow = (new Follow())
                ->setComment($faker->sentence());
            for ($j = 1; $j <= rand(0, 5); ++$j) {
                $follow->setOfferCandidacy($faker->randomElement($this->offerCandidacyRepository->findAll()));
            }
            $manager->persist($follow);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            OfferCandidacyFixtures::class,
        ];
    }
}
