<?php

namespace App\DataFixtures;

use App\Entity\Search;
use App\Repository\CityRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SearchFixtures extends Fixture implements DependentFixtureInterface
{
    protected const NBR_SEARCHES = 50;
    private const CONTRACTS      = [
        'Cdi'                 => 'Cdi',
        'Cdd'                 => 'Cdd',
        'Freelance'           => 'LIB',
        'Mission Intérimaire' => 'MIS',
    ];

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly TagRepository $tagRepository,
        private readonly CityRepository $cityRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Searches
        for ($i = 1; $i <= self::NBR_SEARCHES; ++$i) {
            $search = (new Search())
                ->setName('A supprimer' . $faker->sentence())
                ->setContract(rand(0, 1) ? $faker->randomElement(self::CONTRACTS) : null)
                ->setTelework(rand(0, 1) ? true : false)
                ->setNewsletter(rand(0, 1) ? true : false)
                ->setTag($faker->randomElement($this->tagRepository->findAll()))
                ->setUserSearch($faker->randomElement($this->userRepository->findAll()))
                ->setCity($faker->randomElement($this->cityRepository->findAll()));
            $manager->persist($search);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CityFixtures::class,
        ];
    }
}
