<?php

namespace App\DataFixtures;

use App\Entity\HardSkill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HardSkillFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->createSkillEntity('Application web', $manager);
        $this->createSkillEntity('Concevoir une application web', $manager);
        $this->createSkillEntity("Surveiller le fonctionnement d'applicatifs et logiciels", $manager);
        $this->createSkillEntity('Rédiger un cahier des charges, des spécifications techniques', $manager);
        $this->createSkillEntity('Recueillir et analyser les besoins client', $manager);
    }

    private function createSkillEntity(string $name, ObjectManager $manager): HardSkill
    {
        $skill = new HardSkill();
        $skill->setName($name);
        $manager->persist($skill);
        $manager->flush();

        return $skill;
    }

    public function getDependencies(): array
    {
        return [
            CompanyFixtures::class,
        ];
    }
}
