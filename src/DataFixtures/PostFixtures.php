<?php

namespace App\DataFixtures;

use App\Service\PostService;
use App\Util\PostChoice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly PostService $postService,
        private readonly PostChoice $postChoice,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->postChoice->getListOfTypes() as $data) {
            $post = $this->postService->createOnePost($data);
            $manager->persist($post);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            NavbarFixtures::class,
        ];
    }
}
