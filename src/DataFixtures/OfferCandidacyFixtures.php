<?php

namespace App\DataFixtures;

use App\Entity\OfferCandidacy;
use App\Repository\OfferRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OfferCandidacyFixtures extends Fixture implements DependentFixtureInterface
{
    protected const NBR_CANDIDACIES = 25;

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly OfferRepository $offerRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $offers = $this->offerRepository->findAll();

        // OfferCandidacies
        for ($i = 1; $i <= self::NBR_CANDIDACIES; ++$i) {
            $candidacy = (new OfferCandidacy())
                ->setResponse($faker->sentence())
                ->setUserOfferCandidacy($faker->randomElement($this->userRepository->findAll()));
            for ($j = 1; $j <= rand(0, 1); ++$j) {
                $offer = $faker->randomElement($offers);
                $candidacy->addOffer($offer);
            }
            $manager->persist($candidacy);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            LanguageFixtures::class,
        ];
    }
}
