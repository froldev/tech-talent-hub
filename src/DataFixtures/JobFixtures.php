<?php

namespace App\DataFixtures;

use App\Constant\JobConstants;
use App\Entity\Job;
use App\Repository\JobRepository;
use App\Util\JobChoice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class JobFixtures extends Fixture implements DependentFixtureInterface
{
    public const LIST_OF_CHILDREN = [
        JobConstants::DEVELOPER => [
            'Développeur / Développeuse Big Data',
            'Développeur / Développeuse back-end',
            'Développeur / Développeuse d\'application',
            'Développeur / Développeuse front-end',
            'Développeur / Développeuse full-stack',
        ],
        JobConstants::DATA => [
            'Data analyst',
            'Data scientist',
            'Data manager',
        ],
        JobConstants::VIDEO_GAMES => [
            'Programmeur / Programmeuse - jeux vidéo',
        ],
        JobConstants::TESTER => [
            'Testeur / Testeuse informatique',
        ],
        JobConstants::AGILITY => [
            'Product Owner',
            'Scrum Master',
        ],
    ];

    public function __construct(
        private readonly JobRepository $jobRepository,
        private readonly JobChoice $jobChoice,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        // create parent job
        foreach ($this->jobChoice->getListOfParent() as $job) {
            $jobEntity = (new Job())
                ->setName($job)
                ->setActive(true)
                ->setParent(null)
                ->setCountOffer(rand(1, 100));
            $manager->persist($jobEntity);
        }
        $manager->flush();

        // create child job
        foreach (self::LIST_OF_CHILDREN as $key => $value) {
            $parent = $this->jobRepository->findOneBy(['name' => $key]);

            foreach ($value as $job) {
                $jobEntity = (new Job())
                    ->setName($job)
                    ->setActive(true)
                    ->setParent($parent)
                    ->setCountOffer(rand(1, 100));
                $manager->persist($jobEntity);
            }
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
