<?php

namespace App\DataFixtures;

use App\Service\DepartmentService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DepartmentFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly DepartmentService $departmentService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        // create regions
        $this->departmentService->createDepartments();
    }

    public function getDependencies(): array
    {
        return [
            RegionFixtures::class,
        ];
    }
}
