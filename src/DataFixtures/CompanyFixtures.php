<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture implements DependentFixtureInterface
{
    public const NBR_COMPANIES = 10;

    public function load(ObjectManager $manager): void
    {
        $seyos = [
            'name'           => 'Seyos',
            'description'    => 'Seyos est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Conseil pour les affaires et autres conseils de gestion',
        ];
        $adsearch = [
            'name'           => 'Adsearch',
            'description'    => 'Adsearch est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Activités des agences de placement de main-d"œuvre',
        ];
        $talan = [
            'name'           => 'Talan',
            'description'    => 'Talan est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Conseil pour les affaires et autres conseils de gestion',
        ];
        $providenceTravailTemporaire = [
            'name'        => 'Providence Travail Temporaire',
            'description' => 'Providence Travail Temporaire est une entreprise '
            . 'spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Activités des agences de travail temporaire',
        ];
        $tudiel = [
            'name'           => 'Tudiel',
            'description'    => 'Tudiel est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Ingénierie, études techniques',
        ];
        $systeam = [
            'name'           => 'Systeam',
            'description'    => 'Systeam est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Ingénierie, études techniques',
        ];
        $pharea = [
            'name'           => 'Pharea',
            'description'    => 'Pharea est une entreprise spécialisée dans le recrutement de profils IT.',
            'activitySector' => 'Conseil pour les affaires et autres conseils de gestion',
        ];

        $this->createCompanyEntity($seyos, $manager);
        $this->createCompanyEntity($adsearch, $manager);
        $this->createCompanyEntity($talan, $manager);
        $this->createCompanyEntity($providenceTravailTemporaire, $manager);
        $this->createCompanyEntity($tudiel, $manager);
        $this->createCompanyEntity($systeam, $manager);
        $this->createCompanyEntity($pharea, $manager);
    }

    private function createCompanyEntity(array $datas, ObjectManager $manager): Company
    {
        $company = (new Company())
            ->setName($datas['name'])
            ->setDescription($datas['description'])
            ->setActivitySector($datas['activitySector']);
        $manager->persist($company);
        $manager->flush();

        return $company;
    }

    public function getDependencies(): array
    {
        return [
            PartnerFixtures::class,
        ];
    }
}
