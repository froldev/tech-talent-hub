<?php

namespace App\DataFixtures;

use App\Entity\Language;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LanguageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->createLanguageEntity('Français', $manager);
        $this->createLanguageEntity('Anglais', $manager);
        $this->createLanguageEntity('Allemand', $manager);
        $this->createLanguageEntity('Espagnol', $manager);
        $this->createLanguageEntity('Italien', $manager);
    }

    private function createLanguageEntity(string $name, ObjectManager $manager): Language
    {
        $language = new Language();
        $language->setName($name);
        $manager->persist($language);
        $manager->flush();

        return $language;
    }

    public function getDependencies(): array
    {
        return [
            SoftSkillFixtures::class,
        ];
    }
}
