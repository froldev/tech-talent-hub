<?php

namespace App\DataFixtures;

use App\Service\ParameterService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParameterFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly ParameterService $parameterService,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $this->parameterService->importParameters();
    }

    public function getDependencies(): array
    {
        return [
            PostFixtures::class,
        ];
    }
}
