<?php

namespace App\Entity;

use App\Repository\FollowRepository;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FollowRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Follow
{
    use IdTrait;
    use DatetimeTrait;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(
        message: 'Le Commentaire ne doit pas être vide',
    )]
    private ?string $comment = null;

    #[ORM\ManyToOne(inversedBy: 'follow')]
    private ?OfferCandidacy $offerCandidacy = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getOfferCandidacy(): ?OfferCandidacy
    {
        return $this->offerCandidacy;
    }

    public function setOfferCandidacy(?OfferCandidacy $offerCandidacy): static
    {
        $this->offerCandidacy = $offerCandidacy;

        return $this;
    }
}
