<?php

namespace App\Entity;

use App\Repository\TagRepository;
use App\Trait\ActiveTrait;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use App\Trait\SlugTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: TagRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Vich\Uploadable]
class Tag
{
    use IdTrait;
    use NameTrait;
    use ActiveTrait;
    use SlugTrait;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $color = null;

    #[ORM\Column]
    private int $countOffer = 0;

    #[ORM\ManyToMany(targetEntity: Offer::class, inversedBy: 'tags')]
    private Collection $offers;

    #[ORM\OneToMany(mappedBy: 'tag', targetEntity: Search::class, orphanRemoval: true)]
    private Collection $search;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'tags')]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection $tags;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->search = new ArrayCollection();
        $this->tags   = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->slug = (new Slugify())->slugify($this->name . '-' . $this->id);
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        if (empty($this->slug)) {
            $this->slug = (new Slugify())->slugify($this->name . '-' . $this->id);
        }
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color = null): static
    {
        $this->color = $color;

        return $this;
    }

    public function getCountOffer(): int
    {
        return $this->countOffer;
    }

    public function setCountOffer(int $countOffer): static
    {
        $this->countOffer = $countOffer;

        return $this;
    }

    /**
     * @return Collection<int, Offer>
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offers): static
    {
        if (!$this->offers->contains($offers)) {
            $this->offers->add($offers);
        }

        return $this;
    }

    public function removeOffer(Offer $offers): static
    {
        $this->offers->removeElement($offers);

        return $this;
    }

    /**
     * @return Collection<int, Search>
     */
    public function getSearch(): Collection
    {
        return $this->search;
    }

    public function addSearch(Search $search): static
    {
        if (!$this->search->contains($search)) {
            $this->search->add($search);
            $search->setTag($this);
        }

        return $this;
    }

    public function removeSearch(Search $search): static
    {
        if ($this->search->removeElement($search)) {
            // set the owning side to null (unless already changed)
            if ($search->getTag() === $this) {
                $search->setTag(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(self $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->setParent($this);
        }

        return $this;
    }

    public function removeTag(self $tag): static
    {
        if ($this->tags->removeElement($tag)) {
            // set the owning side to null (unless already changed)
            if ($tag->getParent() === $this) {
                $tag->setParent(null);
            }
        }

        return $this;
    }
}
