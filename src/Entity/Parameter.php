<?php

namespace App\Entity;

use App\Repository\ParameterRepository;
use App\Trait\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ParameterRepository::class)]
class Parameter
{
    use IdTrait;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $key = null;

    #[ORM\Column(type: 'json')]
    #[Assert\NotBlank]
    private $value;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    private ?string $section = null;

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;

        return $this;
    }

    public function getValue()
    {
        switch ($this->type) {
            case 'datetime':
                if (is_string($this->value)) {
                    return \DateTime::createFromFormat('H:i:s', $this->value);
                }
                break;
            case 'string':
                return (string) $this->value;
            case 'boolean':
                return (bool) $this->value;
            case 'integer':
                return (int) $this->value;
            default:
                return $this->value;
        }

        return $this->value;
    }

    public function setValue($value): static
    {
        if (!is_string($value) && !is_bool($value) && !is_int($value) && !($value instanceof \DateTimeInterface)) {
            throw new \InvalidArgumentException('Invalid Value type.');
        }

        if ($value instanceof \DateTimeInterface) {
            $value = $value->format('H:i:s');
        }

        $this->value = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(string $section): static
    {
        $this->section = $section;

        return $this;
    }
}
