<?php

namespace App\Entity;

use App\Repository\UserRepository;
use App\Trait\ActiveTrait;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'Il existe déjà un compte avec cette email')]
#[ORM\EntityListeners(['App\EntityListener\UserListener'])]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use IdTrait;
    use ActiveTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 180, unique: true)]
    #[Assert\NotBlank(
        message: 'L\'Email ne doit pas être vide',
    )]
    #[Assert\Email(
        message: 'L\'e-mail {{ value }} n\'est pas un e-mail valide.',
    )]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: 'string')]
    private string $password;

    private ?string $plainPassword = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Veuillez entrer votre nom et prénom ou pseudo',
    )]
    private ?string $fullname = null;

    #[ORM\Column]
    private bool $rgpd = false;

    #[ORM\OneToMany(mappedBy: 'userSearch', targetEntity: Search::class, orphanRemoval: true)]
    private Collection $search;

    #[ORM\OneToMany(mappedBy: 'userLike', targetEntity: OfferLike::class, orphanRemoval: true)]
    private Collection $offerLikes;

    #[ORM\OneToMany(mappedBy: 'userOfferCandidacy', targetEntity: OfferCandidacy::class, orphanRemoval: true)]
    private Collection $offerCandidacy;

    public function __construct()
    {
        $this->search         = new ArrayCollection();
        $this->offerLikes     = new ArrayCollection();
        $this->offerCandidacy = new ArrayCollection();
        $this->createdAt      = new \DateTimeImmutable();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): static
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): static
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @return Collection<int, Search>
     */
    public function getSearches(): Collection
    {
        return $this->search;
    }

    public function addSearch(Search $search): static
    {
        if (!$this->search->contains($search)) {
            $this->search->add($search);
            $search->setUserSearch($this);
        }

        return $this;
    }

    public function removeSearch(Search $search): static
    {
        if ($this->search->removeElement($search)) {
            // set the owning side to null (unless already changed)
            if ($search->getUserSearch() === $this) {
                $search->setUserSearch(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OfferLike>
     */
    public function getOfferLikes(): Collection
    {
        return $this->offerLikes;
    }

    public function addOfferLike(OfferLike $offerLike): static
    {
        if (!$this->offerLikes->contains($offerLike)) {
            $this->offerLikes->add($offerLike);
            $offerLike->setUserLike($this);
        }

        return $this;
    }

    public function removeOfferLike(OfferLike $offerLike): static
    {
        if ($this->offerLikes->removeElement($offerLike)) {
            // set the owning side to null (unless already changed)
            if ($offerLike->getUserLike() === $this) {
                $offerLike->setUserLike(null);
            }
        }

        return $this;
    }

    public function isRgpd(): bool
    {
        return $this->rgpd;
    }

    public function setRgpd(bool $rgpd = false): static
    {
        $this->rgpd = $rgpd;

        return $this;
    }

    /**
     * @return Collection<int, OfferCandidacy>
     */
    public function getOfferCandidacies(): Collection
    {
        return $this->offerCandidacy;
    }

    public function addOfferCandidacy(OfferCandidacy $offerCandidacy): static
    {
        if (!$this->offerCandidacy->contains($offerCandidacy)) {
            $this->offerCandidacy->add($offerCandidacy);
            $offerCandidacy->setUserofferCandidacy($this);
        }

        return $this;
    }

    public function removeOfferCandidacy(OfferCandidacy $offerCandidacy): static
    {
        if ($this->offerCandidacy->removeElement($offerCandidacy)) {
            // set the owning side to null (unless already changed)
            if ($offerCandidacy->getUserOfferCandidacy() === $this) {
                $offerCandidacy->setUserOfferCandidacy(null);
            }
        }

        return $this;
    }
}
