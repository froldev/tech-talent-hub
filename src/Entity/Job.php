<?php

namespace App\Entity;

use App\Repository\JobRepository;
use App\Trait\ActiveTrait;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use App\Trait\SlugTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JobRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Job
{
    use IdTrait;
    use NameTrait;
    use ActiveTrait;
    use SlugTrait;

    #[ORM\OneToMany(mappedBy: 'job', targetEntity: Offer::class, orphanRemoval: true)]
    private Collection $offers;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'jobs', cascade: ['persist'])]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection $jobs;

    #[ORM\Column]
    private int $countOffer = 0;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->jobs   = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->slug = (new Slugify())->slugify($this->name);
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        if (empty($this->slug)) {
            $this->slug = (new Slugify())->slugify($this->name);
        }
    }

    /**
     * @return Collection<int, Offer>
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): static
    {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
            $offer->setJob($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): static
    {
        if ($this->offers->removeElement($offer)) {
            // set the owning side to null (unless already changed)
            if ($offer->getJob() === $this) {
                $offer->setJob(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(self $job): static
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs->add($job);
            $job->setParent($this);
        }

        return $this;
    }

    public function removeJob(self $job): static
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getParent() === $this) {
                $job->setParent(null);
            }
        }

        return $this;
    }

    public function getCountOffer(): ?int
    {
        return $this->countOffer;
    }

    public function setCountOffer(int $countOffer): static
    {
        $this->countOffer = $countOffer;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
