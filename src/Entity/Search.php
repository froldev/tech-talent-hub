<?php

namespace App\Entity;

use App\Repository\SearchRepository;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SearchRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Search
{
    use IdTrait;
    use NameTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contract = null;

    #[ORM\Column]
    private ?bool $telework = null;

    #[ORM\Column]
    private ?bool $newsletter = null;

    #[ORM\ManyToOne(inversedBy: 'search')]
    private ?User $userSearch = null;

    #[ORM\ManyToOne(inversedBy: 'search')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tag $tag = null;

    #[ORM\ManyToOne(inversedBy: 'search')]
    #[ORM\JoinColumn(nullable: false)]
    private ?City $city = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getContract(): ?string
    {
        return $this->contract;
    }

    public function setContract(?string $contract): static
    {
        $this->contract = $contract;

        return $this;
    }

    public function isTelework(): ?bool
    {
        return $this->telework;
    }

    public function settelework(bool $telework): static
    {
        $this->telework = $telework;

        return $this;
    }

    public function isNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): static
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getUserSearch(): ?User
    {
        return $this->userSearch;
    }

    public function setUserSearch(?User $userSearch): static
    {
        $this->userSearch = $userSearch;

        return $this;
    }

    public function getTag(): ?Tag
    {
        return $this->tag;
    }

    public function setTag(?Tag $tag): static
    {
        $this->tag = $tag;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): static
    {
        $this->city = $city;

        return $this;
    }
}
