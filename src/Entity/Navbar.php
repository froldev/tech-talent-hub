<?php

namespace App\Entity;

use App\Repository\NavbarRepository;
use App\Trait\ActiveTrait;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: NavbarRepository::class)]
#[UniqueEntity(fields: ['path'], message: 'Ce lien est déjà utilisé')]
#[ORM\HasLifecycleCallbacks]
class Navbar
{
    use IdTrait;
    use NameTrait;
    use ActiveTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank(
        message: 'Cet élément ne doit pas être vide',
    )]
    private ?string $path = null;

    #[ORM\Column]
    #[Assert\NotBlank(
        message: 'Cet élément ne doit pas être vide',
    )]
    private int $position = 0;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): static
    {
        $this->position = $position;

        return $this;
    }
}
