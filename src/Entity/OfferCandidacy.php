<?php

namespace App\Entity;

use App\Repository\OfferCandidacyRepository;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OfferCandidacyRepository::class)]
#[ORM\HasLifecycleCallbacks]
class OfferCandidacy
{
    use IdTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'La Réponse ne doit pas être vide',
    )]
    private ?string $response = null;

    #[ORM\ManyToMany(targetEntity: Offer::class, mappedBy: 'offerCandidacy')]
    private Collection $offers;

    #[ORM\ManyToOne(inversedBy: 'offerCandidacy')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $userOfferCandidacy = null;

    #[ORM\OneToMany(mappedBy: 'offerCandidacy', targetEntity: Follow::class, orphanRemoval: true)]
    private Collection $follow;

    public function __construct()
    {
        $this->offers    = new ArrayCollection();
        $this->follow    = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(string $response): static
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return Collection<int, Offer>
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): static
    {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
            $offer->addOfferCandidacy($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): static
    {
        if ($this->offers->removeElement($offer)) {
            $offer->removeOfferCandidacy($this);
        }

        return $this;
    }

    public function getUserOfferCandidacy(): ?User
    {
        return $this->userOfferCandidacy;
    }

    public function setUserOfferCandidacy(?User $userOfferCandidacy): static
    {
        $this->userOfferCandidacy = $userOfferCandidacy;

        return $this;
    }

    /**
     * @return Collection<int, Follow>
     */
    public function getFollow(): Collection
    {
        return $this->follow;
    }

    public function addFollow(Follow $follow): static
    {
        if (!$this->follow->contains($follow)) {
            $this->follow->add($follow);
            $follow->setOfferCandidacy($this);
        }

        return $this;
    }

    public function removeFollow(Follow $follow): static
    {
        if ($this->follow->removeElement($follow)) {
            // set the owning side to null (unless already changed)
            if ($follow->getOfferCandidacy() === $this) {
                $follow->setOfferCandidacy(null);
            }
        }

        return $this;
    }
}
