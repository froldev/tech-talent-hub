<?php

namespace App\Entity;

use App\Repository\DepartmentRepository;
use App\Trait\CodeTrait;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use App\Trait\SlugTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DepartmentRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Department
{
    use IdTrait;
    use NameTrait;
    use CodeTrait;
    use SlugTrait;

    /**
     * @var Collection<int, City>
     */
    #[ORM\OneToMany(mappedBy: 'department', targetEntity: City::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $cities;

    #[ORM\ManyToOne(targetEntity: Region::class, inversedBy: 'departments')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Region $region = null;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->slug = (new Slugify())->slugify($this->name);
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        if (empty($this->slug)) {
            $this->slug = (new Slugify())->slugify($this->name);
        }
    }

    /**
     * @return Collection<int, City>
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(City $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities->add($city);
            $city->setDepartment($this); // Associe la ville à ce département
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->cities->removeElement($city)) {
            // S'assure que la relation est bien supprimée du côté de la ville
            if ($city->getDepartment() === $this) {
                $city->setDepartment(null);
            }
        }

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): static
    {
        $this->region = $region;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
