<?php

namespace App\Entity;

use App\Repository\OfferLikeRepository;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OfferLikeRepository::class)]
#[ORM\HasLifecycleCallbacks]
class OfferLike
{
    use IdTrait;
    use DatetimeTrait;

    #[ORM\ManyToOne(inversedBy: 'offerLikes')]
    private ?User $userLike = null;

    #[ORM\ManyToOne(inversedBy: 'offerLikes')]
    private ?Offer $likes = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getUserLike(): ?User
    {
        return $this->userLike;
    }

    public function setUserLike(?User $userLike): static
    {
        $this->userLike = $userLike;

        return $this;
    }

    public function getLikes(): ?Offer
    {
        return $this->likes;
    }

    public function setLikes(?Offer $likes): static
    {
        $this->likes = $likes;

        return $this;
    }
}
