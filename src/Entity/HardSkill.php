<?php

namespace App\Entity;

use App\Repository\HardSkillRepository;
use App\Trait\IdTrait;
use App\Trait\NameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HardSkillRepository::class)]
#[ORM\HasLifecycleCallbacks]
class HardSkill
{
    use IdTrait;
    use NameTrait;

    #[ORM\ManyToMany(targetEntity: Offer::class, inversedBy: 'hardSkills')]
    private Collection $offers;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return Collection<int, Offer>
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): static
    {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): static
    {
        $this->offers->removeElement($offer);

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
