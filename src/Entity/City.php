<?php

namespace App\Entity;

use App\Repository\CityRepository;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use App\Trait\SlugTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CityRepository::class)]
#[UniqueEntity(fields: ['label'], message: 'Ce label existe déjà !')]
#[ORM\HasLifecycleCallbacks]
class City
{
    use IdTrait;
    use SlugTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Le Nom ne doit pas être vide',
    )]
    private ?string $name = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $label = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(
        min: 5,
        max: 5,
        minMessage: 'Le Code Postal doit faire {{ limit }} chiffres',
        maxMessage: 'Le Code Postal doit faire {{ limit }} chiffres',
    )]
    private ?string $zipCode = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $latitude = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $longitude = null;

    #[ORM\OneToMany(mappedBy: 'city', targetEntity: Offer::class, orphanRemoval: true)]
    private Collection $offers;

    #[ORM\OneToMany(mappedBy: 'city', targetEntity: Search::class, orphanRemoval: true)]
    private Collection $search;

    #[ORM\ManyToOne(targetEntity: Department::class, inversedBy: 'cities')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Department $department = null;

    public function __construct()
    {
        $this->offers    = new ArrayCollection();
        $this->search    = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->slug = (new Slugify())->slugify($this->name);

        if (empty($this->label)) {
            $this->label = $this->getDepartment()->getCode() . ' - ' . $this->getName();
        }
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        if (empty($this->slug)) {
            $this->slug = (new Slugify())->slugify($this->name);
        }

        if (empty($this->label)) {
            $this->label = $this->getDepartment()->getCode() . ' - ' . $this->getName();
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode = null): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): static
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): static
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection<int, Offer>
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): static
    {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
            $offer->setCity($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): static
    {
        if ($this->offers->removeElement($offer)) {
            // set the owning side to null (unless already changed)
            if ($offer->getCity() === $this) {
                $offer->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Search>
     */
    public function getSearch(): Collection
    {
        return $this->search;
    }

    public function addSearch(Search $search): static
    {
        if (!$this->search->contains($search)) {
            $this->search->add($search);
            $search->setCity($this);
        }

        return $this;
    }

    public function removeSearch(Search $search): static
    {
        if ($this->search->removeElement($search)) {
            // set the owning side to null (unless already changed)
            if ($search->getCity() === $this) {
                $search->setCity(null);
            }
        }

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department = null): static
    {
        $this->department = $department;

        return $this;
    }

    public function __toString(): string
    {
        return $this->label;
    }
}
