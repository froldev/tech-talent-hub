<?php

namespace App\Entity;

use App\Repository\OfferRepository;
use App\Trait\ActiveTrait;
use App\Trait\DatetimeTrait;
use App\Trait\IdTrait;
use App\Trait\SlugTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OfferRepository::class)]
#[UniqueEntity(fields: ['reference'])]
#[ORM\HasLifecycleCallbacks]
class Offer
{
    use IdTrait;
    use ActiveTrait;
    use SlugTrait;
    use DatetimeTrait;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Cet élément ne doit pas être vide',
    )]
    private ?string $channel = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Cet élément ne doit pas être vide',
    )]
    private ?string $title = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank(
        message: 'La Référence ne doit pas être vide',
    )]
    private string $reference;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $candidacyUrl = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contact = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contract = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contractType = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contractNature = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $experience = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $salary = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $salaryCommentary = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $salaryComplement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $salaryOther = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $duration = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $workTravel = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $qualification = null;

    #[ORM\Column]
    private bool $alternation = false;

    #[ORM\Column]
    private bool $telework = false;

    #[ORM\Column]
    private bool $validation = false;

    #[ORM\OneToMany(mappedBy: 'likes', targetEntity: OfferLike::class, orphanRemoval: true)]
    private Collection $offerLikes;

    #[ORM\ManyToOne(targetEntity: City::class, inversedBy: 'offers')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?City $city = null;

    #[ORM\ManyToOne(targetEntity: Job::class, inversedBy: 'offers')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?Job $job = null;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'offers')]
    private ?Company $company = null;

    #[ORM\ManyToMany(targetEntity: Partner::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $partners;

    #[ORM\ManyToMany(targetEntity: HardSkill::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $hardSkills;

    #[ORM\ManyToMany(targetEntity: SoftSkill::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $softSkills;

    #[ORM\ManyToMany(targetEntity: Tag::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $tags;

    #[ORM\ManyToMany(targetEntity: OfferCandidacy::class, inversedBy: 'offers', cascade: ['persist'])]
    private Collection $offerCandidacy;

    #[ORM\ManyToMany(targetEntity: Language::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $languages;

    #[ORM\ManyToMany(targetEntity: Formation::class, mappedBy: 'offers', cascade: ['persist'])]
    private Collection $formations;

    public function __construct()
    {
        $this->tags           = new ArrayCollection();
        $this->offerLikes     = new ArrayCollection();
        $this->offerCandidacy = new ArrayCollection();
        $this->partners       = new ArrayCollection();
        $this->hardSkills     = new ArrayCollection();
        $this->softSkills     = new ArrayCollection();
        $this->languages      = new ArrayCollection();
        $this->formations     = new ArrayCollection();
        $this->createdAt      = new \DateTimeImmutable();
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->slug = (new Slugify())->slugify($this->title . '-' . $this->reference);
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        if (empty($this->slug)) {
            $this->slug = (new Slugify())->slugify($this->title . '-' . $this->reference);
        }
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function setChannel(string $channel): static
    {
        $this->channel = $channel;

        return $this;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getCandidacyUrl(): ?string
    {
        return $this->candidacyUrl;
    }

    public function setCandidacyUrl(?string $candidacyUrl): static
    {
        $this->candidacyUrl = $candidacyUrl;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): static
    {
        $this->contact = $contact;

        return $this;
    }

    public function getContract(): ?string
    {
        return $this->contract;
    }

    public function setContract(?string $contract): static
    {
        $this->contract = $contract;

        return $this;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function setContractType(?string $contractType): static
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getContractNature(): ?string
    {
        return $this->contractNature;
    }

    public function setContractNature(?string $contractNature): static
    {
        $this->contractNature = $contractNature;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(?string $experience): static
    {
        $this->experience = $experience;

        return $this;
    }

    public function getSalary(): ?string
    {
        return $this->salary;
    }

    public function setSalary(?string $salary): static
    {
        $this->salary = $salary;

        return $this;
    }

    public function getSalaryCommentary(): ?string
    {
        return $this->salaryCommentary;
    }

    public function setSalaryCommentary(?string $salaryCommentary): static
    {
        $this->salaryCommentary = $salaryCommentary;

        return $this;
    }

    public function getSalaryComplement(): ?string
    {
        return $this->salaryComplement;
    }

    public function setSalaryComplement(?string $salaryComplement): static
    {
        $this->salaryComplement = $salaryComplement;

        return $this;
    }

    public function getSalaryOther(): ?string
    {
        return $this->salaryOther;
    }

    public function setSalaryOther(?string $salaryOther): static
    {
        $this->salaryOther = $salaryOther;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): static
    {
        $this->duration = $duration;

        return $this;
    }

    public function getWorkTravel(): ?string
    {
        return $this->workTravel;
    }

    public function setWorkTravel(?string $workTravel): static
    {
        $this->workTravel = $workTravel;

        return $this;
    }

    public function getQualification(): ?string
    {
        return $this->qualification;
    }

    public function setQualification(?string $qualification): static
    {
        $this->qualification = $qualification;

        return $this;
    }

    public function isAlternation(): bool
    {
        return $this->alternation;
    }

    public function setAlternation(bool $alternation = false): static
    {
        $this->alternation = $alternation;

        return $this;
    }

    public function isTelework(): bool
    {
        return $this->telework;
    }

    public function setTelework(bool $telework = false): static
    {
        $this->telework = $telework;

        return $this;
    }

    public function isValidation(): bool
    {
        return $this->validation;
    }

    public function setValidation(bool $validation = false): static
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->addOffer($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeOffer($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, OfferLike>
     */
    public function getOfferLikes(): Collection
    {
        return $this->offerLikes;
    }

    public function addOfferLike(OfferLike $offerLike): static
    {
        if (!$this->offerLikes->contains($offerLike)) {
            $this->offerLikes->add($offerLike);
            $offerLike->setLikes($this);
        }

        return $this;
    }

    public function removeOfferLike(OfferLike $offerLike): static
    {
        if ($this->offerLikes->removeElement($offerLike)) {
            // set the owning side to null (unless already changed)
            if ($offerLike->getLikes() === $this) {
                $offerLike->setLikes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OfferCandidacy>
     */
    public function getOfferCandidacy(): Collection
    {
        return $this->offerCandidacy;
    }

    public function addOfferCandidacy(OfferCandidacy $offerCandidacy): static
    {
        if (!$this->offerCandidacy->contains($offerCandidacy)) {
            $this->offerCandidacy->add($offerCandidacy);
        }

        return $this;
    }

    public function removeOfferCandidacy(OfferCandidacy $offerCandidacy): static
    {
        $this->offerCandidacy->removeElement($offerCandidacy);

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): static
    {
        $this->company = $company;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): static
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, HardSkill>
     */
    public function getPartners(): Collection
    {
        return $this->partners;
    }

    public function addPartner(Partner $partner): static
    {
        if (!$this->hardSkills->contains($partner)) {
            $this->partners->add($partner);
            $partner->addOffer($this);
        }

        return $this;
    }

    public function removePartner(Partner $partner): static
    {
        if ($this->partners->removeElement($partner)) {
            $partner->removeOffer($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, HardSkill>
     */
    public function getHardSkills(): Collection
    {
        return $this->hardSkills;
    }

    public function addHardSkill(HardSkill $hardSkill): static
    {
        if (!$this->hardSkills->contains($hardSkill)) {
            $this->hardSkills->add($hardSkill);
            $hardSkill->addOffer($this);
        }

        return $this;
    }

    public function removeHardSkill(HardSkill $hardSkill): static
    {
        if ($this->hardSkills->removeElement($hardSkill)) {
            $hardSkill->removeOffer($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, SoftSkill>
     */
    public function getSoftSkills(): Collection
    {
        return $this->softSkills;
    }

    public function addSoftSkill(SoftSkill $softSkill): static
    {
        if (!$this->softSkills->contains($softSkill)) {
            $this->softSkills->add($softSkill);
            $softSkill->addOffer($this);
        }

        return $this;
    }

    public function removeSoftSkill(SoftSkill $softSkill): static
    {
        if ($this->softSkills->removeElement($softSkill)) {
            $softSkill->removeOffer($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Language>
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(Language $language): static
    {
        if (!$this->languages->contains($language)) {
            $this->languages->add($language);
            $language->addOffer($this);
        }

        return $this;
    }

    public function removeLanguage(Language $language): static
    {
        if ($this->languages->removeElement($language)) {
            $language->removeOffer($this);
        }

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $job): static
    {
        $this->job = $job;

        return $this;
    }

    public function countTags(): int
    {
        return $this->tags->count();
    }

    public function countOfferLikes(): int
    {
        return $this->offerLikes->count();
    }

    public function countOfferCandidacies(): int
    {
        return $this->offerCandidacy->count();
    }

    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return Collection<int, Formation>
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): static
    {
        if (!$this->formations->contains($formation)) {
            $this->formations->add($formation);
            $formation->addOffer($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): static
    {
        if ($this->formations->removeElement($formation)) {
            $formation->removeOffer($this);
        }

        return $this;
    }
}
