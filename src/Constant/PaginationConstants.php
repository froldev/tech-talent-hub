<?php

namespace App\Constant;

class PaginationConstants
{
    public const ONE          = 1;
    public const TWO          = 2;
    public const THREE        = 3;
    public const FOUR         = 4;
    public const FIVE         = 5;
    public const SIX          = 6;
    public const SEVEN        = 7;
    public const EIGHT        = 8;
    public const NINE         = 9;
    public const TEN          = 10;
    public const TWELVE       = 12;
    public const FOURTEEN     = 14;
    public const SIXTEEN      = 16;
    public const EIGHTEEN     = 18;
    public const TWENTY       = 20;
    public const TWENTY_FOUR  = 24;
    public const FOURTY_EIGHT = 48;
}
