<?php

namespace App\Constant;

class TimeConstants
{
    public const CACHE_ONE_DAY   = 86400;
    public const CACHE_ONE_WEEK  = 604800;
    public const CACHE_ONE_MONTH = 2592000;
}
