<?php

namespace App\Constant;

class ParameterConstants
{
    public const PASSWORD_MIN = 8;
    public const MAX_DEPT     = 25;

    public const SECTION_PAGINATION = 'pagination';
    public const SECTION_API        = 'api';
    public const SECTION_SCHEDULER  = 'scheduler';
    public const SECTION_MODULE     = 'module';

    public const PAGINATION_DEFAULT = 'pagination_default';

    public const PARAMETER_STRING   = 'texte';
    public const PARAMETER_BOOLEAN  = 'booleen';
    public const PARAMETER_INTEGER  = 'chiffre';
    public const PARAMETER_DATETIME = 'date';
    public const PARAMETER_TIME     = 'heures';

    public const SCHEDULER_ANALYSE_PARTNERS = 'scheduler_analyse_partners';
    public const SCHEDULER_ANALYSE_TAGS     = 'scheduler_analyse_tags';
    public const SCHEDULER_ANALYSE_JOBS     = 'scheduler_analyse_jobs';
    public const SCHEDULER_ANALYSE_OFFERS   = 'scheduler_analyse_offers';
    public const SCHEDULER_FETCH_OFFERS     = 'scheduler_fetch_offers';
    public const SCHEDULER_DEFAULT          = 'scheduler_default';

    public const MODULE_SAAS = 'module_saas';

    public const HOMEPAGE_OFFERS_COUNT   = 'homepage_offers_count';
    public const HOMEPAGE_PARTNERS_COUNT = 'homepage_partners_count';
    public const HOMEPAGE_TAGS_COUNT     = 'homepage_tags_count';
    public const OFFERS_COUNT            = 'offers_count';
    public const PARTNERS_COUNT          = 'partners_count';
    public const TAGS_COUNT              = 'tags_count';
    public const SALARY_COUNT            = 'salary_count';
    public const TEST_COUNT              = 'test_count';
    public const PARAMETER_COUNT         = 'parameter_count';

    public const FRANCE_TRAVAIL_URL_TOKEN     = 'france_travail_url_token';
    public const FRANCE_TRAVAIL_URL_API       = 'france_travail_url_api';
    public const FRANCE_TRAVAIL_CLIENT_ID     = 'france_travail_client_id';
    public const FRANCE_TRAVAIL_CLIENT_SECRET = 'france_travail_client_secret';
    public const FRANCE_TRAVAIL_API_SCOPE     = 'france_travail_api_scope';

    public const BATCH_SIZE = 'batch_size';
}
