<?php

declare(strict_types=1);

namespace App\Constant;

class CacheConstants
{
    public const CACHE_OFFER_DETAIL    = 'offer_detail_';
    public const CACHE_OFFERS_LIST     = 'offers_list';
    public const CACHE_OFFERS_HOMEPAGE = 'offers_homepage';
    public const CACHE_ACCOUNT         = 'account_';
    public const CACHE_SALARY_LIST     = 'salary_list';
    public const CACHE_TEST_LIST       = 'test_list';
    public const CACHE_NAVBAR_LIST     = 'navbar_list';
}
