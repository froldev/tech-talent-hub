<?php

declare(strict_types=1);

namespace App\Constant;

class JobConstants
{
    public const LEVEL_PARENT = 'categories';
    public const LEVEL_CHILD  = 'metiers';

    public const DEVELOPER       = 'Développeur / Développeuse';
    public const DATA            = 'Data';
    public const VIDEO_GAMES     = 'Jeux vidéo';
    public const TESTER          = 'Testeur / Testeuse';
    public const AGILITY         = 'Agilité';
    public const ANALYST         = 'Analyste';
    public const PROJECT_MANAGER = 'Chef de projet';
    public const TO_DEFINE       = 'A Définir';
    public const EXCLUDE         = 'A Exclure';

    public const MAX_MINI = 10;
}
