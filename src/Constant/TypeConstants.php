<?php

namespace App\Constant;

class TypeConstants
{
    public const ACTIVE   = 'active';
    public const INACTIVE = 'inactive';
    public const TRUE     = true;
    public const FALSE    = false;
}
