<?php

declare(strict_types=1);

namespace App\Constant;

class ColorConstants
{
    public const BLUE    = 'blue-600';
    public const CYAN    = 'cyan-500';
    public const DARK    = 'gray-800';
    public const EMERALD = 'emerald-600';
    public const GREEN   = 'green-600';
    public const INDIGO  = 'indigo-600';
    public const LIGHT   = 'gray-50';
    public const ORANGE  = 'orange-600';
    public const PURPLE  = 'purple-600';
    public const RED     = 'red-600';
    public const GRAY    = 'gray-500';
    public const SKY     = 'sky-500';
    public const YELLOW  = 'yellow-500';
}
