<?php

namespace App\Constant;

class TagConstants
{
    public const BACK       = 'Back-end';
    public const FRONT      = 'Front-end';
    public const MOBILE     = 'Mobile';
    public const DEVOPS     = 'DevOps';
    public const DATA       = 'Data';
    public const DATABASE   = 'Database';
    public const TEST       = 'Test';
    public const CLOUD      = 'Cloud';
    public const TOOLING    = 'Tooling';
    public const MANAGEMENT = 'Gestion de projet';
    public const SECURITY   = 'Sécurité';
    public const OTHERS     = 'Autres';
    public const TO_DEFINE  = 'A Définir';
    public const EXCLUDE    = 'A Exclure';

    public const BACK_DESCRIPTION       = 'Pour les accros à la technique et des belles architectures.';
    public const FRONT_DESCRIPTION      = 'Pour ceux qui apprécient visualiser leur travail !';
    public const MOBILE_DESCRIPTION     = 'Les passionnés des smartphones trouveront leur bonheur ici.';
    public const DEVOPS_DESCRIPTION     = "Pour les adeptes de l'automatisation et de la mise en production.";
    public const DATA_DESCRIPTION       = 'Pour les amoureux des chiffres et des données.';
    public const DATABASE_DESCRIPTION   = 'Pour les passionnés de la gestion de bases de données.';
    public const TEST_DESCRIPTION       = 'Pour ceux qui aiment tester et valider leur travail.';
    public const CLOUD_DESCRIPTION      = "Pour les adeptes du cloud et de l'infrastructure.";
    public const TOOLING_DESCRIPTION    = "Pour les passionnés de l'automatisation et de l'industrialisation.";
    public const MANAGEMENT_DESCRIPTION = 'Pour les chefs de projet et les managers.';
    public const SECURITY_DESCRIPTION   = 'Pour les passionnés de la sécurité informatique.';
    public const OTHERS_DESCRIPTION     = 'Pour tout ce qui peut venir vous aider à travailler plus sereinement.';
    public const TO_DEFINE_DESCRIPTION  = 'A définir';
    public const EXCLUDE_DESCRIPTION    = 'A exclure';
}
