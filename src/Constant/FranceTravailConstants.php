<?php

declare(strict_types=1);

namespace App\Constant;

class FranceTravailConstants
{
    public const NAME        = 'FRANCE TRAVAIL';
    public const DESCRIPTION = 'Premier acteur du marché du travail en France'
        . ' avec plus de 58 000 collaborateurs et plus de 890 agences et relais de'
        . ' proximité ainsi qu’un réseau de partenaires sur l’ensemble du'
        . ' territoire, France Travail œuvre au quotidien pour faciliter le retour'
        . ' à l’emploi des demandeurs et offrir aux entreprises des réponses'
        . ' adaptées à leurs besoins de recrutement.';
    public const URL  = 'https://www.francetravail.fr/';
    public const LOGO = 'https://www.pole-emploi.fr/logos/img/partenaires/francetravail.svg';
}
