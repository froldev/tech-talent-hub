<?php

namespace App\Constant;

class PageOriginConstants
{
    public const ORIGIN_OFFER   = 'offre';
    public const ORIGIN_PARTNER = 'partenaire';
    public const ORIGIN_TAG     = 'technologie';
    public const ORIGIN_HOME    = 'accueil';
}
