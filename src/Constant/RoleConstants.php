<?php

namespace App\Constant;

class RoleConstants
{
    public const ADMIN = 'ROLE_ADMIN';
    public const USER  = 'ROLE_USER';
    public const ROLES = [
        'Administrateur' => self::ADMIN,
        'Utilisateur'    => self::USER,
    ];
}
