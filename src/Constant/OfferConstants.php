<?php

namespace App\Constant;

class OfferConstants
{
    public const TECHNOLOGY  = 'Technologies';
    public const FORMATION   = 'Formation';
    public const HARDSKILLS  = 'Hardskills';
    public const SOFTSKILLS  = 'Softskills';
    public const EXPERIENCE  = 'Expérience';
    public const STATUS      = 'Statut';
    public const LANGUAGES   = 'Langues';
    public const DEPLACEMENT = 'Déplacements';
    public const DURATION    = 'Durée';

    public const APEC                  = 'APEC';
    public const INDEED                = 'INDEED';
    public const MONSTER               = 'MONSTER';
    public const HELLOWORK             = 'HELLOWORK';
    public const METEOJOB              = 'METEOJOB';
    public const LINKEDIN              = 'LINKEDIN';
    public const WE_LOVE_DEVS          = 'WE-LOVE-DEVS';
    public const WELCOME_TO_THE_JUNGLE = 'WELCOME-TO-THE-JUNGLE';
    public const TELEPHONE             = 'TELEPHONE';
    public const RESEAU                = 'RESEAU';
    public const AUTRE                 = 'AUTRE';
}
