<?php

declare(strict_types=1);

namespace App\Constant;

class ApiConstants
{
    public const API_CHANNEL              = 'channel';
    public const DEPARTMENT_CODE          = 'departmentCode';
    public const API_FRANCE_TRAVAIL_TOKEN = 'https://entreprise.francetravail.fr/'
        . 'connexion/oauth2/access_token?realm=/partenaire';
    public const API_FRANCE_TRAVAIL_URL = 'https://api.francetravail.io/'
        . 'partenaire/offresdemploi/v2/offres/search?codeROME=M1805&inclureLimitrophes=false';
    public const API_FRANCE_TRAVAIL_SCOPE = 'o2dsoffre api_offresdemploiv2';
    public const API_BATCH_SIZE           = 10;
}
