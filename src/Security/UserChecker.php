<?php

namespace App\Security;

use App\Entity\User as AppUser;
use App\Service\EmailService;
use App\Service\JWTService;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function __construct(
        private readonly EmailService $emailService,
        private JWTService $jwtService,
    ) {
    }

    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if (!$user->isActive()) {
            // send email
            $this->emailService->sendEmail(
                'contact@techtalenthub.fr',
                $user->getEmail(),
                'Activation de votre compte sur le site TechTalentHub',
                'register',
                [
                    'user'  => $user,
                    'token' => $this->jwtService->generateToken($user, $_ENV['JWT_SECRET']),
                ],
            );
            // the message passed to this exception is meant to be displayed to the user
            throw new CustomUserMessageAccountStatusException('Un email de confirmation vous a été envoyé.');
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }
    }
}
