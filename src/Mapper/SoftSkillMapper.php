<?php

namespace App\Mapper;

use App\DTO\SoftSkillDTO;
use App\Entity\SoftSkill;
use Doctrine\ORM\EntityManagerInterface;

class SoftSkillMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): SoftSkill
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to entity.
     */
    private static function fromApiData(array $apiData): SoftSkillDTO
    {
        return new SoftSkillDTO(
            name: $apiData['libelle'],
            description: $apiData['description'],
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(SoftSkillDTO $dto): SoftSkill
    {
        $softSkill = (new SoftSkill())
            ->setName($dto->getName())
            ->setDescription($dto->getDescription());

        $this->entityManager->persist($softSkill);

        return $softSkill;
    }
}
