<?php

namespace App\Mapper;

use App\DTO\HardSkillDTO;
use App\Entity\HardSkill;
use Doctrine\ORM\EntityManagerInterface;

class HardSkillMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): HardSkill
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to entity.
     */
    private static function fromApiData(array $apiData): HardSkillDTO
    {
        return new HardSkillDTO(
            name: $apiData['libelle'],
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(HardSkillDTO $dto): HardSkill
    {
        $hardSkill = (new HardSkill())
            ->setName($dto->getName());

        $this->entityManager->persist($hardSkill);

        return $hardSkill;
    }
}
