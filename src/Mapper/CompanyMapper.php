<?php

namespace App\Mapper;

use App\DTO\CompanyDTO;
use App\Entity\Company;
use App\Service\ApiDataExtractorService;
use Doctrine\ORM\EntityManagerInterface;

class CompanyMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly ApiDataExtractorService $apiDataExtractorService,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): Company
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to entity.
     */
    private function fromApiData(array $apiData): CompanyDTO
    {
        return new CompanyDTO(
            name: $apiData['entreprise']['nom'] ?? 'Nom non disponible',
            description: $this->apiDataExtractorService->getDataInArray('description', $apiData['entreprise']),
            activitySector: $this->apiDataExtractorService->getDataInArray('secteurActiviteLibelle', $apiData),
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(CompanyDTO $companyDTO): Company
    {
        $company = (new Company())
            ->setName($companyDTO->getName())
            ->setDescription($companyDTO->getDescription())
            ->setActivitySector($companyDTO->getActivitySector());

        $this->entityManager->persist($company);

        return $company;
    }
}
