<?php

namespace App\Mapper;

use App\Constant\TagConstants;
use App\DTO\JobDTO;
use App\Entity\Job;
use App\Repository\JobRepository;
use Doctrine\ORM\EntityManagerInterface;

class JobMapper
{
    public function __construct(
        private readonly JobRepository $jobRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): Job
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to entity.
     */
    private static function fromApiData(array $apiData): JobDTO
    {
        return new JobDTO(
            name: $apiData['appellationlibelle'] ?? 'Nom du Métier indisponible',
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(JobDTO $dto): Job
    {
        $parent = $this->jobRepository->findJobByName(TagConstants::EXCLUDE);

        $job = (new Job())
            ->setName($dto->getName())
            ->setParent($parent)
            ->setActive(true);

        $this->entityManager->persist($job);

        return $job;
    }
}
