<?php

namespace App\Mapper;

use App\DTO\FormationDTO;
use App\Entity\Formation;
use Doctrine\ORM\EntityManagerInterface;

class FormationMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): Formation
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to entity.
     */
    private static function fromApiData(array $apiData): FormationDTO
    {
        return new FormationDTO(
            name: $apiData['niveauLibelle'],
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(FormationDTO $dto): Formation
    {
        $formation = (new Formation())
            ->setName($dto->getName());

        $this->entityManager->persist($formation);

        return $formation;
    }
}
