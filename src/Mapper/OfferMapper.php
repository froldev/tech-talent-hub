<?php

namespace App\Mapper;

use App\Constant\FranceTravailConstants;
use App\DTO\OfferDTO;
use App\Entity\City;
use App\Entity\Company;
use App\Entity\Job;
use App\Entity\Offer;
use Doctrine\ORM\EntityManagerInterface;

class OfferMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData, ?Job $job, ?City $city, ?Company $company): Offer
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto, $job, $city, $company);
    }

    /**
     * Map from DTO to entity.
     */
    private static function fromApiData(array $apiData): OfferDTO
    {
        return new OfferDTO(
            channel: FranceTravailConstants::NAME,
            title: $apiData['intitule']         ?? 'Titre indisponible',
            reference: $apiData['id']           ?? 'Référence indisponible',
            alternation: $apiData['alternance'] ?? false,
            telework: $apiData['teletravail']   ?? false,
            createdAt: isset($apiData['dateCreation'])
                        ? new \DateTimeImmutable($apiData['dateCreation'])
                        : new \DateTimeImmutable(),
            candidacyUrl: self::getCandidacyUrl($apiData)        ?? null,
            description: $apiData['description']                 ?? null,
            contact: $apiData['contact']['nom']                  ?? null,
            contract: $apiData['typeContrat']                    ?? null,
            contractType: $apiData['typeContratLibelle']         ?? null,
            contractNature: $apiData['natureContrat']            ?? null,
            experience: $apiData['experienceLibelle']            ?? null,
            salary: $apiData['salaire']['libelle']               ?? null,
            salaryCommentary: $apiData['salaire']['commentaire'] ?? null,
            salaryComplement: $apiData['salaire']['complement1'] ?? null,
            salaryOther: $apiData['salaire']['complement2']      ?? null,
            duration: $apiData['dureeTravailLibelle']            ?? null,
            workTravel: $apiData['deplacement']                  ?? null,
            qualification: $apiData['qualificationLibelle']      ?? null,
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(OfferDTO $dto, ?Job $job, ?City $city, ?Company $company): Offer
    {
        $offer = (new Offer())
            ->setChannel($dto->getChannel())
            ->setTitle($dto->getTitle())
            ->setReference($dto->getReference())
            ->setCandidacyUrl($dto->getCandidacyUrl())
            ->setDescription($dto->getDescription())
            ->setContact($dto->getContact())
            ->setContract($dto->getContract())
            ->setContractType($dto->getContractType())
            ->setContractNature($dto->getContractNature())
            ->setExperience($dto->getExperience())
            ->setAlternation($dto->isAlternation())
            ->setTelework($dto->isTelework())
            ->setSalary($dto->getSalary())
            ->setSalaryCommentary($dto->getSalaryCommentary())
            ->setSalaryComplement($dto->getSalaryComplement())
            ->setSalaryOther($dto->getSalaryOther())
            ->setDuration($dto->getDuration())
            ->setWorkTravel($dto->getWorkTravel())
            ->setQualification($dto->getQualification())
            ->setCreatedAt($dto->getCreatedAt());

        if (null !== $job) {
            $offer->setJob($job);
        }

        if (null !== $city) {
            $offer->setCity($city);
        }

        if (null !== $company) {
            $offer->setCompany($company);
        }

        $this->entityManager->persist($offer);

        return $offer;
    }

    /**
     * Function to get Candidacy URL.
     */
    private static function getCandidacyUrl(array $datas): ?string
    {
        // find first urlPostulation or coordonnees1
        if (array_key_exists('contact', $datas) && !empty($datas['contact'])) {
            $contact = $datas['contact'];

            if (!empty($contact['urlPostulation'])) {
                return self::modifyUrlCandidacy($contact['urlPostulation']);
            } elseif (!empty($datas['coordonnees1'])) {
                return self::modifyUrlCandidacy($datas['coordonnees1']);
            }
        }

        // if empty, find urlOrigine or url
        if (array_key_exists('origineOffre', $datas) && !empty($datas['origineOffre'])) {
            $partners = $datas['origineOffre']['partenaires'] ?? [];

            foreach ($partners as $partner) {
                if (!empty($partner['url'])) {
                    return self::modifyUrlCandidacy($partner['url']);
                }
            }

            return self::modifyUrlCandidacy($datas['origineOffre']['urlOrigine'] ?? null);
        }

        return null;
    }

    /**
     * Function to modify the URL candidacy.
     */
    private static function modifyUrlCandidacy(string $url): string
    {
        // Remove unnecessary text
        return str_replace('Pour postuler, utiliser le lien suivant : ', '', $url);
    }
}
