<?php

namespace App\Mapper;

use App\DTO\LanguageDTO;
use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;

class LanguageMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): Language
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to API data.
     */
    private function fromApiData(array $apiData): LanguageDTO
    {
        return new LanguageDTO(
            name: $apiData['libelle'],
        );
    }

    /**
     * Map from DTO to entity.
     */
    public function toEntity(LanguageDTO $dto): Language
    {
        $entity = (new Language())
            ->setName($dto->getName());

        $this->entityManager->persist($entity);

        return $entity;
    }
}
