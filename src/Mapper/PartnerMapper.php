<?php

namespace App\Mapper;

use App\DTO\PartnerDTO;
use App\Entity\Partner;
use App\Service\PartnerService;
use Doctrine\ORM\EntityManagerInterface;

class PartnerMapper
{
    public function __construct(
        private readonly PartnerService $partnerService,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData): Partner
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto);
    }

    /**
     * Map from DTO to API data.
     */
    private function fromApiData(array $apiData): PartnerDTO
    {
        return new PartnerDTO(
            name: $apiData['nom'],
            url: $apiData['url']                 ?? null,
            logo: $apiData['logo']               ?? null,
            description: $apiData['description'] ?? null,
        );
    }

    /**
     * Map from DTO to entity.
     */
    public function toEntity(PartnerDTO $dto): Partner
    {
        $url      = $this->partnerService->modifyUrlPartner($dto->getUrl(), $dto->getName());
        $position = $this->partnerService->getMaxPosition() + 1;

        $entity = (new Partner())
            ->setName($dto->getName())
            ->setUrl($url)
            ->setLogo($dto->getLogo())
            ->setDescription($dto->getDescription())
            ->setPosition($position)
            ->setActive(true);

        $this->entityManager->persist($entity);

        return $entity;
    }
}
