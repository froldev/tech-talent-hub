<?php

namespace App\Mapper;

use App\DTO\CityDTO;
use App\Entity\City;
use App\Service\ApiDataExtractorService;
use App\Service\CityService;
use App\Service\DepartmentService;
use Doctrine\ORM\EntityManagerInterface;

class CityMapper
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly CityService $cityService,
        private readonly DepartmentService $departmentService,
        private readonly ApiDataExtractorService $apiDataExtractorService,
    ) {
    }

    /**
     * Map from API data to DTO.
     */
    public function mapFromApiData(array $apiData, ?string $departmentCode): City
    {
        $dto = $this->fromApiData($apiData);

        return $this->toEntity($dto, $departmentCode);
    }

    /**
     * Map from DTO to entity.
     */
    private function fromApiData(array $apiData): CityDTO
    {
        return new CityDTO(
            label: $this->apiDataExtractorService->getDataInArray(
                'libelle',
                $apiData['lieuTravail'],
            ) ?? 'Libellé non disponible',
            zipCode: $this->apiDataExtractorService->getDataInArray(
                'codePostal',
                $apiData['lieuTravail'],
            ) ?? 'Code Postal non disponible',
            latitude: $this->apiDataExtractorService->getDataInArray(
                'latitude',
                $apiData['lieuTravail'],
            ) ?? 'Latitude non disponible',
            longitude: $this->apiDataExtractorService->getDataInArray(
                'longitude',
                $apiData['lieuTravail'],
            ) ?? 'Longitude non disponible',
        );
    }

    /**
     * Map from DTO to entity.
     */
    private function toEntity(CityDTO $dto, string $departmentCode): City
    {
        $department = $this->departmentService->getDepartmentByCode($departmentCode);

        if (!$department) {
            throw new \Exception("Le département avec le code $departmentCode n'existe pas.");
        }

        $name  = $this->cityService->getCityLabel($dto->getLabel());
        $label = $dto->getZipCode() . ' - ' . $name;

        $city = (new City())
            ->setName($name)
            ->setLabel($label)
            ->setZipCode($dto->getZipCode())
            ->setDepartment($department)
            ->setLatitude($dto->getLatitude())
            ->setLongitude($dto->getLongitude());

        $this->entityManager->persist($city);

        return $city;
    }
}
