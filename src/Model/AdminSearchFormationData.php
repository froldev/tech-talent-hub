<?php

namespace App\Model;

class AdminSearchFormationData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
