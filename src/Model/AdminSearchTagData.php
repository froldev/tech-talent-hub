<?php

namespace App\Model;

class AdminSearchTagData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';

    public ?string $parent = '';

    public ?bool $active = null;
}
