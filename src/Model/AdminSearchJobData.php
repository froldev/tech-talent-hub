<?php

namespace App\Model;

class AdminSearchJobData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';

    public ?string $parent = '';

    public ?bool $active = null;
}
