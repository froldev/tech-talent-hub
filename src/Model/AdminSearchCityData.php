<?php

namespace App\Model;

class AdminSearchCityData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';

    public ?string $department = '';
}
