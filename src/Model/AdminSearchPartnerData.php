<?php

namespace App\Model;

class AdminSearchPartnerData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
