<?php

namespace App\Model;

class AdminSearchUserData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
