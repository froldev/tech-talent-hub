<?php

namespace App\Model;

class AdminSearchParameterData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
