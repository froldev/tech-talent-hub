<?php

namespace App\Model;

class AdminSearchSoftSkillData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
