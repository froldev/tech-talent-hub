<?php

namespace App\Model;

class AdminSearchCompanyData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
