<?php

namespace App\Model;

class AdminSearchNavbarData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
