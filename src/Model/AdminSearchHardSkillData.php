<?php

namespace App\Model;

class AdminSearchHardSkillData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
