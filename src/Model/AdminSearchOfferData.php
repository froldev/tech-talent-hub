<?php

namespace App\Model;

class AdminSearchOfferData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';

    public ?array $tags = [];

    public ?string $department = '';

    public ?string $contract = '';

    public ?bool $active = null;

    public ?bool $validation = null;
}
