<?php

namespace App\Model;

class AdminSearchTagTypeData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
