<?php

namespace App\Model;

class AdminSearchLanguageData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';
}
