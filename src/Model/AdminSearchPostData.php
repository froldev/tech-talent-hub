<?php

namespace App\Model;

class AdminSearchPostData
{
    /**
     * @var int
     */
    public $page = 1;

    public ?string $q = '';

    public ?string $type = '';
}
