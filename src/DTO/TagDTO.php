<?php

namespace App\DTO;

class TagDTO
{
    public function __construct(
        private string $name,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }
}
