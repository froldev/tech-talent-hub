<?php

namespace App\DTO;

class CompanyDTO
{
    public function __construct(
        private string $name,
        private ?string $description = null,
        private ?string $activitySector = null,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getActivitySector(): ?string
    {
        return $this->activitySector;
    }
}
