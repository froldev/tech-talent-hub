<?php

namespace App\DTO;

class SoftSkillDTO
{
    public function __construct(
        private string $name,
        private ?string $description = null,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
