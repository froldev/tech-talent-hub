<?php

namespace App\DTO;

class PartnerDTO
{
    public function __construct(
        private string $name,
        private ?string $url = null,
        private ?string $logo = null,
        private ?string $description = null,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }
}
