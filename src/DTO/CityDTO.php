<?php

namespace App\DTO;

class CityDTO
{
    public function __construct(
        private string $label,
        private ?string $zipCode,
        private ?string $latitude = null,
        private ?string $longitude = null,
    ) {
    }

    // Getters for all properties
    public function getLabel(): string
    {
        return $this->label;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }
}
