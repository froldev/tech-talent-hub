<?php

namespace App\DTO;

class JobDTO
{
    public function __construct(
        private readonly string $name,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }
}
