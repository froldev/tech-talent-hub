<?php

namespace App\DTO;

class LanguageDTO
{
    public function __construct(
        private string $name,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }
}
