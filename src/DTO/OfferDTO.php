<?php

namespace App\DTO;

class OfferDTO
{
    public function __construct(
        private string $channel,
        private string $title,
        private string $reference,
        private \DateTimeImmutable $createdAt,
        private bool $alternation = false,
        private bool $telework = false,
        private ?string $candidacyUrl = null,
        private ?string $description = null,
        private ?string $contract = null,
        private ?string $contractType = null,
        private ?string $contractNature = null,
        private ?string $experience = null,
        private ?string $contact = null,
        private ?string $salary = null,
        private ?string $salaryCommentary = null,
        private ?string $salaryComplement = null,
        private ?string $salaryOther = null,
        private ?string $duration = null,
        private ?string $workTravel = null,
        private ?string $qualification = null,
    ) {
    }

    // Getters for all properties
    public function getChannel(): string
    {
        return $this->channel;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isAlternation(): bool
    {
        return $this->alternation;
    }

    public function isTelework(): bool
    {
        return $this->telework;
    }

    public function getCandidacyUrl(): ?string
    {
        return $this->candidacyUrl;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getContract(): ?string
    {
        return $this->contract;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function getContractNature(): ?string
    {
        return $this->contractNature;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function getSalary(): ?string
    {
        return $this->salary;
    }

    public function getSalaryCommentary(): ?string
    {
        return $this->salaryCommentary;
    }

    public function getSalaryComplement(): ?string
    {
        return $this->salaryComplement;
    }

    public function getSalaryOther(): ?string
    {
        return $this->salaryOther;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function getWorkTravel(): ?string
    {
        return $this->workTravel;
    }

    public function getQualification(): ?string
    {
        return $this->qualification;
    }
}
