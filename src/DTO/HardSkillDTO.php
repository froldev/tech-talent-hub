<?php

namespace App\DTO;

class HardSkillDTO
{
    public function __construct(
        private string $name,
    ) {
    }

    // Getters for all properties
    public function getName(): string
    {
        return $this->name;
    }
}
