<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\AnalysisOffersService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'analysis:offers',
    description: 'Analysis offers without tags',
)]
class AnalysisOffersCommand extends Command
{
    public function __construct(
        private readonly AnalysisOffersService $analysisOffersService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);
        $console->title('START : analysis offers without tags');
        $this->analysisOffersService->analysisOffersWithoutTags();
        $console->title('END : analysis offers without tags');

        return Command::SUCCESS;
    }
}
