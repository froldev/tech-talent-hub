<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\RegionService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:regions',
    description: 'Import all regions in database',
)]
class ImportRegionsCommand extends Command
{
    public function __construct(
        private readonly RegionService $regionService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->warning('Start : Create regions command');
        $this->regionService->importRegions();
        $console->success('End : Create regions command');

        return Command::SUCCESS;
    }
}
