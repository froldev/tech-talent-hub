<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\AnalysisJobsService;
use App\Service\AnalysisOffersService;
use App\Service\AnalysisPartnersService;
use App\Service\AnalysisTagsService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'analysis:all',
    description: 'Analysis partners, jobs, offers and tags.',
)]
class AnalysisCommand extends Command
{
    public function __construct(
        private readonly AnalysisPartnersService $analysisPartnersService,
        private readonly AnalysisJobsService $analysisJobsService,
        private readonly AnalysisOffersService $analysisOffersService,
        private readonly AnalysisTagsService $analysisTagsService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->warning('START : analysis url in partners');
        $this->analysisPartnersService->analysisPartners();
        $console->success('END : analysis url in partners');

        $console->warning('START : analysis tags');
        $this->analysisTagsService->analysisTags();
        $console->success('END : analysis tags');

        $console->title('START : analysis jobs');
        $this->analysisJobsService->analysisJobs();
        $console->title('END : analysis jobs');

        $console->title('START : analysis offers without tags');
        $this->analysisOffersService->analysisOffersWithoutTags();
        $console->title('END : analysis offers without tags');

        return Command::SUCCESS;
    }
}
