<?php

declare(strict_types=1);

namespace App\Command;

use App\Message\AnalysisAndFetchOffersMessage;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'analysis:fetchs',
    description: 'Analysis partners, jobs, offers, tags and fetch offers.',
)]
class AnalysisAndFetchsOffersCommand extends Command
{
    public function __construct(
        private readonly MessageBusInterface $bus,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->title('START : analysis partners, jobs, offers, tags and fetch offers');
        $message = new AnalysisAndFetchOffersMessage();
        $this->bus->dispatch($message);
        $console->success('END : analysis partners, jobs, offers, tags and fetch offers');

        return Command::SUCCESS;
    }
}
