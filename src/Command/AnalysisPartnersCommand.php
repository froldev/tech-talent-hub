<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\AnalysisPartnersService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'analysis:partners',
    description: 'Analysis url in partners.',
)]
class AnalysisPartnersCommand extends Command
{
    public function __construct(
        private readonly AnalysisPartnersService $analysisPartnersService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);
        $console->title('START : analysis url in partners');
        $this->analysisPartnersService->analysisPartners();
        $console->title('END : analysis url in partners');

        return Command::SUCCESS;
    }
}
