<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\NavbarService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:navbar',
    description: 'Import links in navbar',
)]
class ImportNavbarCommand extends Command
{
    public function __construct(
        private readonly NavbarService $navbarService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->warning('Start : Import Links Navbar command');
        $this->navbarService->importLinksInNavbar();
        $console->success('End : Import Links Navbar command');

        return Command::SUCCESS;
    }
}
