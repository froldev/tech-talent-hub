<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\ParameterService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:parameters',
    description: 'Import all parameters in database',
)]
class ImportParametersCommand extends Command
{
    public function __construct(
        private readonly ParameterService $parameterService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        // Import all pagination
        $console->warning('START : Import all parameters');
        $this->parameterService->importParameters();
        $console->success('END : Import all parameters');

        return Command::SUCCESS;
    }
}
