<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\JobService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:jobs',
    description: 'Import jobs in database',
)]
class ImportJobsCommand extends Command
{
    public function __construct(
        private readonly JobService $jobService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->warning('Start : Create Jobs command');
        $this->jobService->getJobs();
        $console->success('End : Create Jobs command');

        return Command::SUCCESS;
    }
}
