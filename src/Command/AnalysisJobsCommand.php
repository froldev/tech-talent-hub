<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\AnalysisJobsService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'analysis:jobs',
    description: 'Create all jobs in database',
)]
class AnalysisJobsCommand extends Command
{
    public function __construct(
        private readonly AnalysisJobsService $analysisJobsService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);
        $console->title('START : analysis jobs');
        $this->analysisJobsService->analysisJobs();
        $console->title('END : analysis jobs');

        return Command::SUCCESS;
    }
}
