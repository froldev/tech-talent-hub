<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\DepartmentService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:departments',
    description: 'Import departments in database',
)]
class ImportDepartmentsCommand extends Command
{
    public function __construct(
        private readonly DepartmentService $departmentService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->warning('Start : Create departments command');
        $this->departmentService->importDepartments();
        $console->success('End : Create departments command');

        return Command::SUCCESS;
    }
}
