<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\PostService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:posts',
    description: 'Create all posts in database',
)]
class ImportPostsCommand extends Command
{
    public function __construct(
        private readonly PostService $postService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        // Import all posts of salary
        $console->warning('START : Import all posts of salary');
        $this->postService->importPosts();
        $console->success('END : Import all posts of salary');

        return Command::SUCCESS;
    }
}
