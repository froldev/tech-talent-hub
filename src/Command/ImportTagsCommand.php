<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\TagService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'import:tags',
    description: 'Create all tags in database',
)]
class ImportTagsCommand extends Command
{
    public function __construct(
        private readonly TagService $tagService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);
        $console->title('Start of import tags command');

        // Create all tags with email
        $this->tagService->importTags();

        $console->title('End of import tags command');

        return Command::SUCCESS;
    }
}
