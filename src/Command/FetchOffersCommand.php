<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\FetchOffersService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'fetch:offers',
    description: 'Fetch offers from FranceTravail API',
)]
class FetchOffersCommand extends Command
{
    public function __construct(
        private readonly FetchOffersService $fetchOffersService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $console = new SymfonyStyle($input, $output);

        $console->title('Fetching data from FranceTravail API');
        $this->fetchOffersService->fetchAndSaveOffers();
        $console->success('Data fetched from FranceTravail API');

        return Command::SUCCESS;
    }
}
