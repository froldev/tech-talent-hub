<?php

namespace App\MessageHandler;

use App\Message\AnalysisAndFetchOffersMessage;
use App\Service\FetchOffersService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class FetchOffersMessageHandler
{
    public function __construct(
        private readonly FetchOffersService $fetchOffersService,
    ) {
    }

    public function __invoke(AnalysisAndFetchOffersMessage $message)
    {
        $this->fetchOffersService->fetchAndSaveOffers();
    }
}
