<?php

namespace App\MessageHandler;

use App\Message\AnalysisAndFetchOffersMessage;
use App\Service\AnalysisOffersService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class AnalysisOffersMessageHandler
{
    public function __construct(
        private readonly AnalysisOffersService $analysisOffersService,
    ) {
    }

    public function __invoke(AnalysisAndFetchOffersMessage $message)
    {
        $this->analysisOffersService->analysisOffersWithoutTags();
    }
}
