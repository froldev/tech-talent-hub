<?php

namespace App\MessageHandler;

use App\Message\AnalysisAndFetchOffersMessage;
use App\Service\AnalysisJobsService;
use App\Service\AnalysisOffersService;
use App\Service\AnalysisPartnersService;
use App\Service\AnalysisTagsService;
use App\Service\FetchOffersService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class AnalysisAndFetchOffersMessageHandler
{
    public function __construct(
        private readonly AnalysisPartnersService $analysisPartnersService,
        private readonly AnalysisJobsService $analysisJobsService,
        private readonly AnalysisOffersService $analysisOffersService,
        private readonly AnalysisTagsService $analysisTagsService,
        private readonly FetchOffersService $fetchOffersService,
    ) {
    }

    public function __invoke(AnalysisAndFetchOffersMessage $message)
    {
        $this->analysisPartnersService->analysisPartners();
        $this->analysisTagsService->analysisTags();
        $this->analysisJobsService->analysisJobs();
        $this->analysisOffersService->analysisOffersWithoutTags();
        $this->fetchOffersService->fetchAndSaveOffers();
    }
}
