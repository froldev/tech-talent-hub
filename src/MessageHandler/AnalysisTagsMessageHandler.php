<?php

namespace App\MessageHandler;

use App\Message\AnalysisTagsMessage;
use App\Service\AnalysisTagsService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class AnalysisTagsMessageHandler
{
    public function __construct(
        private readonly AnalysisTagsService $analysisTagsService,
    ) {
    }

    public function __invoke(AnalysisTagsMessage $message)
    {
        $this->analysisTagsService->analysisTags();
    }
}
