<?php

namespace App\MessageHandler;

use App\Message\AnalysisAndFetchOffersMessage;
use App\Service\AnalysisJobsService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class AnalysisJobsMessageHandler
{
    public function __construct(
        private readonly AnalysisJobsService $analysisJobsService,
    ) {
    }

    public function __invoke(AnalysisAndFetchOffersMessage $message)
    {
        $this->analysisJobsService->analysisJobs();
    }
}
