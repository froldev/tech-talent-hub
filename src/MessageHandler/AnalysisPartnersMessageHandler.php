<?php

namespace App\MessageHandler;

use App\Message\AnalysisPartnersMessage;
use App\Service\AnalysisPartnersService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class AnalysisPartnersMessageHandler
{
    public function __construct(
        private readonly AnalysisPartnersService $analysisPartnersService,
    ) {
    }

    public function __invoke(AnalysisPartnersMessage $message)
    {
        $this->analysisPartnersService->analysisPartners();
    }
}
