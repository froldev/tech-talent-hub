<?php

namespace App\Controller\App;

use App\Constant\PageOriginConstants;
use App\Constant\ParameterConstants;
use App\Repository\NavbarRepository;
use App\Repository\OfferRepository;
use App\Repository\ParameterRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly PartnerRepository $partnerRepository,
        private readonly PostRepository $postRepository,
        private readonly NavbarRepository $navbarRepository,
        private readonly ParameterRepository $parameterRepository,
    ) {
    }

    #[Route('/', name: 'app_home_index')]
    public function index(): Response
    {
        // Check if there is pagination parameter
        if (0 === $this->parameterRepository->countParameterByType(ParameterConstants::PARAMETER_INTEGER)) {
            return $this->render(
                'pages/app/errors/404.html.twig',
                [
                    'message' => 'The pagination parameter is not set.',
                ],
            );
        }

        return $this->render(
            'pages/app/home/index.html.twig',
            [
                'offers'        => $this->offerRepository->offersOnHomepageWithTheMostLikes(),
                'tags'          => $this->tagRepository->getSelectionActiveTagsByCount(),
                'partners'      => $this->partnerRepository->getSelectionActivePartnersByCount(),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
                'countPosts'    => $this->postRepository->countTotalPosts(),
                'origin'        => PageOriginConstants::ORIGIN_HOME,
            ],
        );
    }

    public function renderNavBar(): Response
    {
        return $this->render(
            'partials/app/_navbar.html.twig',
            [
                'navbars' => $this->navbarRepository->findBy(
                    ['active' => true],
                    [
                        'position' => 'ASC',
                    ],
                ),
            ],
        );
    }
}
