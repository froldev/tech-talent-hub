<?php

namespace App\Controller\App;

use App\Constant\PageOriginConstants;
use App\Entity\Partner;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/partenaires', name: 'app_partner_')]
class PartnerController extends AbstractController
{
    public function __construct(
        private readonly TagRepository $tagRepository,
        private readonly OfferRepository $offerRepository,
        private readonly PartnerRepository $partnerRepository,
        private readonly PostRepository $postRepository,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/app/partner/index.html.twig',
            [
                'partners'      => $this->partnerRepository->findAllPartners($request->query->getInt('page', 1)),
                'showDatas'     => ($this->offerRepository->countTotalOffers() > 0),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
                'countPosts'    => $this->postRepository->countTotalPosts(),
            ],
        );
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Partner $partner, Request $request): Response
    {
        return $this->render(
            'pages/app/partner/show.html.twig',
            [
                'partner'         => $partner,
                'offersByPartner' => $this->offerRepository->findAllOffersByPartner(
                    $partner->getSlug(),
                    $request->query->getInt('page', 1),
                ),
                'origin' => PageOriginConstants::ORIGIN_PARTNER,
            ],
        );
    }
}
