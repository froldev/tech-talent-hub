<?php

namespace App\Controller\App;

use App\Form\RequestResetPasswordType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use App\Service\EmailService;
use App\Service\JWTService;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly JWTService $jwtService,
        private readonly EmailService $emailService,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route(path: '/connexion', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'pages/app/security/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error'         => $error,
            ],
        );
    }

    #[Route(path: '/deconnexion', name: 'app_logout')]
    public function logout(): void
    {
        throw new LogicException('This method can be blank');
    }

    #[Route(path: '/mot-de-passe-oublie', name: 'app_forgot_password')]
    public function forgotPassword(Request $request): Response
    {
        $form = $this->createForm(RequestResetPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // found user
            $user = $this->userRepository->findOneBy(
                [
                    'email' => $form->get('email')->getData(),
                ],
            );

            // if user
            if ($user) {
                // generate a token
                $token = $this->jwtService->generateToken(
                    $user,
                    $this->getParameter('app.jwtsecret'),
                );

                // generate url with reset_password
                $url = $this->generateUrl(
                    'app_reset_password',
                    [
                        'token' => $token,
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                );

                // send email
                $this->emailService->sendEmail(
                    'contact@techtalenthub.fr',
                    $user->getEmail(),
                    'Réinitialisation de votre mot de passe sur le site TechTalentHub',
                    'reset_password',
                    [
                        'user' => $user,
                        'url'  => $url,
                    ],
                );

                // flash message
                $this->addFlash('success', 'Un email vous a été envoyé pour réinitialiser votre mot de passe.');

                return $this->redirectToRoute('app_login');
            }

            // not user found
            $this->addFlash('error', 'Un problème est survenu.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'pages/app/security/forgot_password.html.twig',
            [
                'RequestPasswordForm' => $form,
            ],
        );
    }

    #[Route(path: '/reinitialisation-du-mot-de-passe/{token}', name: 'app_reset_password')]
    public function resetPassword(
        string $token,
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
    ): Response {
        if ($this->jwtService->allIsCheck($token, $this->getParameter('app.jwtsecret'))) {
            $payload = $this->jwtService->getPayload($token);
            $user    = $this->userRepository->find($payload['user_id']);

            if ($user) {
                $form = $this->createForm(ResetPasswordType::class);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $user->setPassword(
                        $passwordHasher->hashPassword(
                            $user,
                            $form->get('password')->getData(),
                        ),
                    );
                    $this->entityManager->flush();

                    $this->addFlash('success', 'Votre mot de passe a été réinitialisé.');

                    return $this->redirectToRoute('app_login');
                }

                return $this->render(
                    'pages/app/security/reset_password.html.twig',
                    [
                        'ResetPasswordForm' => $form,
                    ],
                );
            }
        }

        $this->addFlash('error', 'Le token est invalide ou a expiré.');

        return $this->redirectToRoute('app_login');
    }
}
