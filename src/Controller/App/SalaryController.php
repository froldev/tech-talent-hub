<?php

namespace App\Controller\App;

use App\Constant\PostConstants;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/salaires', name: 'app_salary_')]
class SalaryController extends AbstractController
{
    public function __construct(
        private readonly PostRepository $postRepository,
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly PartnerRepository $partnerRepository,
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/app/post/index.html.twig',
            [
                'title'  => 'Le Baromètre des Salaires',
                'banner' => 'banner-salary',
                'posts'  => $this->postRepository->findAllPosts(
                    $request->query->getInt('page', 1),
                    PostConstants::SALARY,
                ),
                'countPosts'    => $this->postRepository->countTotalPosts(PostConstants::SALARY),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
            ],
        );
    }
}
