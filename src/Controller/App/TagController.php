<?php

namespace App\Controller\App;

use App\Constant\PageOriginConstants;
use App\Entity\Tag;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/technologies', name: 'app_tag_')]
class TagController extends AbstractController
{
    public function __construct(
        private readonly TagRepository $tagRepository,
        private readonly OfferRepository $offerRepository,
        private readonly PartnerRepository $partnerRepository,
        private readonly PostRepository $postRepository,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/app/tag/index.html.twig',
            [
                'tags'          => $this->tagRepository->findAllChildrenTags($request->query->getInt('page', 1)),
                'showDatas'     => ($this->offerRepository->countTotalOffers() > 0),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
                'countPosts'    => $this->postRepository->countTotalPosts(),
            ],
        );
    }

    #[Route('/{slug}/{id}', name: 'show', methods: ['GET'])]
    public function show(Tag $tag, Request $request): Response
    {
        return $this->render(
            'pages/app/tag/show.html.twig',
            [
                'tag'         => $tag,
                'offersByTag' => $this->offerRepository->findAllOffersByTag($tag, $request->query->getInt('page', 1)),
                'origin'      => PageOriginConstants::ORIGIN_TAG,
            ],
        );
    }
}
