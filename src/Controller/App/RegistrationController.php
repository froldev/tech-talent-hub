<?php

namespace App\Controller\App;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Service\EmailService;
use App\Service\JWTService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly JWTService $jwtService,
        private readonly EmailService $emailService,
    ) {
    }

    #[Route('/inscription', name: 'app_register')]
    public function register(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $entityManager,
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData(),
                ),
            );
            $user->setRoles(['ROLE_USER']);

            $entityManager->persist($user);
            $entityManager->flush();

            // token
            $token = $this->jwtService->generateToken($user, $this->getParameter('app.jwtsecret'));
            // url
            // generate url with reset_password
            $url = $this->generateUrl('app_verify_user', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

            // send email
            $this->emailService->sendEmail(
                'contact@techtalenthub.fr',
                $user->getEmail(),
                'Activation de votre compte sur le site TechTalentHub',
                'register',
                [
                    'user' => $user,
                    'url'  => $url,
                ],
            );

            // add flash message
            $this->addFlash(
                'success',
                'Un email de confirmation vous a été envoyé.' .
                'Veuillez cliquer sur le lien pour activer votre compte '
                . 'et vous connecter.',
            );

            return $this->redirectToRoute('app_home_index');
            // return $userAuthenticator->authenticateUser($user, $authenticator, $request);
        }

        return $this->render(
            'pages/app/registration/register.html.twig',
            [
                'registrationForm' => $form->createView(),
            ],
            new Response(null, $form->isSubmitted() && !$form->isValid() ? 422 : 200),
        );
    }

    #[Route('/verification/{token}', name: 'app_verify_user')]
    public function verifyUser(
        string $token,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        // all is check
        if ($this->jwtService->allIsCheck($token, $this->getParameter('app.jwtsecret'))) {
            // decode token
            $payload = $this->jwtService->getPayload($token);

            // get user
            $user = $userRepository->find($payload['user_id']);

            if ($user && !$user->isActive()) {
                $user->setActive(true);
                $entityManager->flush();

                // add flash message
                $this->addFlash('success', 'Votre compte a bien été activé !');

                return $this->redirectToRoute('app_login');
            }
        }

        // add flash message
        $this->addFlash('danger', 'Le lien de vérification est invalide ou a expiré.');

        return $this->redirectToRoute('app_register');
    }
}
