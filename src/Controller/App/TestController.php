<?php

namespace App\Controller\App;

use App\Constant\PostConstants;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tests-techniques', name: 'app_test_')]
class TestController extends AbstractController
{
    public function __construct(
        private readonly PostRepository $postRepository,
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly PartnerRepository $partnerRepository,
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/app/post/index.html.twig',
            [
                'title'         => 'Tests techniques',
                'banner'        => 'banner-training',
                'posts'         => $this->postRepository->findAllPosts(
                    $request->query->getInt('page', 1),
                    PostConstants::TEST,
                ),
                'countPosts'    => $this->postRepository->countTotalPosts(PostConstants::TEST),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
            ],
        );
    }
}
