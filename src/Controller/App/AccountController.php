<?php

namespace App\Controller\App;

use App\Entity\User;
use App\Form\AccountUserType;
use App\Repository\UserRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mon-compte', name: 'app_account_')]
class AccountController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserService $userService,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/profil', name: 'index')]
    public function index(): Response
    {
        $this->verifyIfConnected();

        $id = $this->getUser()->id ?? null;

        return $this->render(
            'pages/app/account/index.html.twig',
            [
                'account'             => $this->getUser(),
                'countOfferLikes'     => $id ? $this->userRepository->countOfferLikes($id) : 0,
                'countOfferCandidacy' => $id ? $this->userRepository->countOfferCandidacy($id) : 0,
                'countOfferSearch'    => $id ? $this->userRepository->countOfferSearch($id) : 0,
            ],
        );
    }

    #[Route('/candidatures', name: 'candidacies')]
    public function candidacies(): Response
    {
        $this->verifyIfConnected();

        $id = $this->getUser()->id ?? null;

        return $this->render(
            'pages/app/account/candidacies.html.twig',
            [
                'account'             => $this->getUser(),
                'candidacies'         => $id ? $this->userRepository->getOfferCandidacies($id) : [],
                'countOfferCandidacy' => $id ? $this->userRepository->countOfferCandidacy($id) : 0,
            ],
        );
    }

    #[Route('/offres-sauvegardees', name: 'likes')]
    public function likes(): Response
    {
        $this->verifyIfConnected();

        $id = $this->getUser()->id ?? null;

        return $this->render(
            'pages/app/account/likes.html.twig',
            [
                'account'         => $this->getUser(),
                'likes'           => $id ? $this->userRepository->getOfferLikes($id) : [],
                'countOfferLikes' => $id ? $this->userRepository->countOfferLikes($id) : 0,
            ],
        );
    }

    #[Route('/recherches', name: 'searches')]
    public function searches(): Response
    {
        $this->verifyIfConnected();

        $id = $this->getUser()->id ?? null;

        return $this->render(
            'pages/app/account/searches.html.twig',
            [
                'account'          => $this->getUser(),
                'searches'         => $id ? $this->userRepository->getOfferSearch($id) : [],
                'countOfferSearch' => $id ? $this->userRepository->countOfferSearch($id) : 0,
            ],
        );
    }

    #[Route('/parametres', name: 'parameters')]
    public function parameters(Request $request): Response
    {
        $this->verifyIfConnected();

        $form = $this->createForm(
            AccountUserType::class,
            $this->getUser(),
            [
                'user' => $this->getUser(),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_logout');
        }

        return $this->render(
            'pages/app/account/parameters.html.twig',
            [
                'account' => $this->getUser(),
                'form'    => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(
        Request $request,
        User $user,
    ): Response {
        $this->verifyIfConnected();

        $currentUser = $this->getUser();

        if ($currentUser instanceof User && $this->userService->verifyIfUserIsAdmin($currentUser)) {
            throw new Exception('Vous ne pouvez pas supprimer un administrateur.');
        }

        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $this->container->get('security.token_storage')->setToken(null);
            $this->userService->deleteUser($user);

            // add flash message
            $this->addFlash('success', 'Votre compte a été supprimé avec succès.');

            return $this->redirectToRoute('app_home_index');
        }

        return $this->redirectToRoute('app_account_parameters', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/ajouter-une-recherche', name: 'add')]
    public function add(): Response
    {
        return $this->render('pages/app/account/add.html.twig');
    }

    /**
     * Verify if user is connected.
     */
    private function verifyIfConnected(): void
    {
        if (null !== $this->getUser()) {
            $this->redirectToRoute('app_login');
        }
    }
}
