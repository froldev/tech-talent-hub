<?php

namespace App\Controller\App;

use App\Constant\PageOriginConstants;
use App\Entity\Offer;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/offres', name: 'app_offer_')]
class OfferController extends AbstractController
{
    public function __construct(
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly PartnerRepository $partnerRepository,
        private readonly PostRepository $postRepository,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/app/offer/index.html.twig',
            [
                'offers'        => $this->offerRepository->findAllOffers($request->query->getInt('page', 1)),
                'showDatas'     => ($this->offerRepository->countTotalOffers() > 0),
                'countOffers'   => $this->offerRepository->countTotalOffers(),
                'countTags'     => $this->tagRepository->countTotalTags(),
                'countPartners' => $this->partnerRepository->countTotalPartners(),
                'countPosts'    => $this->postRepository->countTotalPosts(),
                'origin'        => PageOriginConstants::ORIGIN_OFFER,
            ],
        );
    }

    #[Route('/{origin}/{slug}', name: 'show', methods: ['GET'])]
    public function show(Offer $offer, ?string $origin = null): Response
    {
        switch ($origin) {
            case PageOriginConstants::ORIGIN_PARTNER:
                $link      = $this->generateUrl('app_partner_index');
                $linkTitle = 'Partenaires';
                break;
            case PageOriginConstants::ORIGIN_TAG:
                $link      = $this->generateUrl('app_tag_index');
                $linkTitle = 'Technologies';
                break;
            case PageOriginConstants::ORIGIN_HOME:
                $link      = $this->generateUrl('app_home_index');
                $linkTitle = 'Accueil';
                break;
            default:
                $link      = $this->generateUrl('app_offer_index');
                $linkTitle = 'Offres';
                break;
        }

        return $this->render(
            'pages/app/offer/show.html.twig',
            [
                'offer'     => $offer,
                'link'      => $link,
                'linkTitle' => $linkTitle,
            ],
        );
    }
}
