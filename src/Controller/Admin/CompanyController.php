<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Form\AdminCompanyType;
use App\Form\AdminSearchCompanyType;
use App\Model\AdminSearchCompanyData;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/societes', name: 'app_admin_company_')]
#[IsGranted('ROLE_ADMIN')]
class CompanyController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(CompanyRepository $companyRepository, Request $request): Response
    {
        $adminSearchCompanyData = new AdminSearchCompanyData();
        $form                   = $this->createForm(AdminSearchCompanyType::class, $adminSearchCompanyData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchCompanyData->page = $request->query->getInt('page', 1);
            $companies                    = $companyRepository->findBySearch($adminSearchCompanyData);

            return $this->render(
                'pages/admin/company/index.html.twig',
                [
                    'companies' => $companies,
                    'form'      => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/company/index.html.twig',
            [
                'companies' => $companyRepository->findAllCompanies($request->query->getInt('page', 1)),
                'form'      => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $company = new Company();
        $form    = $this->createForm(AdminCompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($company);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_company_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/company/new.html.twig',
            [
                'company' => $company,
                'form'    => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Company $company): Response
    {
        return $this->render(
            'pages/admin/company/show.html.twig',
            [
                'company' => $company,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Company $company, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminCompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_company_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/company/edit.html.twig',
            [
                'company' => $company,
                'form'    => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Company $company, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $company->getId(), $request->request->get('_token'))) {
            $entityManager->remove($company);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_company_index', [], Response::HTTP_SEE_OTHER);
    }
}
