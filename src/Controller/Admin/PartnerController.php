<?php

namespace App\Controller\Admin;

use App\Entity\Partner;
use App\Form\AdminPartnerToDefineType;
use App\Form\AdminPartnerType;
use App\Form\AdminSearchPartnerType;
use App\Model\AdminSearchPartnerData;
use App\Repository\PartnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/partenaires', name: 'app_admin_partner_')]
#[IsGranted('ROLE_ADMIN')]
class PartnerController extends AbstractController
{
    public function __construct(
        private readonly PartnerRepository $partnerRepository,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $countPartnerToDefine = $this->partnerRepository->countPartnersWithoutUrl();

        $adminSearchPartnerData = new AdminSearchPartnerData();
        $form                   = $this->createForm(AdminSearchPartnerType::class, $adminSearchPartnerData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchPartnerData->page = $request->query->getInt('page', 1);
            $partners                     = $this->partnerRepository->findBySearch($adminSearchPartnerData);

            $countPartnerToDefine = $this->partnerRepository->countPartnersWithoutUrl();

            return $this->render(
                'pages/admin/partner/index.html.twig',
                [
                    'partners'             => $partners,
                    'form'                 => $form,
                    'countPartnerToDefine' => $countPartnerToDefine,
                ],
            );
        }

        return $this->render(
            'pages/admin/partner/index.html.twig',
            [
                'partners'             => $this->partnerRepository->findAllPartners($request->query->getInt('page', 1)),
                'form'                 => $form,
                'countPartnerToDefine' => $countPartnerToDefine,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $partner = new Partner();
        $form    = $this->createForm(AdminPartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $partner->setPosition($this->partnerRepository->countPosition() + 1);
            $entityManager->persist($partner);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_partner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/partner/new.html.twig',
            [
                'partner' => $partner,
                'form'    => $form,
            ],
        );
    }

    #[Route('/definir-une-url', name: 'define', methods: ['GET', 'POST'])]
    public function define(Request $request, EntityManagerInterface $entityManager): Response
    {
        $countPartnerToDefine = $this->partnerRepository->countPartnersWithoutUrl();

        if (0 === $countPartnerToDefine) {
            return $this->redirectToRoute('app_admin_partner_index', [], Response::HTTP_SEE_OTHER);
        }

        $partner = $this->partnerRepository->getFirstPartnerWithoutUrl();

        $form = $this->createForm(AdminPartnerToDefineType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $countPartnerToDefine = $this->partnerRepository->countPartnersWithoutUrl();

            if (0 === $countPartnerToDefine) {
                return $this->redirectToRoute('app_admin_partner_index', [], Response::HTTP_SEE_OTHER);
            }

            $partner = $this->partnerRepository->getFirstPartnerWithoutUrl();

            return $this->render(
                'pages/admin/partner/define.html.twig',
                [
                    'partner'              => $partner,
                    'form'                 => $form,
                    'countPartnerToDefine' => $countPartnerToDefine,
                ],
            );
        }

        return $this->render(
            'pages/admin/partner/define.html.twig',
            [
                'partner'              => $partner,
                'form'                 => $form,
                'countPartnerToDefine' => $countPartnerToDefine,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Partner $partner, EntityManagerInterface $entityManager): Response
    {
        $position = $partner->getPosition();

        $form = $this->createForm(
            AdminPartnerType::class,
            $partner,
            [
                'position' => array_flip($this->partnerRepository->getAllPosition()),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $otherPartner = $this->partnerRepository->findOneBy(['position' => $partner->getPosition()]);

            if ($otherPartner) {
                $otherPartner->setPosition($position);
                $entityManager->persist($otherPartner);
            }
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_partner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/partner/edit.html.twig',
            [
                'partner' => $partner,
                'form'    => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Partner $partner, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $partner->getId(), $request->request->get('_token'))) {
            $entityManager->remove($partner);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_partner_index', [], Response::HTTP_SEE_OTHER);
    }
}
