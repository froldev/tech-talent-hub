<?php

namespace App\Controller\Admin;

use App\Entity\Formation;
use App\Form\AdminFormationType;
use App\Form\AdminSearchFormationType;
use App\Model\AdminSearchFormationData;
use App\Repository\FormationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/formations', name: 'app_admin_formation_')]
#[IsGranted('ROLE_ADMIN')]
class FormationController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(FormationRepository $formationRepository, Request $request): Response
    {
        $adminSearchFormationData = new AdminSearchFormationData();
        $form                     = $this->createForm(AdminSearchFormationType::class, $adminSearchFormationData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchFormationData->page = $request->query->getInt('page', 1);
            $formations                     = $formationRepository->findBySearch($adminSearchFormationData);

            return $this->render(
                'pages/admin/formation/index.html.twig',
                [
                    'formations' => $formations,
                    'form'       => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/formation/index.html.twig',
            [
                'formations' => $formationRepository->findAllFormations($request->query->getInt('page', 1)),
                'form'       => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $formation = new Formation();
        $form      = $this->createForm(AdminFormationType::class, $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($formation);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_formation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/formation/new.html.twig',
            [
                'formation' => $formation,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Formation $formation): Response
    {
        return $this->render(
            'pages/admin/formation/show.html.twig',
            [
                'formation' => $formation,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Formation $formation, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminFormationType::class, $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_formation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/formation/edit.html.twig',
            [
                'formation' => $formation,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Formation $formation, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $formation->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($formation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_formation_index', [], Response::HTTP_SEE_OTHER);
    }
}
