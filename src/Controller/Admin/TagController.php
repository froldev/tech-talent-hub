<?php

namespace App\Controller\Admin;

use App\Entity\Tag;
use App\Form\AdminSearchTagType;
use App\Form\AdminTagType;
use App\Model\AdminSearchTagData;
use App\Repository\SearchRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/technologies', name: 'app_admin_tag_')]
#[IsGranted('ROLE_ADMIN')]
class TagController extends AbstractController
{
    public function __construct(
        private readonly SearchRepository $searchRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(TagRepository $tagRepository, Request $request): Response
    {
        $adminSearchTagData = new AdminSearchTagData();
        $form               = $this->createForm(AdminSearchTagType::class, $adminSearchTagData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchTagData->page = $request->query->getInt('page', 1);

            return $this->render(
                'pages/admin/tag/index.html.twig',
                [
                    'tags' => $tagRepository->findBySearch($adminSearchTagData),
                    'form' => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/tag/index.html.twig',
            [
                'tags' => $tagRepository->findBySearch($adminSearchTagData),
                'form' => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $tag  = new Tag();
        $form = $this->createForm(AdminTagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($tag);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_tag_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/tag/new.html.twig',
            [
                'tag'  => $tag,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Tag $tag): Response
    {
        return $this->render(
            'pages/admin/tag/show.html.twig',
            [
                'tag' => $tag,
            ],
        );
    }

    #[Route('/modifier/{id}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(AdminTagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_tag_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/tag/edit.html.twig',
            [
                'tag'  => $tag,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Tag $tag): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tag->getId(), $request->request->get('_token'))) {
            $searches = $this->searchRepository->findBy(['tag' => $tag]);

            foreach ($searches as $search) {
                $this->entityManager->remove($search);
            }

            $this->entityManager->remove($tag);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_tag_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/supprimer-les-tags-des-offres', name: 'delete_tags_to_offer', methods: ['POST'])]
    public function deleteTagsToOffer(Tag $tag): void
    {
        $offers = $tag->getOffers();

        foreach ($offers as $offer) {
            $offer->removeTag($tag);
            $this->entityManager->persist($offer);
        }
        $this->entityManager->flush();
    }
}
