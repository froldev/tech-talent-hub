<?php

namespace App\Controller\Admin;

use App\Entity\City;
use App\Form\AdminCityType;
use App\Form\AdminSearchCityType;
use App\Model\AdminSearchCityData;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/villes', name: 'app_admin_city_')]
#[IsGranted('ROLE_ADMIN')]
class CityController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(CityRepository $cityRepository, Request $request): Response
    {
        $adminSearchCityData = new AdminSearchCityData();
        $form                = $this->createForm(AdminSearchCityType::class, $adminSearchCityData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchCityData->page = $request->query->getInt('page', 1);
            $cities                    = $cityRepository->findBySearch($adminSearchCityData);

            return $this->render(
                'pages/admin/city/index.html.twig',
                [
                    'cities' => $cities,
                    'form'   => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/city/index.html.twig',
            [
                'cities' => $cityRepository->findAllCities($request->query->getInt('page', 1)),
                'form'   => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $city = new City();
        $form = $this->createForm(AdminCityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($city);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_city_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/city/new.html.twig',
            [
                'city' => $city,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(City $city): Response
    {
        return $this->render(
            'pages/admin/city/show.html.twig',
            [
                'city' => $city,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, City $city, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminCityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $city->setLabel($city->getDepartment()->getCode() . ' - ' . $city->getName());
            $entityManager->persist($city);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_city_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/city/edit.html.twig',
            [
                'city' => $city,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, City $city, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $city->getId(), $request->request->get('_token'))) {
            $entityManager->remove($city);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_city_index', [], Response::HTTP_SEE_OTHER);
    }
}
