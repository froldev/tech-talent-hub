<?php

namespace App\Controller\Admin;

use App\Entity\Parameter;
use App\Form\AdminParameterType;
use App\Form\AdminSearchParameterType;
use App\Model\AdminSearchParameterData;
use App\Repository\ParameterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/parametres', name: 'app_admin_parameters_')]
#[IsGranted('ROLE_ADMIN')]
class ParameterController extends AbstractController
{
    public function __construct(
        private readonly ParameterRepository $paramaterRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $adminSearchParameterData = new AdminSearchParameterData();
        $form                     = $this->createForm(AdminSearchParameterType::class, $adminSearchParameterData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchParameterData->page = $request->query->getInt('page', 1);
            $parameters                     = $this->paramaterRepository->findBySearch($adminSearchParameterData);

            return $this->render(
                'pages/admin/parameters/parameter/index.html.twig',
                [
                    'parameters' => $parameters,
                    'form'       => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/parameters/parameter/index.html.twig',
            [
                'parameters' => $this->paramaterRepository->findAllParameters($request->query->getInt('page', 1)),
                'form'       => $form,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Parameter $parameter): Response
    {
        $form = $this->createForm(AdminParameterType::class, $parameter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_parameters_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/parameter/edit.html.twig',
            [
                'parameter' => $parameter,
                'form'      => $form,
            ],
        );
    }
}
