<?php

namespace App\Controller\Admin;

use App\Entity\Tag;
use App\Form\AdminSearchTagTypeType;
use App\Form\AdminTagTypeType;
use App\Model\AdminSearchTagTypeData;
use App\Repository\SearchRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/parametres/technologies', name: 'app_admin_tag_type_')]
#[IsGranted('ROLE_ADMIN')]
class TagTypeController extends AbstractController
{
    public function __construct(
        private readonly TagRepository $tagRepository,
        private readonly SearchRepository $searchRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        $adminSearchTagTypeData = new AdminSearchTagTypeData();
        $form                   = $this->createForm(AdminSearchTagTypeType::class, $adminSearchTagTypeData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchTagTypeData->page = $request->query->getInt('page', 1);

            return $this->render(
                'pages/admin/parameters/tag/index.html.twig',
                [
                    'tags' => $this->tagRepository->findParameterBySearch($adminSearchTagTypeData),
                    'form' => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/parameters/tag/index.html.twig',
            [
                'tags' => $this->tagRepository->findParameterBySearch($adminSearchTagTypeData),
                'form' => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $tag  = new Tag();
        $form = $this->createForm(AdminTagTypeType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($tag);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_tag_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/tag/new.html.twig',
            [
                'tag'  => $tag,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Tag $tag): Response
    {
        return $this->render(
            'pages/admin/parameters/tag/show.html.twig',
            [
                'tag' => $tag,
            ],
        );
    }

    #[Route('/modifier/{id}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(AdminTagTypeType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_tag_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/tag/edit.html.twig',
            [
                'tag'  => $tag,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Tag $tag): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tag->getId(), $request->request->get('_token'))) {
            $searches = $this->searchRepository->findBy(['tag' => $tag]);

            foreach ($searches as $search) {
                $this->entityManager->remove($search);
            }

            $this->entityManager->remove($tag);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_tag_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
