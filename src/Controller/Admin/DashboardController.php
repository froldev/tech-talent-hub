<?php

namespace App\Controller\Admin;

use App\Repository\CityRepository;
use App\Repository\CompanyRepository;
use App\Repository\JobRepository;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use App\Service\ParameterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/dashboard', name: 'app_admin_dashboard_')]
#[IsGranted('ROLE_ADMIN')]
class DashboardController extends AbstractController
{
    public function __construct(
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly PartnerRepository $partnerRepository,
        private readonly UserRepository $userRepository,
        private readonly CityRepository $cityRepository,
        private readonly CompanyRepository $companyRepository,
        private readonly JobRepository $jobRepository,
        private readonly ParameterService $parameterService,
    ) {
    }

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        // Verify if parameters are set
        if (!$this->parameterService->verifyIfParametersAreSet()) {
            return $this->render('pages/admin/parameters/parameter/france_travail.html.twig');
        }

        return $this->render(
            'pages/admin/dashboard/index.html.twig',
            [
                'countOffers'        => $this->offerRepository->countTotalOffers(),
                'countTags'          => $this->tagRepository->countTotalTags(),
                'countPartners'      => $this->partnerRepository->countTotalPartners(),
                'countJobs'          => $this->jobRepository->countTotalJobs(),
                'countUsers'         => $this->userRepository->countTotalUsers(),
                'countCities'        => $this->cityRepository->countTotalCities(),
                'countCompanies'     => $this->companyRepository->countTotalCompanies(),
                'offersByDepartment' => $this->offerRepository->getOffersByDepartment(),
                'offersByTag'        => $this->offerRepository->getOffersByTag(),
            ],
        );
    }
}
