<?php

namespace App\Controller\Admin;

use App\Entity\HardSkill;
use App\Form\AdminHardSkillType;
use App\Form\AdminSearchHardSkillType;
use App\Model\AdminSearchHardSkillData;
use App\Repository\HardSkillRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/savoirs-faire', name: 'app_admin_hardskill_')]
#[IsGranted('ROLE_ADMIN')]
class HardSkillController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(HardSkillRepository $hardSkillRepository, Request $request): Response
    {
        $adminSearchHardSkillData = new AdminSearchHardSkillData();
        $form                     = $this->createForm(AdminSearchHardSkillType::class, $adminSearchHardSkillData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchHardSkillData->page = $request->query->getInt('page', 1);
            $hardskills                     = $hardSkillRepository->findBySearch($adminSearchHardSkillData);

            return $this->render(
                'pages/admin/hardskill/index.html.twig',
                [
                    'hardskills' => $hardskills,
                    'form'       => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/hardskill/index.html.twig',
            [
                'hardskills' => $hardSkillRepository->findAllHardSkills($request->query->getInt('page', 1)),
                'form'       => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $hardSkill = new HardSkill();
        $form      = $this->createForm(AdminHardSkillType::class, $hardSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($hardSkill);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_hardskill_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/hardskill/new.html.twig',
            [
                'hardskill' => $hardSkill,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(HardSkill $hardSkill): Response
    {
        return $this->render(
            'pages/admin/hardskill/show.html.twig',
            [
                'hardskill' => $hardSkill,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, HardSkill $hardSkill, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminHardSkillType::class, $hardSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_hardskill_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/hardskill/edit.html.twig',
            [
                'hardskill' => $hardSkill,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, HardSkill $hardSkill, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $hardSkill->getId(), $request->request->get('_token'))) {
            $entityManager->remove($hardSkill);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_hardskill_index', [], Response::HTTP_SEE_OTHER);
    }
}
