<?php

namespace App\Controller\Admin;

use App\Message\AnalysisAndFetchOffersMessage;
use App\Message\AnalysisJobsMessage;
use App\Message\AnalysisOffersMessage;
use App\Message\AnalysisPartnersMessage;
use App\Message\AnalysisTagsMessage;
use App\Message\FetchOffersMessage;
use App\Service\ParameterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/planifications', name: 'app_admin_scheduler_')]
#[IsGranted('ROLE_ADMIN')]
class SchedulerController extends AbstractController
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private readonly ParameterService $parameterService,
    ) {
    }

    #[Route('/analyses/{type}', name: 'analysis', methods: ['GET', 'POST'])]
    public function analysis(Request $request, ?string $type = null): Response
    {
        // Verify if parameters are set
        if (!$this->parameterService->verifyIfParametersAreSet()) {
            return $this->render('pages/admin/parameters/parameter/france_travail.html.twig');
        }

        // Map of messages and corresponding classes
        $analysisMap = [
            'partners' => [
                'messageClass' => AnalysisPartnersMessage::class,
                'flashMessage' => 'Partenaires',
            ],
            'tags' => [
                'messageClass' => AnalysisTagsMessage::class,
                'flashMessage' => 'Technologies',
            ],
            'jobs' => [
                'messageClass' => AnalysisJobsMessage::class,
                'flashMessage' => 'Métiers',
            ],
            'offers' => [
                'messageClass' => AnalysisOffersMessage::class,
                'flashMessage' => 'Offres',
            ],
        ];

        // If type is null or invalid, return the initial view or throw an exception
        if (null === $type || !array_key_exists($type, $analysisMap)) {
            if (null === $type) {
                return $this->render('pages/admin/scheduler/analysis.html.twig', ['type' => $type]);
            }
            throw $this->createNotFoundException('Invalid analysis type.');
        }

        // If the request is in POST method, trigger the analysis
        if ($request->isMethod('POST')) {
            $messageClass = $analysisMap[$type]['messageClass'];
            $this->messageBus->dispatch(new $messageClass());

            // Add flash message for user
            $this->addFlash(
                'success',
                'Vous allez recevoir un mail avec les informations concernant l\'analyse des ' .
                $analysisMap[$type]['flashMessage'] .
                ' dans quelques minutes.',
            );

            return $this->redirectToRoute('app_admin_scheduler_analysis', ['type' => $type]);
        }

        return $this->render('pages/admin/scheduler/analysis.html.twig', ['type' => $type]);
    }

    #[Route('/recuperations/{type}', name: 'fetchs', methods: ['GET', 'POST'])]
    public function fetch(Request $request, ?string $type = null): Response
    {
        // Verify if parameters are set
        if (!$this->parameterService->verifyIfParametersAreSet()) {
            return $this->render('pages/admin/parameters/parameter/france_travail.html.twig');
        }

        // Map of messages and corresponding classes
        $fetchsMap = [
            'offers' => [
                'messageClass' => FetchOffersMessage::class,
                'flashMessage' => 'Offres',
            ],
            'all' => [
                'messageClass' => AnalysisAndFetchOffersMessage::class,
                'flashMessage' => 'Analyse et récupération des offres',
            ],
        ];

        // If type is null or invalid, return the initial view or throw an exception
        if (null === $type || !array_key_exists($type, $fetchsMap)) {
            if (null === $type) {
                return $this->render('pages/admin/scheduler/fetchs.html.twig', ['type' => $type]);
            }
            throw $this->createNotFoundException('Invalid fetchs type.');
        }

        // If the request is in POST method, trigger the fetch
        if ($request->isMethod('POST')) {
            $messageClass = $fetchsMap[$type]['messageClass'];
            $this->messageBus->dispatch(new $messageClass());

            // Add flash message for user
            $this->addFlash(
                'success',
                'Vous allez recevoir un mail avec les informations concernant la
                récupérations des ' . $fetchsMap[$type]['flashMessage'] . ' dans plusieurs minutes.',
            );

            return $this->redirectToRoute('app_admin_scheduler_fetchs', ['type' => $type]);
        }

        return $this->render('pages/admin/scheduler/fetchs.html.twig', ['type' => $type]);
    }
}
