<?php

namespace App\Controller\Admin;

use App\Entity\SoftSkill;
use App\Form\AdminSearchSoftSkillType;
use App\Form\AdminSoftSkillType;
use App\Model\AdminSearchSoftSkillData;
use App\Repository\SoftSkillRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/savoirs-etre', name: 'app_admin_softskill_')]
#[IsGranted('ROLE_ADMIN')]
class SoftSkillController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(SoftSkillRepository $softSkillRepository, Request $request): Response
    {
        $adminSearchSoftSkillData = new AdminSearchSoftSkillData();
        $form                     = $this->createForm(AdminSearchSoftSkillType::class, $adminSearchSoftSkillData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchSoftSkillData->page = $request->query->getInt('page', 1);
            $softskills                     = $softSkillRepository->findBySearch($adminSearchSoftSkillData);

            return $this->render(
                'pages/admin/softskill/index.html.twig',
                [
                    'softskills' => $softskills,
                    'form'       => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/softskill/index.html.twig',
            [
                'softskills' => $softSkillRepository->findAllSoftSkills($request->query->getInt('page', 1)),
                'form'       => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $softSkill = new SoftSkill();
        $form      = $this->createForm(AdminSoftSkillType::class, $softSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($softSkill);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_softskill_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/softskill/new.html.twig',
            [
                'softskill' => $softSkill,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(SoftSkill $softSkill): Response
    {
        return $this->render(
            'pages/admin/softskill/show.html.twig',
            [
                'softskill' => $softSkill,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SoftSkill $softSkill, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminSoftSkillType::class, $softSkill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_softskill_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/softskill/edit.html.twig',
            [
                'softskill' => $softSkill,
                'form'      => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, SoftSkill $softSkill, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $softSkill->getId(), $request->request->get('_token'))) {
            $entityManager->remove($softSkill);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_softskill_index', [], Response::HTTP_SEE_OTHER);
    }
}
