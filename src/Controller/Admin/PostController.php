<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\AdminPostType;
use App\Form\AdminSearchPostType;
use App\Model\AdminSearchPostData;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/blog', name: 'app_admin_post_')]
#[IsGranted('ROLE_ADMIN')]
class PostController extends AbstractController
{
    public function __construct(
        private readonly PostRepository $postRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $adminSearchPostData = new AdminSearchPostData();
        $form                = $this->createForm(AdminSearchPostType::class, $adminSearchPostData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchPostData->page = $request->query->getInt('page', 1);
            $posts                     = $this->postRepository->findBySearch($adminSearchPostData);

            return $this->render(
                'pages/admin/parameters/post/index.html.twig',
                [
                    'posts' => $posts,
                    'form'  => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/parameters/post/index.html.twig',
            [
                'posts' => $this->postRepository->findAllPosts($request->query->getInt('page', 1)),
                'form'  => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $post = new Post();
        $form = $this->createForm(AdminPostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/post/new.html.twig',
            [
                'post' => $post,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Post $post): Response
    {
        return $this->render(
            'pages/admin/parameters/post/show.html.twig',
            [
                'post' => $post,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(AdminPostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/post/edit.html.twig',
            [
                'post' => $post,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete' . $post->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($post);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_post_index', [], Response::HTTP_SEE_OTHER);
    }
}
