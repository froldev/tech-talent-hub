<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Form\AdminOfferToDefineType;
use App\Form\AdminOfferType;
use App\Form\AdminSearchOfferType;
use App\Model\AdminSearchOfferData;
use App\Repository\OfferRepository;
use App\Repository\TagRepository;
use App\Service\ParameterService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/offres', name: 'app_admin_offer_')]
#[IsGranted('ROLE_ADMIN')]
class OfferController extends AbstractController
{
    public function __construct(
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly ParameterService $parameterService,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        // Verify if parameters are set
        if (!$this->parameterService->verifyIfParametersAreSet()) {
            return $this->render('pages/admin/parameters/parameter/france_travail.html.twig');
        }

        $countOfferToDefine = $this->offerRepository->countOffersWithoutTags();

        $adminSearchOfferData = new AdminSearchOfferData();
        $form                 = $this->createForm(AdminSearchOfferType::class, $adminSearchOfferData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchOfferData->page = $request->query->getInt('page', 1);
            $offers                     = $this->offerRepository->findBySearch($adminSearchOfferData);

            return $this->render(
                'pages/admin/offer/index.html.twig',
                [
                    'offers'             => $offers,
                    'form'               => $form,
                    'countOfferToDefine' => $countOfferToDefine,
                ],
            );
        }

        return $this->render(
            'pages/admin/offer/index.html.twig',
            [
                'offers'             => $this->offerRepository->findAllOffers($request->query->getInt('page', 1)),
                'form'               => $form,
                'countOfferToDefine' => $countOfferToDefine,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $offer = new Offer();
        $form  = $this->createForm(AdminOfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/offer/new.html.twig',
            [
                'offer' => $offer,
                'form'  => $form,
            ],
        );
    }

    #[Route('/definir-les-technologies', name: 'define', methods: ['GET', 'POST'])]
    public function define(Request $request, EntityManagerInterface $entityManager): Response
    {
        $countOfferToDefine = $this->offerRepository->countOffersWithoutTags();

        if (0 === $countOfferToDefine) {
            return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
        }

        $offer = $this->offerRepository->firstgetOfferWithoutTagsAndIsValidation();

        $form = $this->createForm(AdminOfferToDefineType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offer = $form->getData();

            $tags = $request->request->all()[$form->getName()]['tags'];

            $offer->setActive(false);
            $offer->setValidation(false);

            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $tag = $this->tagRepository->findOneBy(['id' => $tag]);
                    $offer->addTag($tag);
                    $entityManager->persist($offer);
                }
                $offer->setActive(true);
                $offer->setValidation(true);
            }

            $entityManager->persist($offer);
            $entityManager->flush();

            $countOfferToDefine = $this->offerRepository->countOffersWithoutTags();

            if (0 === $countOfferToDefine) {
                return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
            }

            $offer = $this->offerRepository->firstgetOfferWithoutTagsAndIsValidation();

            return $this->redirectToRoute(
                'app_admin_offer_define',
                [
                    'offer'              => $offer,
                    'form'               => $form,
                    'countOfferToDefine' => $countOfferToDefine,
                ],
            );
        }

        return $this->render(
            'pages/admin/offer/define.html.twig',
            [
                'offer'              => $offer,
                'form'               => $form,
                'countOfferToDefine' => $countOfferToDefine,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Offer $offer, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminOfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // if validation is false or no tags, the offer is not active
            if (false === $form->get('validation')->getData() || 0 === count($offer->getTags())) {
                $offer->setActive(false);
            }

            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/offer/edit.html.twig',
            [
                'offer' => $offer,
                'form'  => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Offer $offer, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $offer->getId(), $request->request->get('_token'))) {
            $entityManager->remove($offer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
    }
}
