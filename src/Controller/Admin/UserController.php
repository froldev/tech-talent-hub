<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\AdminSearchUserType;
use App\Form\AdminUserType;
use App\Model\AdminSearchUserData;
use App\Repository\UserRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/utilisateurs', name: 'app_admin_user_')]
#[IsGranted('ROLE_ADMIN')]
class UserController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly UserRepository $userRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $adminSearchUserData = new AdminSearchUserData();
        $form                = $this->createForm(AdminSearchUserType::class, $adminSearchUserData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchUserData->page = $request->query->getInt('page', 1);
            $users                     = $this->userRepository->findBySearch($adminSearchUserData);

            return $this->render(
                'pages/admin/parameters/user/index.html.twig',
                [
                    'users' => $users,
                    'form'  => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/parameters/user/index.html.twig',
            [
                'users' => $this->userRepository->findAllUsers($request->query->getInt('page', 1)),
                'form'  => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            // add flash message
            $this->addFlash('success', 'Utilisateur ajouté avec succès.');

            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/user/new.html.twig',
            [
                'user' => $user,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render(
            'pages/admin/parameters/user/show.html.twig',
            [
                'user' => $user,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserPasswordHasherInterface $hasher): Response
    {
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (
                null !== $form->getData()->getPlainPassword()
                && $hasher->isPasswordValid($user, $form->getData()->getPlainPassword())
            ) {
                $user->setPassword(
                    $hasher->hashPassword($user, $form->getData()->getPlainPassword()),
                );
            }

            $this->entityManager->flush();

            // add flash message
            $this->addFlash('success', 'Utilisateur modifié avec succès.');

            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/user/edit.html.twig',
            [
                'user' => $user,
                'form' => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(
        Request $request,
        User $user,
    ): Response {
        if (null !== $this->getUser()) {
            $this->redirectToRoute('app_login');
        }

        $currentUser = $this->getUser();

        if ($currentUser instanceof User && $this->userService->verifyIfUserIsAdmin($currentUser)) {
            throw new Exception('Vous ne pouvez pas supprimer un administrateur.');
        }

        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $this->container->get('security.token_storage')->setToken(null);
            $this->userService->deleteUser($user);

            // add flash message
            $this->addFlash('success', 'Votre compte a été supprimé avec succès.');

            return $this->redirectToRoute('app_home_index');
        }

        return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
