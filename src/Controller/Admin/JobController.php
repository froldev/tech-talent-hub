<?php

namespace App\Controller\Admin;

use App\Constant\JobConstants;
use App\Entity\Job;
use App\Form\AdminJobToDefineType;
use App\Form\AdminJobType;
use App\Form\AdminSearchJobType;
use App\Model\AdminSearchJobData;
use App\Repository\JobRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/metiers', name: 'app_admin_job_')]
#[IsGranted('ROLE_ADMIN')]
class JobController extends AbstractController
{
    public function __construct(
        private readonly JobRepository $jobRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $countJobToDefine = $this->jobRepository->countJobWithParentIsToDefine();

        $adminSearchJobData = new AdminSearchJobData();
        $form               = $this->createForm(AdminSearchJobType::class, $adminSearchJobData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchJobData->page = $request->query->getInt('page', 1);
            $jobs                     = $this->jobRepository->findBySearch($adminSearchJobData);

            return $this->render(
                'pages/admin/job/index.html.twig',
                [
                    'jobs'             => $jobs,
                    'form'             => $form,
                    'countJobToDefine' => $countJobToDefine,
                    'parent'           => JobConstants::LEVEL_PARENT,
                    'child'            => JobConstants::LEVEL_CHILD,
                ],
            );
        }

        return $this->render(
            'pages/admin/job/index.html.twig',
            [
                'jobs'             => $this->jobRepository->findAllJobs($request->query->getInt('page', 1)),
                'form'             => $form,
                'countJobToDefine' => $countJobToDefine,
                'parent'           => JobConstants::LEVEL_PARENT,
                'child'            => JobConstants::LEVEL_CHILD,
            ],
        );
    }

    #[Route('/ajouter/{level}', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, string $level): Response
    {
        $level = $request->attributes->get('_route_params')['level'];

        $job  = new Job();
        $form = $this->createForm(AdminJobType::class, $job, ['level' => $level]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (JobConstants::LEVEL_PARENT === $level) {
                $job->setParent(null);
            }

            // if parent is exclude, child is exclude
            $this->verifyExcludeParent($job);

            $this->entityManager->persist($job);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_job_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/job/new.html.twig',
            [
                'job'   => $job,
                'form'  => $form,
                'level' => (JobConstants::LEVEL_CHILD === $level),
            ],
        );
    }

    #[Route('/definir-le-metier', name: 'define', methods: ['GET', 'POST'])]
    public function define(Request $request): Response
    {
        $countJobToDefine = $this->jobRepository->countJobWithParentIsToDefine();

        if (0 === $countJobToDefine) {
            return $this->redirectToRoute('app_admin_job_index', [], Response::HTTP_SEE_OTHER);
        }

        $jobToBeDefine = $this->jobRepository->findOneBy(['name' => JobConstants::TO_DEFINE]);
        $job           = $this->jobRepository->findTheFirstJobAboutParent($jobToBeDefine);

        $form = $this->createForm(AdminJobToDefineType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();

            $idParent = $request->request->all()['admin_job_to_define']['parent'];
            $parent   = $this->jobRepository->find($idParent);
            $job->setParent($parent);

            if (null !== $parent && JobConstants::EXCLUDE !== $parent->getName()) {
                $job->setActive(true);
            }

            $this->entityManager->persist($job);
            $this->entityManager->flush();

            $countJobToDefine = $this->jobRepository->countJobWithParentIsToDefine();

            if (0 === $countJobToDefine) {
                return $this->redirectToRoute('app_admin_job_index', [], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute(
                'app_admin_job_define',
                [
                    'job'              => $job,
                    'form'             => $form,
                    'countJobToDefine' => $countJobToDefine,
                ],
                Response::HTTP_SEE_OTHER,
            );
        }

        return $this->render(
            'pages/admin/job/define.html.twig',
            [
                'job'              => $job,
                'form'             => $form,
                'countJobToDefine' => $countJobToDefine,
            ],
        );
    }

    #[Route('/{id}/modifier/{level}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, string $level): Response
    {
        $job   = $this->jobRepository->find($request->attributes->get('_route_params')['id']);
        $level = $request->attributes->get('_route_params')['level'];

        $form = $this->createForm(AdminJobType::class, $job, ['level' => $level]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (JobConstants::LEVEL_PARENT === $level) {
                $job->setParent(null);
            }

            // if parent is exclude, child is exclude
            $this->verifyExcludeParent($job);

            $this->entityManager->persist($job);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_job_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/job/edit.html.twig',
            [
                'job'   => $job,
                'form'  => $form,
                'level' => (JobConstants::LEVEL_CHILD === $level),
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Job $job, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $job->getId(), $request->request->get('_token'))) {
            $entityManager->remove($job);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_job_index', [], Response::HTTP_SEE_OTHER);
    }

    private function verifyExcludeParent(Job $job): void
    {
        if (null !== $job->getParent() && JobConstants::EXCLUDE === $job->getParent()->getName()) {
            $job->setActive(false);
        }
    }
}
