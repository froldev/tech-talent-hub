<?php

namespace App\Controller\Admin;

use App\Entity\Navbar;
use App\Form\AdminNavbarType;
use App\Form\AdminSearchNavbarType;
use App\Model\AdminSearchNavbarData;
use App\Repository\NavbarRepository;
use App\Service\RoutesService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/menu', name: 'app_admin_navbar_')]
#[IsGranted('ROLE_ADMIN')]
class NavbarController extends AbstractController
{
    public function __construct(
        private readonly NavbarRepository $navbarRepository,
        private EntityManagerInterface $entityManager,
        private readonly RoutesService $routesService,
    ) {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $adminSearchNavbarData = new AdminSearchNavbarData();
        $form                  = $this->createForm(AdminSearchNavbarType::class, $adminSearchNavbarData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchNavbarData->page = $request->query->getInt('page', 1);
            $navbars                     = $this->navbarRepository->findBySearch($adminSearchNavbarData);

            return $this->render(
                'pages/admin/parameters/navbar/index.html.twig',
                [
                    'navbars' => $navbars,
                    'form'    => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/parameters/navbar/index.html.twig',
            [
                'navbars' => $this->navbarRepository->findAllNavbars($request->query->getInt('page', 1)),
                'form'    => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $navbar = new Navbar();
        $form   = $this->createForm(
            AdminNavbarType::class,
            $navbar,
            [
                'position' => $this->navbarRepository->getPositions(),
                'routes'   => $this->routesService->getRoutesForNavbarFront(),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($navbar);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_navbar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/navbar/new.html.twig',
            [
                'navbar' => $navbar,
                'form'   => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Navbar $navbar): Response
    {
        return $this->render(
            'pages/admin/parameters/navbar/show.html.twig',
            [
                'navbar' => $navbar,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Navbar $navbar): Response
    {
        $form = $this->createForm(
            AdminNavbarType::class,
            $navbar,
            [
                'position' => $this->navbarRepository->getPositions(),
                'routes'   => $this->routesService->getRoutesForNavbarFront(),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_admin_navbar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/parameters/navbar/edit.html.twig',
            [
                'navbar' => $navbar,
                'form'   => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Navbar $navbar): Response
    {
        if ($this->isCsrfTokenValid('delete' . $navbar->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($navbar);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_navbar_index', [], Response::HTTP_SEE_OTHER);
    }
}
