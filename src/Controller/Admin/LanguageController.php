<?php

namespace App\Controller\Admin;

use App\Entity\Language;
use App\Form\AdminLanguageType;
use App\Form\AdminSearchLanguageType;
use App\Model\AdminSearchLanguageData;
use App\Repository\LanguageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/langues', name: 'app_admin_language_')]
#[IsGranted('ROLE_ADMIN')]
class LanguageController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(LanguageRepository $languageRepository, Request $request): Response
    {
        $adminSearchLanguageData = new AdminSearchLanguageData();
        $form                    = $this->createForm(AdminSearchLanguageType::class, $adminSearchLanguageData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adminSearchLanguageData->page = $request->query->getInt('page', 1);
            $languages                     = $languageRepository->findBySearch($adminSearchLanguageData);

            return $this->render(
                'pages/admin/language/index.html.twig',
                [
                    'languages' => $languages,
                    'form'      => $form,
                ],
            );
        }

        return $this->render(
            'pages/admin/language/index.html.twig',
            [
                'languages' => $languageRepository->findAllLanguages($request->query->getInt('page', 1)),
                'form'      => $form,
            ],
        );
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $language = new Language();
        $form     = $this->createForm(AdminLanguageType::class, $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($language);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_language_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/language/new.html.twig',
            [
                'language' => $language,
                'form'     => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Language $language): Response
    {
        return $this->render(
            'pages/admin/language/show.html.twig',
            [
                'language' => $language,
            ],
        );
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Language $language, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AdminLanguageType::class, $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_language_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'pages/admin/language/edit.html.twig',
            [
                'language' => $language,
                'form'     => $form,
            ],
        );
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Language $language, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $language->getId(), $request->request->get('_token'))) {
            $entityManager->remove($language);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_language_index', [], Response::HTTP_SEE_OTHER);
    }
}
