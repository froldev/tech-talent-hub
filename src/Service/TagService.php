<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Offer;
use App\Entity\Tag;
use App\Repository\OfferRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class TagService
{
    public function __construct(
        private readonly OfferRepository $offerRepository,
        private readonly TagRepository $tagRepository,
        private readonly EmailService $emailService,
        private readonly TagDataService $tagDataService,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Function to search tag in title or description and add tag in offer.
     */
    public function searchAndAddTagsInOffers(?array $offers = null): void
    {
        // search tags active in offers
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->warning('START : Search tags in offers');
        $console->progressStart(count($offers));

        // if empty is offers
        if (empty($offers)) {
            return;
        }

        foreach ($offers as $offer) {
            $this->getAllTagsInOffer($offer);
            $console->progressAdvance();
        }
        $console->progressFinish();
        $console->success('END : Search tags in offers');
    }

    /**
     * Function to remove tag in offer.
     */
    public function removeTagsInOffers(): void
    {
        // search tags active in offers
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->title('START : Removing tags in offers');

        $offers = $this->offerRepository->findAll();
        $console->progressStart(count($offers));

        foreach ($offers as $offer) {
            $tags = $offer->getTags();

            foreach ($tags as $tag) {
                $offer->removeTag($tag);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
            $console->progressAdvance();
        }
        $console->progressFinish();

        $console->success('END : Removing tags in offers');
    }

    /**
     * Function to delete all tags.
     */
    public function deleteAllTags(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->title('START : Deleting all tags');

        $tags = $this->tagRepository->findAll();

        $console->progressStart(count($tags));

        foreach ($tags as $tag) {
            $this->entityManager->remove($tag);
            $this->entityManager->flush();
            $console->progressAdvance();
        }
        $console->progressFinish();

        $console->success('END : Deleting all tags');
    }

    /**
     * Function to count offers for each tag.
     */
    public function countOffersForEachTag(): int
    {
        $console   = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $tags      = $this->tagRepository->findBy(['active' => true]);

        if (!empty($tags)) {
            $console->warning('START : Count offers for each tag');

            foreach ($tags as $tag) {
                $tag->setCountOffer($tag->getOffers()->count());
                $this->entityManager->persist($tag);
            }
            $this->entityManager->flush();
            $console->success('END : Count offers for each tag');
        }

        return count($tags);
    }

    /**
     * Function to create parent tag.
     */
    public function createParentTag(array $tag): Tag
    {
        $parent = (new Tag())
            ->setName($tag['name'])
            ->setParent(null)
            ->setColor($tag['color'])
            ->setDescription($tag['description'])
            ->setActive(true)
            ->setCountOffer(0);

        $this->entityManager->persist($parent);
        $this->entityManager->flush();

        return $parent;
    }

    /**
     * Function to create child tag.
     */
    public function createChildTag(string $child, string $parent): Tag
    {
        $parent = $this->getParentTagForChidTag($parent);

        $child = (new Tag())
            ->setName($child)
            ->setParent($parent)
            ->setColor(null)
            ->setDescription(null)
            ->setActive(true)
            ->setCountOffer(0);

        $this->entityManager->persist($child);
        $this->entityManager->flush();

        return $child;
    }

    /**
     * Function to get parent tag for child tag.
     */
    public function getParentTagForChidTag(string $parent): Tag
    {
        return $this->tagRepository->findOneBy(['name' => $parent]);
    }

    /**
     * Function to import tags.
     */
    public function importTags(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // import parent tags
        $countParent = $this->importParentTag();

        // create children
        $countChild = $this->importChildrenTag();

        // send email
        $console->title('Send email');
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Import des Technologies',
            'import_tags',
            [
                'date'        => date('d/m/Y H:i:s'),
                'countParent' => $countParent,
                'countChild'  => $countChild,
            ],
        );
        $console->success('Email sent successfully');
    }

    /**
     * Function to import parent tags.
     */
    public function importParentTag(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->warning('Importing parent tags');
        $listOfTags = $this->tagDataService->getListOfParentTags();

        $totalParent = count($listOfTags);

        $console->progressStart($totalParent);

        foreach ($listOfTags as $tag) {
            // saerch tag in database
            $parentTag = $this->tagRepository->findTagByName($tag['name']);

            if (null !== $parentTag) {
                continue;
            }

            // create parent tag
            $parentTag = $this->createParentTag($tag);
            $this->entityManager->persist($parentTag);
            $console->progressAdvance();
        }
        $console->progressFinish();
        $this->entityManager->flush();

        $console->success($totalParent . ' Parent Tags imported');

        return $totalParent;
    }

    /**
     * Function to import child tags.
     */
    public function importChildrentag(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->warning('Importing child tags');
        $listOfTags = $this->tagDataService->getListOfChildrenTags();
        $totalChild = count($listOfTags);

        $console->progressStart($totalChild);

        foreach ($listOfTags as $tag) {
            $parent = $tag['parent'];

            foreach ($tag['children'] as $child) {
                // search tag in database
                $childTag = $this->tagRepository->findTagByName($child);

                if (null !== $childTag) {
                    continue;
                }

                // create child tag
                $childTag = $this->createChildTag($child, $parent);
                $this->entityManager->persist($childTag);
                $console->progressAdvance();
            }
        }
        $console->progressFinish();
        $this->entityManager->flush();

        $console->success($totalChild . ' Child Tags imported');

        return $totalChild;
    }

    /**
     * Function to get child active tags.
     */
    public function getChildActiveTags(): array
    {
        $childActiveTags = $this->tagRepository->findAllChildTags();

        return $childActiveTags;
    }

    /**
     * Function to get child tag by name.
     */
    public function getChildTagByName(string $name): ?Tag
    {
        $childTag = $this->tagRepository->findChildTagByName($name);

        return $childTag;
    }

    private function normalizeTags(array $tags): array
    {
        $normalizedTags = [];

        foreach ($tags as $tag) {
            $normalizedTags[strtolower($tag->getName())] = $tag;
        }

        return $normalizedTags;
    }

    private function findTagsInText(string $text): array
    {
        $foundTags       = [];
        $normalizedText  = strtolower($text);
        $childActiveTags = $this->getChildActiveTags();
        $tags            = $this->normalizeTags($childActiveTags);

        foreach ($tags as $normalizedTag => $originalTag) {
            // Crée une regex pour gérer le pluriel et les erreurs de frappe communes
            $pattern = '/' . preg_quote($normalizedTag, '/') . 's?/i';

            // Vérifie si le tag existe dans le texte
            if (preg_match($pattern, $normalizedText)) {
                $foundTags[] = $originalTag;
            }
        }

        return $foundTags;
    }

    public function getTagsFromOffer(Offer $offer): array
    {
        $title       = $offer->getTitle()       ?? '';
        $description = $offer->getDescription() ?? '';

        $foundTagsInTitle       = $this->findTagsInText($title);
        $foundTagsInDescription = $this->findTagsInText($description);

        // Combine et élimine les doublons
        $foundTags = array_unique(array_merge($foundTagsInTitle, $foundTagsInDescription));

        return $foundTags;
    }

    /**
     * Function to get child active tags query builder.
     */
    public function getChildActiveTagsQueryBuilder(): QueryBuilder
    {
        return $this->tagRepository->findAllChildActiveTagsQueryBuilder();
    }

    /**
     * Function to add or create formation.
     */
    public function getAllTagsInOffer(Offer $offer): void
    {
        $tagsToAdd = $this->getTagsFromOffer($offer);

        if (empty($tagsToAdd)) {
            return;
        }

        foreach ($tagsToAdd as $tagEntity) {
            if (!$offer->getTags()->contains($tagEntity)) {
                $offer->addTag($tagEntity);
                $this->entityManager->persist($offer);
            }
        }
        $this->entityManager->flush();
    }
}
