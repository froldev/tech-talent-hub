<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\JobConstants;
use App\Entity\Job;
use App\Repository\JobRepository;
use App\Util\JobChoice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class JobService
{
    public function __construct(
        private readonly JobRepository $jobRepository,
        private EntityManagerInterface $entityManager,
        private readonly JobChoice $jobChoice,
    ) {
    }

    /**
     * Get active child jobs.
     */
    public function getActiveChildJobs(): array
    {
        $jobs = $this->jobRepository->findActiveChildren();

        return $jobs;
    }

    /**
     * Create parent job.
     */
    public function createParentJob(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->progressStart(count($this->jobChoice->getListOfParent()));

        $jobEntity = null;
        $count     = 0;

        foreach ($this->jobChoice->getListOfParent() as $parent) {
            // create only if parent not exist
            if (!$this->jobRepository->findOneBy(['name' => $parent])) {
                $jobEntity = (new Job())
                    ->setName($parent)
                    ->setParent(null)
                    ->setCountOffer(0)
                    ->setActive(true);
                $this->entityManager->persist($jobEntity);
                $this->entityManager->flush();
                ++$count;
            }
            $console->progressAdvance();
        }
        $console->progressFinish();

        return $count;
    }

    /**
     * Create children jobs.
     */
    public function createChildrenJobs(array $children): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->progressStart(count($children));

        $count = 0;

        foreach ($children as $child) {
            // create only if child not exist
            if (!$this->jobRepository->findOneBy(['name' => $child])) {
                $parent = null;

                // Recherche du parent correspondant au mot clé
                foreach ($this->jobChoice->getListOfKeywordsMapping() as $keyword => $parentName) {
                    $search = stripos(strtolower($child), $keyword);

                    if (false !== $search) {
                        $parent = $this->jobRepository->findOneBy(['name' => $parentName]);
                        break;
                    }
                }

                // If no parent is found, use the default parent
                if (null === $parent) {
                    $parent = $this->jobRepository->findOneBy(['name' => JobConstants::TO_DEFINE]);
                }

                // Création de l'entité Job
                $jobEntity = (new Job())
                    ->setName($child)
                    ->setParent($parent)
                    ->setCountOffer(0)
                    ->setActive(true);
                $this->entityManager->persist($jobEntity);
                $this->entityManager->flush();
                ++$count;
            }
            $console->progressAdvance();
        }
        $console->progressFinish();

        return $count;
    }

    /**
     * Count job with parent is to define.
     */
    public function countJobWithParentIsToDefine(): int
    {
        return $this->jobRepository->countJobWithParentIsToDefine();
    }

    /**
     * Get jobs.
     */
    public function getJobs(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // create parents jobs
        $console->warning('START : Create parent jobs');
        $this->createParentJob();
        $console->success('END : Create parent jobs');

        // create children jobs
        $console->warning('START : Create children jobs');
        $this->createChildrenJobs($this->jobChoice->getListOfChildren());
        $console->success('END : Create children jobs');

        // active children jobs
        $console->warning('START : Active children jobs');
        $this->updateActiveJobsIfNotDefined();
        $console->success('END : Active children jobs');
    }

    /**
     * Update active jobs if not defined.
     */
    public function updateActiveJobsIfNotDefined(): void
    {
        $jobs = $this->jobRepository->findAll();

        foreach ($jobs as $job) {
            if (null !== $job->getParent() && JobConstants::TO_DEFINE !== $job->getParent()->getName()) {
                $job->setActive(true);
                $this->entityManager->persist($job);
            }
        }
        $this->entityManager->flush();
    }

    /**
     * Function to get active children jobs.
     */
    public function getActiveChildrenJobs(): array
    {
        return $this->jobRepository->findActiveChildren();
    }

    /**
     * Function to count offers for each job.
     */
    public function countOffersForEachJob(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        $jobs = $this->jobRepository->findAll();

        if (!empty($jobs)) {
            $console->warning('START : Count offers for each job');

            foreach ($jobs as $job) {
                $job->setCountOffer($job->getOffers()->count());
                $this->entityManager->persist($job);
            }
            $this->entityManager->flush();
            $console->success('END : Count offers for each job');
        }
    }

    /**
     * Function to get job by name.
     */
    public function getJobByName(string $name): ?Job
    {
        return $this->jobRepository->findJobByName($name);
    }
}
