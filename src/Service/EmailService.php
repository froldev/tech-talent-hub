<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class EmailService
{
    public function __construct(
        private MailerInterface $mailer,
    ) {
    }

    /**
     * Sends an email.
     */
    public function sendEmail(
        string $from,
        string $to,
        string $subject,
        string $template,
        array $context,
    ): void {
        // create email
        $email = (new TemplatedEmail())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->htmlTemplate("emails/$template.html.twig")
            ->context($context);

        // send email
        $this->mailer->send($email);
    }
}
