<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\CityDTO;
use App\DTO\CompanyDTO;
use App\DTO\HardSkillDTO;
use App\DTO\JobDTO;
use App\DTO\LanguageDTO;
use App\DTO\PartnerDTO;
use App\DTO\SoftSkillDTO;
use App\DTO\TagDTO;
use App\Repository\TagRepository;

class ApiMatchingService
{
    public function __construct(
        private readonly TagRepository $tagRepository,
        private string $title = 'title',
        private string $description = 'description',
    ) {
    }

    public function matchCity(array $offer): CityDTO
    {
        $city = $offer['lieuTravail'];

        return new CityDTO(
            label: strtoupper($city['libelle'] ?? ''),
            zipCode: $city['codePostal']  ?? null,
            latitude: $city['latitude']   ?? null,
            longitude: $city['longitude'] ?? null,
        );
    }

    public function matchJob(array $offer): JobDTO
    {
        return new JobDTO(
            name: $this->getData('appellationlibelle', $offer),
        );
    }

    public function matchCompany(array $offer): CompanyDTO
    {
        $entreprise = $offer['entreprise'];

        return new CompanyDTO(
            name: $entreprise['nom']                ?? '',
            description: $entreprise['description'] ?? '',
            // logo: $entreprise['logo'] ?? null,
            activitySector: $offer['secteurActiviteLibelle'] ?? null,
        );
    }

    public function matchPartner(array $offer): array
    {
        $partnerEntities = [];

        $partners = $offer['origineOffre']['partenaires'] ?? [];

        foreach ($partners as $entity) {
            $partnerEntities[] = new PartnerDTO(
                name: $entity['nom'] ?? '',
                url: $this->modifyUrlPartner($entity['url'] ?? ''),
                description: $entity['description'] ?? '',
                logo: $entity['logo']               ?? null,
            );
        }

        return $partnerEntities;
    }

    public function matchHardSkill(array $offer): array
    {
        $hardSkillDTOs = [];

        foreach ($offer['competences'] as $skill) {
            $hardSkillDTOs[] = new HardSkillDTO(
                name: $skill['libelle'] ?? '',
            );
        }

        return $hardSkillDTOs;
    }

    public function matchSoftSkill(array $offer): array
    {
        $softSkillDTOs = [];

        foreach ($offer['qualitesProfessionnelles'] as $skill) {
            $softSkillDTOs[] = new SoftSkillDTO(
                name: $skill['libelle']            ?? '',
                description: $skill['description'] ?? '',
            );
        }

        return $softSkillDTOs;
    }

    public function matchTag(): array
    {
        $tagDTOs     = [];
        $addAtTheEnd = [' ', ',', '.', ';', ':', '!', '?', ')', ']', '/'];
        $listOfTags  = $this->tagRepository->findBy(['active' => true]);

        foreach ($listOfTags as $tag) {
            $tagName     = strtolower($tag->getName());
            $title       = strtolower($this->title);
            $description = strtolower($this->description);

            foreach ($addAtTheEnd as $word) {
                if (!$this->tagAlreadyAdded($tagDTOs, $tagName)) {
                    $searchWord = $tagName . $word;

                    if (
                        $this->searchWordInContent($searchWord, $title)
                        || $this->searchWordInContent($searchWord, $description)
                    ) {
                        $tagDTOs[] = new TagDTO($tag->getName());
                        break;
                    }
                }
            }
        }

        return $tagDTOs;
    }

    public function matchLanguage(array $offer): array
    {
        $languageDTOs = [];

        foreach ($offer['langues'] as $language) {
            $languageDTOs[] = new LanguageDTO(
                name: $language['libelle'] ?? '',
            );
        }

        return $languageDTOs;
    }

    /**
     * Function to get the data.
     */
    private function getData(string $key, array $entity): string|bool
    {
        $data = '';

        if (array_key_exists($key, $entity)) {
            $data = $entity[$key];
        }

        return $data;
    }

    /**
     * Function to search word in content.
     */
    private function searchWordInContent(string $word, mixed $content): bool
    {
        return false !== stripos($content, $word);
    }

    /**
     * Function to modify the URL partner.
     */
    private function modifyUrlPartner(string $url): string
    {
        // Parse URL to components
        $parsedUrl = parse_url($url);

        // Check if URL parsing was successful
        if (false === $parsedUrl || !isset($parsedUrl['scheme'], $parsedUrl['host'])) {
            // Return the original URL if parsing fails
            return $url;
        }

        // Rebuild URL before any GET parameters
        return $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
    }

    /**
     * Function to check if the tag is already added.
     */
    private function tagAlreadyAdded(array $entityTags, string $tagName): bool
    {
        return in_array($tagName, $entityTags);
    }
}
