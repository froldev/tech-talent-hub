<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ApiConstants;
use App\Constant\FranceTravailConstants;
use App\Constant\ParameterConstants;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class FetchOffersService
{
    public function __construct(
        private string $clientId,
        private string $clientSecret,
        private readonly ApiClientService $apiClientService,
        private readonly ApiTokenService $apiTokenService,
        private readonly ApiEntityCreationService $apiEntityCreationService,
        private readonly OfferService $offerService,
        private readonly JobService $jobService,
        private readonly ParameterService $parameterService,
        private readonly DepartmentService $departmentService,
        private EntityManagerInterface $entityManager,
    ) {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function fetchAndSaveOffers(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        $console->title('Vérification des paramètres');

        // verify if parameters client_id and client_secret are set
        if (
            !$this->apiTokenService->verifyIfParametersAreSet()
            && (!empty($this->clientId) && !empty($this->clientSecret))
        ) {
            $console->title('Ajout des paramètres');

            // set parameters
            $this->parameterService->setParameters(
                [
                    ParameterConstants::FRANCE_TRAVAIL_CLIENT_ID     => $this->clientId,
                    ParameterConstants::FRANCE_TRAVAIL_CLIENT_SECRET => $this->clientSecret,
                ],
            );
        }

        // disabled all active offers
        $console->title('Désactivation des offres actives');
        $this->offerService->disableOffersActive();

        // get all departments
        $console->title('Récupération des départements');
        $departments = $this->departmentService->getDepartments();

        // get active jobs with parent is not tag exclude
        $console->title('Récupération des métiers actifs');
        $jobs = $this->jobService->getActiveChildrenJobs();

        $console->title('Début de la récupération des offres');
        $departmentCount = 0;
        $batchSize       = $this->parameterService->getValue(ParameterConstants::BATCH_SIZE);

        foreach ($departments as $department) {
            ++$departmentCount;

            // Refresh token every batchSize departments
            if (0 === $departmentCount % $batchSize) {
                $this->apiTokenService->refreshToken();
            }

            foreach ($jobs as $job) {
                $console->warning(
                    'Récupération des offres pour le département ' . $department->getCode() .
                    ' et le métier ' . $job->getName(),
                );

                $url = $this->parameterService->getFranceTravailUrlApi() . '&departement=' .
                    $department->getCode() . '&motsCles=' . $job->getName();

                $datas = $this->apiClientService->fetchData($url);

                if (!empty($datas)) {
                    $offerCount = 0;

                    foreach ($datas as $data) {
                        $this->apiEntityCreationService->verifyIfOfferIsCreated(
                            $data,
                            [
                                ApiConstants::API_CHANNEL     => FranceTravailConstants::NAME,
                                ApiConstants::DEPARTMENT_CODE => $department->getCode(),
                            ],
                        );

                        ++$offerCount;

                        if (0 === $offerCount % $batchSize) {
                            $this->entityManager->flush();
                            $this->entityManager->clear();
                        }
                    }

                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
            }
        }
    }
}
