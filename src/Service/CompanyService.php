<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Company;
use App\Repository\CompanyRepository;

class CompanyService
{
    public function __construct(
        private readonly CompanyRepository $companyRepository,
    ) {
    }

    /**
     * Function to get company by name.
     */
    public function getCompanyByName(string $name): ?Company
    {
        return $this->companyRepository->findOneCompanyByName($name);
    }
}
