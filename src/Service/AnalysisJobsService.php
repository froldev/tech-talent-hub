<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnalysisJobsService
{
    public function __construct(
        private readonly JobService $jobService,
        private readonly EmailService $emailService,
        public array $jobs = [],
    ) {
    }

    /**
     * Analysis jobs.
     */
    public function analysisJobs(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // start of function
        $timeStart = time();

        $createParent = $createChildren = $updateChildren = 0;

        // disable all jobs
        $this->jobService->countOffersForEachJob();

        // end of function
        $timeEnd = time();

        // send email with datas
        $console->warning('START : Send email with datas');
        // send email
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Analyse des Métiers avec l\'API France Travail du ' . date('d/m/Y H:i:s'),
            'analysis_jobs',
            [
                'date'         => date('d/m/Y H:i:s'),
                'jobsToDefine' => $this->jobService->countJobWithParentIsToDefine(),
                'time'         => round(($timeEnd - $timeStart) / 60, 2),
            ],
        );
        $console->success('END : Send email with datas');
    }
}
