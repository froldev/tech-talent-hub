<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Navbar;
use App\Repository\NavbarRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class NavbarService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly NavbarRepository $navbarRepository,
        private readonly NavbarDataService $navbarDataService,
    ) {
    }

    public function importLinksInNavbar(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        $console->warning('START : Get partners');

        foreach ($this->navbarDataService->getListOfNavbars() as $link) {
            $navbar = $this->navbarRepository->findOneBy(['path' => $link['path']]);

            if (!$navbar) {
                $navbar = new Navbar();
                $navbar->setName($link['name']);
                $navbar->setPath($link['path']);
                $navbar->setPosition($link['position']);
                $navbar->setActive($link['actif']);
                $this->entityManager->persist($navbar);
            }
        }
        $this->entityManager->flush();
        $console->success('END : Import links in navbar');

        return 0;
    }
}
