<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Partner;
use App\Repository\PartnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class PartnerService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly PartnerRepository $partnerRepository,
    ) {
    }

    /**
     * Function to verify URL in partners.
     */
    public function verifyUrlInPartners(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // Get partners from the database
        $console->warning('START : Get partners');
        $partners = $this->getPartners();
        $console->success('END : Get partners');

        // Loop through each partner
        $count = 0;
        $console->warning('START : Verify URL in partners');
        $console->progressStart(count($partners));

        foreach ($partners as $partner) {
            // Get partner's URL and slug
            $url  = $partner->getUrl();
            $slug = $partner->getSlug();

            // Check if URL is null
            if (null === $url) {
                // Skip to the next partner if URL is null
                continue;
            }

            // Verify if the slug exists in the URL
            if (false === strpos($url, $slug)) {
                // If slug is not found in URL, set URL to null
                $partner->setUrl(null);
            } else {
                // If slug is found in URL, extract and set the domain of the URL
                $parsedUrl = parse_url($url);
                $url       = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
                $partner->setUrl($url);
            }

            // Persist changes to the partner entity
            $this->entityManager->persist($partner);
            ++$count;
            $console->progressAdvance();
        }

        // Flush changes to the database
        $this->entityManager->flush();
        $console->progressFinish();
        $console->success('END : Verify URL in partners');

        return $count;
    }

    /**
     * Function to count offers for each partner.
     */
    public function countOffersForEachPartner(): int
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        $partners = $this->partnerRepository->findBy(['active' => true]);

        if (!empty($partners)) {
            $console->warning('START : Count offers for each partner');

            foreach ($partners as $partner) {
                $partner->setCountOffer($partner->getOffers()->count());
                $this->entityManager->persist($partner);
            }
            $this->entityManager->flush();
            $console->success('END : Count offers for each partner');
        }

        return count($partners);
    }

    /**
     * Function to get partners without URL.
     */
    public function getPartnersWithoutUrl(): array
    {
        $partners = $this->partnerRepository->getPartnersWithoutUrl();

        return $partners;
    }

    /**
     * Function to count partners without URL.
     */
    public function countPartnersWithoutUrl(): int
    {
        $count = $this->partnerRepository->countPartnersWithoutUrl();

        return $count;
    }

    /**
     * Function to get partners.
     */
    private function getPartners(): array
    {
        $partners = $this->partnerRepository->getPartners();

        return $partners;
    }

    /**
     * Function to get partner by name.
     */
    public function getPartnerByName(string $name): ?Partner
    {
        $partner = $this->partnerRepository->findPartnerByName($name);

        return $partner;
    }

    /**
     * Function to get max position.
     */
    public function getMaxPosition(): int
    {
        $maxPosition = $this->partnerRepository->findMaxPosition();

        return $maxPosition ? $maxPosition : 0;
    }

    /**
     * Function to modify the URL partner.
     */
    public function modifyUrlPartner(string $url, string $name): string
    {
        // Parse URL to components
        $parsedUrl = parse_url($url);

        // Check if URL parsing was successful
        if (false === $parsedUrl || !isset($parsedUrl['scheme'], $parsedUrl['host'])) {
            // Return the original URL if parsing fails
            return $url;
        }

        // Rebuild URL before any GET parameters
        return $parsedUrl['scheme'] . '://' . $parsedUrl['host'];
    }
}
