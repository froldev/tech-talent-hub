<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClientService
{
    public function __construct(
        private HttpClientInterface $client,
        private ApiTokenService $tokenService,
        private ApiResponseHandlerService $responseHandler,
    ) {
    }

    public function fetchData(string $url): array
    {
        if ($this->tokenService->isTokenExpired()) {
            $this->tokenService->refreshToken();
        }

        $response = $this->client->request(
            'GET',
            $url,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->tokenService->getToken(),
                ],
            ],
        );

        $this->waitRandomDelay();

        $statusCode = $response->getStatusCode();

        return $this->responseHandler->handle($statusCode, $response, $url, fn ($newUrl) => $this->fetchData($newUrl));
    }

    /**
     * Function to wait random delay.
     */
    private function waitRandomDelay(): void
    {
        usleep(rand(150000, 250000)); // Random delay between 0.15 and 0.25 seconds
    }
}
