<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\PostConstants;
use App\Constant\TypeConstants;

class PostDataService
{
    private const POSTS = [
        [
            "type"    => PostConstants::SALARY,
            "title"   => "Salaire d'un développeur en 2024",
            "content" => "WeLoveDevs.com est un jobboard qui aide " .
            "les développeurs à être heureux ! Pour cela, il faut " .
            "rompre le tabou sur les salaires : est-ce que cette offre" .
            "d’emploi est au niveau du marché ? Est-ce que je peux avoir " .
            "une augmentation ? <br>Ce baromètre répond à vos questions ! " .
            "C’est aussi un outil pour les recruteurs, ils pourront " .
            "facilement accéder au niveau de rémunération attendu d’un " .
            "développeur et mieux définir les fourchettes de salaire sur " .
            "leurs offres d’emploi !",
            "website" => "WeLoveDevs.com",
            "url"     => "https://welovedevs.com/fr/salaires",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::SALARY,
            "title"   => "Salaire d'un développeur Web en 2024",
            "content" => "Les salaires des développeurs web en France " .
            "peuvent varier en fonction de plusieurs facteurs tels que " .
            "l’expérience, les compétences spécifiques, la localisation, " .
            "le type d’entreprise, et le domaine d’activité. <br>Pour 2024, " .
            "bien que je ne puisse pas fournir des données exactes en temps " .
            "réel, je peux offrir une estimation basée sur les tendances et " .
            "les données disponibles jusqu’à début 2023.",
            "website" => "Welcome to the Jungle",
            "url"     => "https://www.welcometothejungle.com/fr/pages/salaire-metier-developpeur-web",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::SALARY,
            "title"   => "Grille des salaires informatique 2024",
            "content" => "L’année 2023 a marqué un tournant pour " .
            "l’économie informatique, avec une stabilité salariale, des " .
            "défis croissants pour les startups et des changements " .
            "notables dans les pratiques de recrutement.<br>Découvrez " .
            "dans cette analyse de marché, les implications de ces " .
            "tendances et les perspectives pour l’année " .
            "2024.",
            "website" => "Externatic",
            "url"     => "https://www.externatic.fr/grille-des-salaires-2024/",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::SALARY,
            "title"   => "Les salaires de la Tech en 2024",
            "content" => "En Mars 2024 TPC a lancé une grande enquête sur les " .
            "salaires des métiers Tech : Product, Design, Software Engineering, " .
            "et Data.<br>3205 professionnels de junior à C-Level sur chacun des " .
            "métiers ont répondu à cette enquête.<br>État du marché, Salaires " .
            "par métier, secteur, séniorité, avis sur leur situation…<br>Nous " .
            "avons analysé les résultats et développé un rapport complet.",
            "website" => "The Product Crew",
            "url"     => "https://theproductcrew.io/ressources/salaires-de-la-tech-2024/",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::SALARY,
            "title"   => "Quel salaire pour un développeur web en 2024 ?",
            "content" => "Chaque année, le secteur du développement web attire " .
            "de nombreux candidats. En pleine ébullition, le marché évolue " .
            "constamment. Alors, à quoi peut-on s’attendre cette année ? " .
            "Réponse dans notre panorama des salaires pour les développeurs " .
            "web en 2024.",
            "website" => "Wild Code School",
            "url"     => "https://www.wildcodeschool.com/fr-fr/blog/quel-salaire-pour-un-d%C3%A9veloppeur-web-en-2024",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "Evaluations des compétences techniques des développeurs",
            "content" => "Choisissez parmi +1000 tests techniques et faites-vous " .
            "remarquer. Valorisez vos compétences dès maintenant et obtenez un " .
            "certificat !",
            "website" => "Skillvalue",
            "url"     => "https://skillvalue.com/fr/quiz/",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "Mettez-vous au défi de coder",
            "content" => "Coderbyte est une application web qui permettra de " .
            "tester vos candidats à partir de situations réelles de codage. Le " .
            "panel de tests disponibles permettront de juger 43 compétences " .
            "(Ruby, Rust, Swift, ou encore Node.js). Les tarifs sont disponibles " .
            "sur demande et dépendent directement de l’examen sélectionné.",
            "website" => "Coderbyte",
            "url"     => "https://coderbyte.com/developers",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "Jouez et devenez un meilleur dev sur CodinGame",
            "content" => "CodinGame est une plateforme qui propose des exercices " .
            "de programmation dans plus de 25 langages de programmation. Les " .
            "exercices sont classés par difficulté et par langage. Vous pouvez " .
            "également participer à des concours de programmation.",
            "website" => "CodinGame",
            "url"     => "https://www.codingame.com/start",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "Testez vos compétences avec les quiz de W3Schools",
            "content" => "Testez vos compétences en développement web avec des " .
            "quiz en ligne. Les quiz sont classés par catégories et par difficulté. " .
            "Vous pouvez également créer vos propres quiz et les partager avec " .
            "la communauté.",
            "website" => "W3Schools",
            "url"     => "https://www.w3schools.com/quiztest/",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "QCM techniques et quiz pour développeurs et techs",
            "content" => "Plus de 100 QCM pour vous préparer aux entretiens " .
            "d'embauche et évaluer vos connaissances dans différentes technos : " .
            "front-end, back-end, web, gestion de projet...",
            "website" => "WeLoveDevs",
            "url"     => "https://welovedevs.com/fr/app/tests",
            "active"  => TypeConstants::TRUE,
        ],
        [
            "type"    => PostConstants::TEST,
            "title"   => "Test technique : comment le préparer ?",
            "content" => "Développeur, designer, data architect ou encore " .
            "responsable marketing : autant de métiers dont le processus de " .
            "recrutement passe le plus souvent par un entretien technique. Et " .
            "bien que la nature du test ne soit généralement pas connue, il est " .
            "important de s’y préparer afin d’adopter les bons comportements.",
            "website" => "OpenClassRooms",
            "url"     => "https://blog.openclassrooms.com/2019/05/27/test-technique-comment-le-preparer/",
            "active"  => TypeConstants::TRUE,
        ],
    ];

    /**
     * Function to get all posts.
     */
    public function getListOfPosts(): array
    {
        return self::POSTS;
    }
}
