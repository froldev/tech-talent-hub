<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;

class RoutesService
{
    public function __construct(
        private RouterInterface $router,
    ) {
    }

    /**
     * Function to get all routes for the navbar in the front.
     */
    public function getRoutesForNavbarFront(): array
    {
        $routes = [];

        foreach ($this->router->getRouteCollection() as $key => $route) {
            if ($key) {
                if (false !== strpos($key, 'index') && false === strpos($key, 'admin')) {
                    $routes[$key] = $key;
                }
            }

            // search index in Route defaults _controller and not admin in route getpath()
        }

        return $routes;
    }
}
