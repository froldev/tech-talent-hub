<?php

declare(strict_types=1);

namespace App\Service;

class ApiDataExtractorService
{
    /**
     * Function to verify if array key exist.
     */
    public function getDataInArray(string $key, array $data): ?string
    {
        return (array_key_exists($key, $data)) ? $data[$key] : null;
    }

    /**
     * Function to verify if array key exist.
     */
    public function verifiIdKeyExistInArray(string $key, array $data): bool
    {
        return array_key_exists($key, $data);
    }
}
