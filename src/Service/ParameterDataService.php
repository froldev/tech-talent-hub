<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ApiConstants;
use App\Constant\PaginationConstants;
use App\Constant\ParameterConstants;

class ParameterDataService
{
    private const PARAMETERS = [
        [
            'key'     => ParameterConstants::HOMEPAGE_OFFERS_COUNT,
            'value'   => PaginationConstants::TWELVE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::HOMEPAGE_PARTNERS_COUNT,
            'value'   => PaginationConstants::TWELVE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::HOMEPAGE_TAGS_COUNT,
            'value'   => PaginationConstants::NINE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::OFFERS_COUNT,
            'value'   => PaginationConstants::TWENTY_FOUR,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::PARTNERS_COUNT,
            'value'   => PaginationConstants::TWENTY_FOUR,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::TAGS_COUNT,
            'value'   => PaginationConstants::TWENTY_FOUR,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::SALARY_COUNT,
            'value'   => PaginationConstants::TWELVE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::TEST_COUNT,
            'value'   => PaginationConstants::TWELVE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::PARAMETER_COUNT,
            'value'   => PaginationConstants::TWENTY_FOUR,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::SCHEDULER_ANALYSE_PARTNERS,
            'value'   => '01:00',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::SCHEDULER_ANALYSE_TAGS,
            'value'   => '01:30',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::SCHEDULER_ANALYSE_JOBS,
            'value'   => '02:00',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::SCHEDULER_ANALYSE_OFFERS,
            'value'   => '02:30',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::SCHEDULER_FETCH_OFFERS,
            'value'   => '04:00',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::PAGINATION_DEFAULT,
            'value'   => PaginationConstants::TWENTY_FOUR,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_PAGINATION,
        ], [
            'key'     => ParameterConstants::MODULE_SAAS,
            'value'   => false,
            'type'    => ParameterConstants::PARAMETER_BOOLEAN,
            'section' => ParameterConstants::SECTION_MODULE,
        ], [
            'key'     => ParameterConstants::SCHEDULER_DEFAULT,
            'value'   => '01:00',
            'type'    => ParameterConstants::PARAMETER_TIME,
            'section' => ParameterConstants::SECTION_SCHEDULER,
        ], [
            'key'     => ParameterConstants::FRANCE_TRAVAIL_URL_TOKEN,
            'value'   => ApiConstants::API_FRANCE_TRAVAIL_TOKEN,
            'type'    => ParameterConstants::PARAMETER_STRING,
            'section' => ParameterConstants::SECTION_API,
        ], [
            'key'     => ParameterConstants::FRANCE_TRAVAIL_URL_API,
            'value'   => ApiConstants::API_FRANCE_TRAVAIL_URL,
            'type'    => ParameterConstants::PARAMETER_STRING,
            'section' => ParameterConstants::SECTION_API,
        ], [
            'key'     => ParameterConstants::FRANCE_TRAVAIL_CLIENT_ID,
            'value'   => '',
            'type'    => ParameterConstants::PARAMETER_STRING,
            'section' => ParameterConstants::SECTION_API,
        ], [
            'key'     => ParameterConstants::FRANCE_TRAVAIL_CLIENT_SECRET,
            'value'   => '',
            'type'    => ParameterConstants::PARAMETER_STRING,
            'section' => ParameterConstants::SECTION_API,
        ], [
            'key'     => ParameterConstants::FRANCE_TRAVAIL_API_SCOPE,
            'value'   => ApiConstants::API_FRANCE_TRAVAIL_SCOPE,
            'type'    => ParameterConstants::PARAMETER_STRING,
            'section' => ParameterConstants::SECTION_API,
        ], [
            'key'     => ParameterConstants::BATCH_SIZE,
            'value'   => ApiConstants::API_BATCH_SIZE,
            'type'    => ParameterConstants::PARAMETER_INTEGER,
            'section' => ParameterConstants::SECTION_API,
        ],
    ];

    /**
     * Function to get all parameters.
     */
    public function getListOfParameters(): array
    {
        return self::PARAMETERS;
    }
}
