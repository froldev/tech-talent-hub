<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\RoleConstants;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
        private readonly EmailService $emailService,
        private readonly UserDataService $userDataService,
    ) {
    }

    /**
     * Function to create one user.
     */
    public function createUser(string $email, string $password, string $fullname, ?string $role = null): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // check if user already exist in database
        $console->title('Check if user already exist in database');
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user) {
            $console->error('User already exist in database');

            return;
        }
        $console->success('User does not exist in database');

        // check if role is admin
        $isAdmin = (RoleConstants::ADMIN === $role);

        // create user
        $console->title('Create user');
        $user = (new User())
            ->setEmail($email)
            ->setFullname($fullname)
            ->setRoles($isAdmin ? [RoleConstants::ADMIN] : [RoleConstants::USER])
            ->setPlainPassword($password)
            ->setRgpd(true)
            ->setActive(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $console->success('User created successfully');

        // send email
        $console->title('Send email');
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Création d\'un Administrateur sur le site TechTalentHub',
            'create_admin',
            [
                'date'     => date('d/m/Y H:i:s'),
                'mail'     => $email,
                'fullname' => $fullname,
                'admin'    => $isAdmin ? 'oui' : 'non',
            ],
        );
        $console->success('Email sent successfully');
    }

    /**
     * Function to verify if user is admin.
     */
    public function verifyIfUserIsAdmin(User $user): bool
    {
        return in_array('ROLE_ADMIN', $user->getRoles());
    }

    /**
     * Function to delete user.
     */
    public function deleteUser(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * Function to import all users.
     */
    public function importUsers(): void
    {
        foreach ($this->userDataService->getListOfusers() as $userData) {
            $this->createUser(
                $userData['email'],
                $userData['password'],
                $userData['fullname'],
                $userData['role'],
            );
        }
    }
}
