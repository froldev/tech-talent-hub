<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\HardSkill;
use App\Repository\HardSkillRepository;

class HardSkillService
{
    public function __construct(
        private readonly HardSkillRepository $hardSkillRepository,
    ) {
    }

    /**
     * Function to get hard skill by name.
     */
    public function getHardSkillByName(string $name): ?HardSkill
    {
        $hardSkill = $this->hardSkillRepository->findHardSkillByName($name);

        return $hardSkill;
    }
}
