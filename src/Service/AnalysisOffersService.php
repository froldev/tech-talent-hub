<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnalysisOffersService
{
    public function __construct(
        private readonly EmailService $emailService,
        private readonly OfferService $offerService,
        private readonly TagService $tagService,
    ) {
    }

    /**
     * Function to get offers from FranceTravail.
     */
    public function analysisOffersWithoutTags(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // start of function
        $timeStart = time();

        // Verify url in partners
        $console->warning('START: analysis offers without tags');
        $offersWithoutTag = $this->offerService->getOffersWithoutTagsAndIsValidation();
        $console->success('END: analysis offers without tags');

        // search and add tags in offers
        $console->warning('START : Search and add tags in offers');
        $this->tagService->searchAndAddTagsInOffers($offersWithoutTag);
        $console->success('END : Search and add tags in offers');

        // end of function
        $timeEnd = time();

        // send email with datas
        $console->title('START : Send email with datas');
        // send email
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Analyse des Offres sans Technologies de l\'API France Travail du ' . date('d/m/Y H:i:s'),
            'analysis_offers',
            [
                'date'                   => date('d/m/Y H:i:s'),
                'countOffersWithoutTags' => $this->offerService->countOffersWithoutTags(),
                'time'                   => round(($timeEnd - $timeStart) / 60, 2),
            ],
        );
        $console->success('END : Send email with datas');
    }
}
