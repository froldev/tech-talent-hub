<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Offer;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class OfferService
{
    public const OFFER     = 'offer';
    public const CITY      = 'city';
    public const JOB       = 'job';
    public const COMPANY   = 'company';
    public const PARTNER   = 'partner';
    public const TAG       = 'tag';
    public const HARDSKILL = 'hardskill';
    public const SOFTSKILL = 'softskill';
    public const LANGUAGE  = 'language';

    public const PARTNER_NAME          = 'name';
    public const SOFTSKILL_NAME        = 'name';
    public const SOFTSKILL_DESCRIPTION = 'description';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly OfferRepository $offerRepository,
    ) {
    }

    /**
     * Function to disables active offers.
     */
    public function disableOffersActive(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // Récupérer les offres actives
        $offersActive = $this->offerRepository->getOffersActive();
        $totalOffers  = count($offersActive);

        if (0 === $totalOffers) {
            $console->info('Aucune offre active à désactiver.');

            return;
        }

        $console->title('Désactivation des offres actives');
        $console->progressStart($totalOffers);

        $batchSize = 100; // Définir une taille de lot pour optimiser les opérations de persistance
        $i         = 0;

        foreach ($offersActive as $offer) {
            $offer->setActive(false);
            $this->entityManager->persist($offer);

            // On fait un flush tous les $batchSize pour améliorer les performances
            if (($i % $batchSize) === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear(); // Nettoyer l'EntityManager pour éviter de surcharger la mémoire
            }

            ++$i;
            $console->progressAdvance();
        }

        // Flush final pour persister les dernières entités
        $this->entityManager->flush();
        $this->entityManager->clear();

        $console->progressFinish();
        $console->success(sprintf('Désactivation de %d offres terminée avec succès.', $totalOffers));
    }

    /**
     * Function to deletes inactive offers without tags.
     */
    public function getOffersWithoutTagsAndIsValidation(): array
    {
        $offers = $this->offerRepository->getOffersWithoutTagsAndIsValidation();

        return $offers;
    }

    /**
     * Function to count offers without tags.
     */
    public function countOffersWithoutTags(): int
    {
        return $this->offerRepository->countOffersWithoutTags();
    }

    /**
     * Function to check if offer is valid.
     */
    public function isOfferValid(Offer $offer): bool
    {
        return count($offer->getTags()) > 0;
    }

    /**
     * Function to get offer by reference.
     */
    public function getOfferByReference(string $ref): ?Offer
    {
        return $this->offerRepository->findOfferByref($ref);
    }

    /**
     * Function to search tag in title or description and add tag in offer.
     */
    public function searchWordInContent(string $word, mixed $content): bool
    {
        return false !== stripos($content, $word);
    }

    /**
     * Function to delete all offers.
     */
    public function deleteAllOffers(): void
    {
        $offers = $this->offerRepository->findAll();

        foreach ($offers as $offer) {
            $this->entityManager->remove($offer);
        }
        $this->entityManager->flush();
    }
}
