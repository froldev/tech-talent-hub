<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\RoleConstants;
use App\Constant\UserConstants;

class UserDataService
{
    private const USERS = [
        [
            'email'    => UserConstants::ADMIN_EMAIL,
            'password' => UserConstants::ADMIN_PASSWORD,
            'fullname' => UserConstants::ADMIN_FULLNAME,
            'role'     => RoleConstants::ADMIN,
        ],
        [
            'email'    => 'test@test.fr',
            'password' => 'password1234',
            'fullname' => 'test',
            'role'     => RoleConstants::USER,
        ],
    ];

    /**
     * Function to get all users.
     */
    public function getListOfUsers(): array
    {
        return self::USERS;
    }
}
