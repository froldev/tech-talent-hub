<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Department;
use App\Repository\DepartmentRepository;
use Doctrine\ORM\EntityManagerInterface;

class DepartmentService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly RegionService $regionService,
        private readonly DepartmentRepository $departmentRepository,
        private readonly EmailService $emailService,
        private readonly DepartmentDataService $departmentDataService,
    ) {
    }

    /**
     * Function to create departments.
     */
    public function importDepartments(): void
    {
        // delete all departments
        $this->removeDepartments();

        // create departments
        $count = $this->createDepartments();

        // send email
        $this->sendMailForDepartments($count);
    }

    public function createDepartments(): int
    {
        $departments = $this->departmentDataService->getListOfDepartments();
        $count       = 0;

        foreach ($departments as $value) {
            // search if departement exists in the database
            $department = $this->departmentRepository->findDepartementByCode($value['code']);

            if (null !== $department) {
                continue;
            }

            // find region in the database
            $region = $this->regionService->getOneOrNullByCode($value['region_code']);

            if (null !== $region) {
                // create department
                $department = (new Department())
                    ->setCode($value['code'])
                    ->setName($value['name'])
                    ->setRegion($region);
                $this->entityManager->persist($department);
                ++$count;
            }
        }
        $this->entityManager->flush();

        return $count;
    }

    /**
     * Function to send email for departments.
     */
    public function sendMailForDepartments(int $count): void
    {
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Import des Départements',
            'import_territory',
            [
                'date'  => date('d/m/Y H:i:s'),
                'count' => $count,
                'title' => 'Import des Départements',
                'label' => 'Nombre de départements importés',
            ],
        );
    }

    /**
     * Function to remove departements.
     */
    public function removeDepartments(): void
    {
        $departements = $this->departmentRepository->findAll();

        foreach ($departements as $departement) {
            $this->entityManager->remove($departement);
        }
        $this->entityManager->flush();
    }

    /**
     * Function to verify if departments are set.
     */
    public function verifyIfDepartmentsAreSet(): bool
    {
        $departments = $this->departmentRepository->findAll();

        if (empty($departments)) {
            return false;
        }

        return true;
    }

    /**
     * Function to get departments.
     */
    public function getDepartments(): array
    {
        // Verify if departments are imported in database
        $this->verifyDepartmentsAreImportedInDatabase();

        return $this->departmentRepository->findAll();
    }

    /**
     * Function to verify if departments are imported in database.
     */
    public function verifyDepartmentsAreImportedInDatabase(): void
    {
        if (!$this->verifyIfDepartmentsAreSet()) {
            $this->importDepartments();
        }
    }

    /**
     * Function to get department by zip code.
     */
    public function getDepartmentByCode(string $zipCode): ?Department
    {
        return $this->departmentRepository->findDepartementByCode($zipCode);
    }
}
