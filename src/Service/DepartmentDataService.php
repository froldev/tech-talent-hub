<?php

declare(strict_types=1);

namespace App\Service;

class DepartmentDataService
{
    private const DEPARTMENTS = [
        [
            'code'        => '01',
            'name'        => 'Ain',
            'region_code' => '84',
        ],
        [
            'code'        => '02',
            'name'        => 'Aisne',
            'region_code' => '32',
        ],
        [
            'code'        => '03',
            'name'        => 'Allier',
            'region_code' => '84',
        ],
        [
            'code'        => '04',
            'name'        => 'Alpes de Haute Provence',
            'region_code' => '93',
        ],
        [
            'code'        => '05',
            'name'        => 'Hautes Alpes',
            'region_code' => '93',
        ],
        [
            'code'        => '06',
            'name'        => 'Alpes Maritimes',
            'region_code' => '93',
        ],
        [
            'code'        => '07',
            'name'        => 'Ardèche',
            'region_code' => '84',
        ],
        [
            'code'        => '08',
            'name'        => 'Ardennes',
            'region_code' => '44',
        ],
        [
            'code'        => '09',
            'name'        => 'Ariège',
            'region_code' => '76',
        ],
        [
            'code'        => '10',
            'name'        => 'Aube',
            'region_code' => '44',
        ],
        // ...
        [
            'code'        => '976',
            'name'        => 'Mayotte',
            'region_code' => '06',
        ],
    ];

    /**
     * Function to get all departments.
     */
    public function getListOfDepartments(): array
    {
        return self::DEPARTMENTS;
    }
}
