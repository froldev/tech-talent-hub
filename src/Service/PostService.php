<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class PostService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly EmailService $emailService,
        private readonly PostDataService $postDataService,
    ) {
    }

    public function importPosts(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        $console->warning('START : Delete all Posts');
        $this->deleteAllPosts();
        $console->success('END : Delete all Posts');

        $console->warning('START : Import all Posts');
        $count = 0;

        $console->progressStart(count($this->postDataService->getListOfPosts()));

        foreach ($this->postDataService->getListOfPosts() as $post) {
            $newPost = $this->createOnePost($post);
            $this->entityManager->persist($newPost);
            ++$count;
            $console->progressAdvance();
        }
        $this->entityManager->flush();
        $console->progressFinish();
        $console->success('END : Import all Posts');

        // send email
        $this->sendEmail($count);
    }

    /**
     * Function to create one post.
     */
    public function createOnePost(array $post): Post
    {
        $post = (new Post())
            ->setType($post['type'] ?? null)
            ->setTitle($post['title'] ?? null)
            ->setContent($post['content'] ?? null)
            ->setWebsite($post['website'] ?? null)
            ->setUrl($post['url'] ?? null)
            ->setImageName($post['image'] ?? null)
            ->setActive($post['active'] ?? false);

        return $post;
    }

    /**
     * Function to send email.
     */
    private function sendEmail(int $count): void
    {
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Import des Posts',
            'import_posts',
            [
                'date'  => date('d/m/Y H:i:s'),
                'title' => 'Import des Posts',
                'count' => $count,
            ],
        );
    }

    private function deleteAllPosts(): void
    {
        $posts = $this->entityManager->getRepository(Post::class)->findAll();

        foreach ($posts as $post) {
            $this->entityManager->remove($post);
        }
        $this->entityManager->flush();
    }
}
