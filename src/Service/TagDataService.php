<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ColorConstants;
use App\Constant\TagConstants;

class TagDataService
{
    private const PARENT_TAGS = [
        [
            "name"        => TagConstants::BACK,
            "color"       => ColorConstants::RED,
            "description" => TagConstants::BACK_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::FRONT,
            "color"       => ColorConstants::BLUE,
            "description" => TagConstants::FRONT_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::MOBILE,
            "color"       => ColorConstants::YELLOW,
            "description" => TagConstants::MOBILE_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::DEVOPS,
            "color"       => ColorConstants::GREEN,
            "description" => TagConstants::DEVOPS_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::DATA,
            "color"       => ColorConstants::SKY,
            "description" => TagConstants::DATA_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::DATABASE,
            "color"       => ColorConstants::CYAN,
            "description" => TagConstants::DATABASE_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::TEST,
            "color"       => ColorConstants::ORANGE,
            "description" => TagConstants::TEST_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::CLOUD,
            "color"       => ColorConstants::GRAY,
            "description" => TagConstants::CLOUD_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::TOOLING,
            "color"       => ColorConstants::EMERALD,
            "description" => TagConstants::TOOLING_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::MANAGEMENT,
            "color"       => ColorConstants::PURPLE,
            "description" => TagConstants::MANAGEMENT_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::SECURITY,
            "color"       => ColorConstants::LIGHT,
            "description" => TagConstants::SECURITY_DESCRIPTION,
        ],
        [
            "name"        => TagConstants::OTHERS,
            "color"       => ColorConstants::DARK,
            "description" => TagConstants::OTHERS_DESCRIPTION,
        ],
    ];

    private const CHILDREN_TAGS = [
        TagConstants::BACK => [
            "parent"   => TagConstants::BACK,
            "children" => [
                "Java", "Python", "Ruby", "PHP", "C#", "Golang", "Node.js",
                "Laravel", "Symfony", "Spring Boot", "Express.js", "Django",
                "ASP.NET", "NestJS",
            ],
        ],
        TagConstants::FRONT => [
            "parent"   => TagConstants::FRONT,
            "children" => [
                "JavaScript", "TypeScript", "React", "Vue.js", "Angular",
                "Svelte", "Next.js", "Nuxt.js", "Gatsby", "Bootstrap",
                "Tailwind CSS", "jQuery",
            ],
        ],
        TagConstants::MOBILE => [
            "parent"   => TagConstants::MOBILE,
            "children" => [
                "Swift", "Kotlin", "Flutter", "React Native", "Objective-C",
                "Dart", "Xamarin", "Ionic", "Cordova",
            ],
        ],
        TagConstants::DEVOPS => [
            "parent"   => TagConstants::DEVOPS,
            "children" => [
                "Docker", "Kubernetes", "Terraform", "Ansible",
                "Jenkins", "Prometheus", "Grafana", "CircleCI",
                "GitLab CI/CD", "Travis CI", "Puppet",
            ],
        ],
        TagConstants::DATA => [
            "parent"   => TagConstants::DATA,
            "children" => [
                "MATLAB", "Apache Spark", "Hadoop", "Scala", "SAS",
                "Pandas", "NumPy", "TensorFlow", "PyTorch", "Keras",
            ],
        ],
        TagConstants::DATABASE => [
            "parent"   => TagConstants::DATABASE,
            "children" => [
                "PostgreSQL", "MySQL", "MongoDB", "Redis", "SQLite",
                "Oracle", "MariaDB", "Cassandra", "Firebase",
                "DynamoDB", "CouchDB",
            ],
        ],
        TagConstants::TEST => [
            "parent"   => TagConstants::TEST,
            "children" => [
                "JUnit", "Selenium", "Cypress", "Mocha", "Jest", "RSpec",
                "Pytest", "Karma", "TestNG", "Chai", "Postman",
            ],
        ],
        TagConstants::CLOUD => [
            "parent"   => TagConstants::CLOUD,
            "children" => [
                "AWS", "Azure", "Google Cloud Platform", "IBM Cloud",
                "DigitalOcean", "Heroku", "Linode", "Oracle Cloud",
                "Alibaba Cloud", "Cloudflare",
            ],
        ],
        TagConstants::TOOLING => [
            "parent"   => TagConstants::TOOLING,
            "children" => [
                "Git", "Webpack", "Babel", "ESLint", "Prettier", "Vite",
                "Grunt", "Gulp", "Parcel", "Nx", "Yarn", "NPM",
            ],
        ],
        TagConstants::MANAGEMENT => [
            "parent"   => TagConstants::MANAGEMENT,
            "children" => [
                "Jira", "Trello", "Asana", "Monday.com", "Basecamp",
                "Slack", "ClickUp", "Notion", "Confluence", "Microsoft Teams",
            ],
        ],
        TagConstants::SECURITY => [
            "parent"   => TagConstants::SECURITY,
            "children" => [
                "Metasploit", "Burp Suite", "Wireshark", "Nmap", "Snort",
                "OpenVAS", "Nessus", "Maltego", "OWASP ZAP", "Aircrack-ng",
            ],
        ],
        TagConstants::OTHERS => [
            "parent"   => TagConstants::OTHERS,
            "children" => [
                "Rust", "Elixir", "Clojure", "Crystal", "Haskell", "Erlang",
                "Julia", "COBOL", "Fortran",
            ],
        ],
    ];

    /**
     * Function to get parent tags.
     */
    public function getListOfParentTags(): array
    {
        return self::PARENT_TAGS;
    }

    /**
     * Function to get child tags.
     */
    public function getListOfChildrenTags(): array
    {
        return self::CHILDREN_TAGS;
    }
}
