<?php

namespace App\Service;

use App\Entity\SoftSkill;
use App\Repository\SoftSkillRepository;

class SoftSkillService
{
    public function __construct(
        private readonly SoftSkillRepository $softSkillRepository,
    ) {
    }

    /**
     * Function to get soft skill by name.
     */
    public function getSoftSkillByName(string $name): ?SoftSkill
    {
        $softSkill = $this->softSkillRepository->findSoftSkillByName($name);

        return $softSkill;
    }
}
