<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Language;
use App\Repository\LanguageRepository;

class LanguageService
{
    public function __construct(
        private readonly LanguageRepository $languageRepository,
    ) {
    }

    /**
     * Function to get language by name.
     */
    public function getLanguageByName(string $name): ?Language
    {
        $language = $this->languageRepository->findLanguageByName($name);

        return $language;
    }
}
