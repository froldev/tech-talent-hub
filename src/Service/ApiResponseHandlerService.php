<?php

declare(strict_types=1);

namespace App\Service;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseHandlerService
{
    private const RESULTATS         = 'resultats';
    private const FILTRE            = 'filtre';
    private const AGREGATION        = 'agregation';
    private const FILTRES_POSSIBLES = 'filtresPossibles';
    private const VALEUR_POSSIBLE   = 'valeurPossible';
    private const NUMBER_RESULTS    = 'nbResultats';
    private const MAX_RESULTS       = 150;

    /**
     * Function to handle response.
     */
    public function handle(int $statusCode, object $response, string $url, callable $fetchData): array
    {
        switch ($statusCode) {
            case Response::HTTP_OK:
                return $this->handleOkResponse($response);
            case Response::HTTP_PARTIAL_CONTENT:
                return $this->handlePartialResponse($response, $url, $fetchData);
            case Response::HTTP_NO_CONTENT:
                return [];
            case Response::HTTP_TOO_MANY_REQUESTS:
                sleep(5);

                return $fetchData($url);
            case Response::HTTP_BAD_REQUEST:
                return [];
            default:
                throw new Exception('Unexpected error: ' . $statusCode);
        }
    }

    /**
     * Handle jobs response.
     */
    private function handleOkResponse(object $response): array
    {
        $results = $response->toArray()[self::RESULTATS];
        unset($response);

        return $results;
    }

    /**
     * Handle partial content.
     */
    private function handlePartialResponse(object $response, string $url, callable $fetchData): array
    {
        $filters = $response->toArray()[self::FILTRES_POSSIBLES];
        $datas   = [];

        foreach ($filters as $filter) {
            $aggregations  = $filter[self::AGREGATION];
            $testResultats = true;
            $valeurs       = [];

            foreach ($aggregations as $aggregation) {
                $valeurs[] = $aggregation[self::VALEUR_POSSIBLE];

                if ($aggregation[self::NUMBER_RESULTS] > self::MAX_RESULTS) {
                    $testResultats = false;
                    break;
                }
            }

            if ($testResultats) {
                foreach ($valeurs as $valeur) {
                    $newUrl = $url . '&' . $filter[self::FILTRE] . '=' . $valeur;
                    $datas  = array_merge($datas, $fetchData($newUrl));
                }

                return $datas;
            }
        }

        $bestFilter = $this->findClosestFilter($filters);

        if ($bestFilter) {
            foreach ($filters as $filter) {
                if ($filter[self::FILTRE] == $bestFilter) {
                    foreach ($filter[self::AGREGATION] as $aggregation) {
                        $newUrl = $url . '&' . $bestFilter . '=' . $aggregation[self::VALEUR_POSSIBLE];
                        $datas  = array_merge($datas, $fetchData($newUrl));
                    }

                    return $datas;
                }
            }
        }

        return $datas;
    }

    /**
     * Find closest filter.
     */
    private function findClosestFilter(array $filters): ?string
    {
        $closestFilter = null;
        $minDifference = PHP_INT_MAX;

        foreach ($filters as $filter) {
            $aggregations = $filter[self::AGREGATION];
            $totalResults = 0;

            foreach ($aggregations as $aggregation) {
                if ($aggregation[self::NUMBER_RESULTS] > self::MAX_RESULTS) {
                    $totalResults += ($aggregation[self::NUMBER_RESULTS] - self::MAX_RESULTS);
                }
            }

            if ($totalResults < $minDifference) {
                $minDifference = $totalResults;
                $closestFilter = $filter[self::FILTRE];
            }
        }

        return $closestFilter;
    }
}
