<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Region;
use App\Repository\RegionRepository;
use Doctrine\ORM\EntityManagerInterface;

class RegionService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly RegionRepository $regionRepository,
        private readonly EmailService $emailService,
        private readonly RegionDataService $regionDataService,
    ) {
    }

    /**
     * Function to create regions.
     */
    public function importRegions(): void
    {
        // delete all regions
        $this->removeRegions();

        // create regions
        $regions = $this->regionDataService->getListOfRegions();
        $count   = 0;

        foreach ($regions as $value) {
            // search if region exist in database
            $region = $this->regionRepository->findOneByCode($value['code']);

            if (null !== $region) {
                continue;
            }

            // create region
            $region = (new Region())
                ->setCode($value['code'])
                ->setName($value['name']);

            $this->entityManager->persist($region);
            ++$count;
        }
        $this->entityManager->flush();

        // send email
        $this->sendMailForRegions($count);
    }

    /**
     * Function to send email for regions.
     */
    public function sendMailForRegions(int $count): void
    {
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Import des Régions',
            'import_territory',
            [
                'date'  => date('d/m/Y H:i:s'),
                'count' => $count,
                'title' => 'Import des Régions',
                'label' => 'Nombre de régions importées',
            ],
        );
    }

    /**
     * Function to remove regions.
     */
    public function removeRegions(): void
    {
        $regions = $this->regionRepository->findAll();

        foreach ($regions as $region) {
            $this->entityManager->remove($region);
        }
        $this->entityManager->flush();
    }

    /**
     * Function to verify if regions are set.
     */
    public function verifyIfRegionsAreSet(): bool
    {
        // Verify if regions exist
        $regions = $this->regionRepository->findAll();

        return count($regions) > 0;
    }

    /**
     * Function to get regions.
     */
    public function getRegions(): array
    {
        // Verify if regions are imported in database
        $this->verifyRegionsAreImportedInDatabase();

        return $this->regionRepository->findAll();
    }

    /**
     * Function to verify if regions are imported in database.
     */
    public function verifyRegionsAreImportedInDatabase(): void
    {
        if (!$this->verifyIfRegionsAreSet()) {
            $this->importRegions();
        }
    }

    /**
     * Function to get region by code.
     */
    public function getOneOrNullByCode(string $code): ?Region
    {
        return $this->regionRepository->findOneByCode($code);
    }
}
