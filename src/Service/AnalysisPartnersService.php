<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnalysisPartnersService
{
    public function __construct(
        private readonly EmailService $emailService,
        private readonly PartnerService $partnerService,
    ) {
    }

    /**
     * Function to get offers from FranceTravail.
     */
    public function analysisPartners(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // start of function
        $timeStart = time();

        // Verify url in partners
        $partnersModify = $this->partnerService->verifyUrlInPartners();

        // count offers for each partner
        $countPartners = $this->partnerService->countOffersForEachPartner();

        // end of function
        $timeEnd = time();

        // send email
        $console->title('Send email');
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Analyse des Partenaires de l\'API France Travail du ' . date('d/m/Y H:i:s'),
            'analysis_partners',
            [
                'date'                    => date('d/m/Y H:i:s'),
                'partnersModify'          => $partnersModify,
                'countPartnersWithoutUrl' => $this->partnerService->countPartnersWithoutUrl(),
                'countPartners'           => $countPartners,
                'time'                    => round(($timeEnd - $timeStart) / 60, 2),
            ],
        );
        $console->success('Email sent successfully');
    }
}
