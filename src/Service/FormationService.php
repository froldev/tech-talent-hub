<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Formation;
use App\Repository\FormationRepository;

class FormationService
{
    public function __construct(
        private readonly FormationRepository $formationRepository,
    ) {
    }

    /**
     * Function to get all formations.
     */
    public function getAllFormations(): array
    {
        return $this->formationRepository->findAll();
    }

    /**
     * Function to get formation by name.
     */
    public function getFormationByName(string $name): ?Formation
    {
        return $this->formationRepository->findFormationByName($name);
    }
}
