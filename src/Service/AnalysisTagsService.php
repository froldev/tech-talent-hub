<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnalysisTagsService
{
    public function __construct(
        private readonly EmailService $emailService,
        private readonly TagService $tagService,
    ) {
    }

    /**
     * Function to get offers from FranceTravail.
     */
    public function analysisTags(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

        // start of function
        $timeStart = time();

        // count offers for each tags
        $countTags = $this->tagService->countOffersForEachTag();

        // end of function
        $timeEnd = time();

        // send email
        $console->title('Send email');
        $this->emailService->sendEmail(
            'contact@techtalenthub.fr',
            'frol.developpement@gmail.com',
            'Analyse des Technologies de l\'API France Travail du ' . date('d/m/Y H:i:s'),
            'analysis_tags',
            [
                'date'      => date('d/m/Y H:i:s'),
                'countTags' => $countTags,
                'time'      => round(($timeEnd - $timeStart) / 60, 2),
            ],
        );
        $console->success('Email sent successfully');
    }
}
