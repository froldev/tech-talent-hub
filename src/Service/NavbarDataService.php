<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\TypeConstants;

class NavbarDataService
{
    private const NAVBARS = [
        [
            'name'     => 'Accueil',
            'path'     => 'app_home_index',
            'position' => 1,
            'actif'    => TypeConstants::TRUE,
        ],
        [
            'name'     => 'Les Offres',
            'path'     => 'app_offer_index',
            'position' => 2,
            'actif'    => TypeConstants::TRUE,
        ],
        [
            'name'     => 'Les Technologies',
            'path'     => 'app_tag_index',
            'position' => 3,
            'actif'    => TypeConstants::FALSE,
        ],
        [
            'name'     => 'Les Partenaires',
            'path'     => 'app_partner_index',
            'position' => 4,
            'actif'    => TypeConstants::FALSE,
        ],
        [
            'name'     => 'Salaires',
            'path'     => 'app_salary_index',
            'position' => 5,
            'actif'    => TypeConstants::TRUE,
        ],
        [
            'name'     => 'Tests',
            'path'     => 'app_test_index',
            'position' => 6,
            'actif'    => TypeConstants::TRUE,
        ],
    ];

    /**
     * Function to get all navbars.
     */
    public function getListOfNavbars(): array
    {
        return self::NAVBARS;
    }
}
