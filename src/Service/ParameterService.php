<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ParameterConstants;
use App\Entity\Parameter;
use App\Repository\ParameterRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

class ParameterService
{
    public function __construct(
        public PaginatorInterface $paginator,
        private EntityManagerInterface $entityManager,
        private readonly ParameterRepository $parameterRepository,
        private readonly ParameterDataService $parameterDataService,
    ) {
    }

    public function getPagination(array $datas, object $request, int $number = 20)
    {
        return $this->paginator->paginate(
            $datas,
            $request->query->getInt('page', 1),
            $number,
        );
    }

    /**
     * Function to create one parameter.
     */
    public function createParameter(
        string $key,
        string|int|bool|DateTimeInterface $value,
        string $type,
        string $section,
    ): Parameter {
        if (empty($key)) {
            throw new InvalidArgumentException('The key cannot be empty.');
        }

        if (empty($type)) {
            throw new InvalidArgumentException('The type cannot be empty.');
        }

        if (empty($section)) {
            throw new InvalidArgumentException('The section cannot be empty.');
        }

        // Valider que la valeur est du bon type selon le type spécifié
        switch ($type) {
            case ParameterConstants::PARAMETER_STRING:
                if (!is_string($value)) {
                    throw new InvalidArgumentException('Expected a string value for type string.');
                }
                break;
            case ParameterConstants::PARAMETER_INTEGER:
                if (!is_int($value)) {
                    throw new InvalidArgumentException('Expected an integer value for type integer.');
                }
                break;
            case ParameterConstants::PARAMETER_BOOLEAN:
                if (!is_bool($value)) {
                    throw new InvalidArgumentException('Expected a boolean value for type boolean.');
                }
                break;
            case ParameterConstants::PARAMETER_DATETIME:
            case ParameterConstants::PARAMETER_TIME:
                if (is_string($value)) {
                    try {
                        $value = new DateTime($value);
                    } catch (Exception $e) {
                        throw new InvalidArgumentException('Expected a valid datetime string for type datetime.');
                    }
                } elseif (!$value instanceof DateTimeInterface) {
                    throw new InvalidArgumentException('Expected a DateTimeInterface value for type datetime.');
                }
                break;
            default:
                throw new InvalidArgumentException('Invalid type specified.');
        }

        // Créer et persister le Parameter
        $parameter = (new Parameter())
            ->setKey($key)
            ->setValue($value)
            ->setType($type)
            ->setSection($section);

        $this->entityManager->persist($parameter);
        $this->entityManager->flush();

        return $parameter;
    }

    /**
     * Function to import all parameters.
     */
    public function importParameters(): void
    {
        $console = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
        $console->warning('START : Import all parameters');

        foreach ($this->parameterDataService->getListOfParameters() as $parameter) {
            $this->createParameter(
                $parameter['key'],
                $parameter['value'],
                $parameter['type'],
                $parameter['section'],
            );
        }

        $console->success('END : Import all parameters');
    }

    /**
     * Function to verify if parameters are set.
     */
    public function verifyIfParametersAreSet(): bool
    {
        // Verify if client_id, client_secret and api_scope are set
        $clientId     = $this->getValue(ParameterConstants::FRANCE_TRAVAIL_CLIENT_ID);
        $clientSecret = $this->getValue(ParameterConstants::FRANCE_TRAVAIL_CLIENT_SECRET);

        if (empty($clientId) || empty($clientSecret)) {
            return false;
        }

        return true;
    }

    /**
     * Function to get France Travail URL Token.
     */
    public function getFranceTravailUrlToken(): string
    {
        return $this->getValue(ParameterConstants::FRANCE_TRAVAIL_URL_TOKEN);
    }

    /**
     * Function to get France Travail URL API.
     */
    public function getFranceTravailUrlApi(): string
    {
        return $this->getValue(ParameterConstants::FRANCE_TRAVAIL_URL_API);
    }

    /**
     * Function to set several parameter.
     */
    public function setParameters(array $parameters): void
    {
        foreach ($parameters as $key => $value) {
            // Vérifier si un paramètre avec cette clé existe déjà
            $parameter   = $this->parameterRepository->findOneBy(['key' => $key]);
            $valueNotSet = '' === $this->getValue($key);

            if ($parameter && $valueNotSet) {
                $parameter->setValue($value);
                $this->entityManager->persist($parameter);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * Function to get value of a parameter.
     */
    public function getValue(string $key): string|int|bool|DateTimeInterface
    {
        $parameter = $this->parameterRepository->findOneBy(['key' => $key]);

        return $parameter ? $parameter->getValue() : '';
    }
}
