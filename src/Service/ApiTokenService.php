<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ParameterConstants;
use App\Repository\ParameterRepository;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use RuntimeException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiTokenService
{
    private const TOKEN_CACHE_KEY            = 'france_travail_api_token';
    private const TOKEN_EXPIRATION_CACHE_KEY = 'france_travail_token_exp';

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly ParameterRepository $parameterRepository,
        private readonly ParameterService $parameterService,
        private readonly CacheItemPoolInterface $cache,
    ) {
    }

    /**
     * Function to get token.
     */
    public function getToken(): string
    {
        // Check if a valid token is already stored in cache
        $cacheItem = $this->cache->getItem(self::TOKEN_CACHE_KEY);

        if ($cacheItem->isHit() && !$this->isTokenExpired()) {
            return $cacheItem->get();
        }

        // If the token is expired or not present, it is refreshed
        return $this->refreshToken();
    }

    /**
     * Checks if the token is expired based on the time stored in the cache.
     */
    public function isTokenExpired(): bool
    {
        // Retrieve cache expiration time
        $expirationCacheItem = $this->cache->getItem(self::TOKEN_EXPIRATION_CACHE_KEY);

        if (!$expirationCacheItem->isHit()) {
            return true;
        }

        $expirationTime = $expirationCacheItem->get();

        // Check if current time exceeds expiration time
        return time() >= $expirationTime;
    }

    /**
     * Retrieves a new token by calling the France Travail API.
     */
    public function refreshToken(): string
    {
        // Check if client_id, client_secret and api_scope are set
        if (!$this->parameterService->verifyIfParametersAreSet()) {
            throw new Exception('Client ID et Client Secret ne sont pas définis.');
        }

        // Call the API to get a new token
        $response = $this->httpClient->request(
            'POST',
            $this->parameterRepository->getValue(ParameterConstants::FRANCE_TRAVAIL_URL_TOKEN),
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'grant_type'    => 'client_credentials',
                    'client_id'     => $this->parameterRepository->getValue(
                        ParameterConstants::FRANCE_TRAVAIL_CLIENT_ID,
                    ),
                    'client_secret' => $this->parameterRepository->getValue(
                        ParameterConstants::FRANCE_TRAVAIL_CLIENT_SECRET,
                    ),
                    'scope'         => $this->parameterRepository->getValue(
                        ParameterConstants::FRANCE_TRAVAIL_API_SCOPE,
                    ),
                ],
            ],
        );

        // Check if the response is successful
        $data = $response->toArray();

        if (!isset($data['access_token'])) {
            throw new RuntimeException('Échec de la récupération du token d\'accès.');
        }

        // Retrieve the new token and expiration time
        $token     = $data['access_token'];
        $expiresIn = $data['expires_in'] ?? 3600;

        // Store the new token in the cache
        $cacheItem = $this->cache->getItem(self::TOKEN_CACHE_KEY);
        $cacheItem->set($token);
        $this->cache->save($cacheItem);

        // Store new expiration time in cache
        $expirationCacheItem = $this->cache->getItem(self::TOKEN_EXPIRATION_CACHE_KEY);
        $expirationCacheItem->set(time() + $expiresIn);
        $this->cache->save($expirationCacheItem);

        return $token;
    }

    /**
     * Function to verify if parameters are set.
     */
    public function verifyIfParametersAreSet(): bool
    {
        // Verify if client_id, client_secret and api_scope are set
        $clientId     = $this->parameterRepository->getValue(ParameterConstants::FRANCE_TRAVAIL_CLIENT_ID);
        $clientSecret = $this->parameterRepository->getValue(ParameterConstants::FRANCE_TRAVAIL_CLIENT_SECRET);

        if (empty($clientId) || empty($clientSecret)) {
            return false;
        }

        return true;
    }
}
