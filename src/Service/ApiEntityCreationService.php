<?php

declare(strict_types=1);

namespace App\Service;

use App\Constant\ApiConstants;
use App\Entity\City;
use App\Entity\Company;
use App\Entity\Formation;
use App\Entity\HardSkill;
use App\Entity\Job;
use App\Entity\Language;
use App\Entity\Offer;
use App\Entity\Partner;
use App\Entity\SoftSkill;
use App\Mapper\CityMapper;
use App\Mapper\CompanyMapper;
use App\Mapper\FormationMapper;
use App\Mapper\HardSkillMapper;
use App\Mapper\JobMapper;
use App\Mapper\LanguageMapper;
use App\Mapper\OfferMapper;
use App\Mapper\PartnerMapper;
use App\Mapper\SoftSkillMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ApiEntityCreationService
{
    public function __construct(
        private readonly ApiMatchingService $apiMatchingService,
        private readonly OfferService $offerService,
        private readonly JobService $jobService,
        private readonly CityService $cityService,
        private readonly DepartmentService $departmentService,
        private readonly CompanyService $companyService,
        private readonly PartnerService $partnerService,
        private readonly HardSkillService $hardSkillService,
        private readonly SoftSkillService $softSkillService,
        private readonly LanguageService $languageService,
        private readonly FormationService $formationService,
        private readonly TagService $tagService,
        private readonly OfferMapper $offerMapper,
        private readonly JobMapper $jobMapper,
        private readonly CityMapper $cityMapper,
        private readonly CompanyMapper $companyMapper,
        private readonly PartnerMapper $partnerMapper,
        private readonly HardSkillMapper $hardSkillMapper,
        private readonly SoftSkillMapper $softSkillMapper,
        private readonly LanguageMapper $languageMapper,
        private readonly FormationMapper $formationMapper,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * Function to verify if offer is created.
     */
    public function verifyIfOfferIsCreated(array $datas, array $options): void
    {
        // Vérifie si l'offre existe déjà
        $offer = $this->checkIfDataExists('offerService', 'getOfferByReference', $datas['id']);

        if ($offer) {
            if ($this->offerService->isOfferValid($offer)) {
                $offer->setActive(true);
                $offer->setValidation(true);
            }
        } else {
            if (!$this->isPostalCodeSet($datas, 'codePostal')) {
                return;
            }

            $job = $city = $company = null;

            if (array_key_exists('appellationlibelle', $datas)) {
                $job = $this->getOrCreateJob($datas);
            }

            if (array_key_exists('lieuTravail', $datas)) {
                $city = $this->getOrCreateCity($datas, $options);
            }

            if (array_key_exists('entreprise', $datas)) {
                $company = $this->getOrCreateCompany($datas);
            }

            $offer = $this->offerMapper->mapFromApiData($datas, $job, $city, $company);

            $this->addOrCreatePartner($offer, $datas);
            $this->addOrCreateHardSkill($offer, $datas);
            $this->addOrCreateSoftSkill($offer, $datas);
            $this->addOrCreateLanguage($offer, $datas);
            $this->addOrCreateFormation($offer, $datas);
            $this->addTags($offer);

            if ($this->offerService->isOfferValid($offer)) {
                $offer->setActive(true);
                $offer->setValidation(true);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * Function to check if postal code is set.
     */
    private function isPostalCodeSet(array $datas, string $key): bool
    {
        return array_key_exists($key, $datas['lieuTravail']);
    }

    /**
     * Function to check if data exists.
     */
    private function checkIfDataExists($serviceName, $method, ...$params): ?object
    {
        // Construire dynamiquement le nom du service et de la méthode
        if (property_exists($this, $serviceName)) {
            $service = $this->{$serviceName};

            if (method_exists($service, $method)) {
                // Appeler la méthode avec un tableau de paramètres
                $result = call_user_func_array([$service, $method], $params);

                return $result;
            }
        }

        return null;
    }

    /**
     * Get or create a job.
     */
    private function getOrCreateJob(array $datas): ?Job
    {
        if (!array_key_exists('appellationlibelle', $datas)) {
            return null;
        }

        // Check if the job already exists
        $job = $this->checkIfDataExists('jobService', 'getJobByName', $datas['appellationlibelle']);

        if ($job) {
            return $job;
        }

        return $this->jobMapper->mapFromApiData($datas);
    }

    /**
     * Function to get or create city.
     */
    private function getOrCreateCity(array $datas, array $options): ?City
    {
        if (!array_key_exists('lieuTravail', $datas)) {
            return null;
        }

        // Check if the city already exists
        if (array_key_exists('libelle', $datas['lieuTravail'])) {
            $name    = $this->cityService->getCityLabel($datas['lieuTravail']['libelle']);
            $zipCode = $datas['lieuTravail']['codePostal'];

            $label = $zipCode . ' - ' . $name;

            $city = $this->checkIfDataExists('cityService', 'getCityByLabel', $label);

            if ($city) {
                return $city;
            }

            return $this->cityMapper->mapFromApiData($datas, $options[ApiConstants::DEPARTMENT_CODE]);
        }

        return null;
    }

    /**
     * Function to get or create company.
     */
    private function getOrCreateCompany(array $datas): ?Company
    {
        if (!array_key_exists('entreprise', $datas)) {
            return null;
        }

        if (array_key_exists('nom', $datas['entreprise'])) {
            $company = $this->companyService->getCompanyByName($datas['entreprise']['nom']);

            $company = $this->checkIfDataExists('companyService', 'getCompanyByName', $datas['entreprise']['nom']);

            if ($company) {
                return $company;
            }

            return $this->companyMapper->mapFromApiData($datas);
        }

        return null;
    }

    /**
     * Function to add or create partner.
     */
    private function addOrCreatePartner(Offer $offer, array $datas): void
    {
        if (array_key_exists('partenaires', $datas['origineOffre'])) {
            foreach ($datas['origineOffre']['partenaires'] as $data) {
                $partner = $this->getOrCreatePartner($data);
                $offer->addPartner($partner);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * Function to get or create partner.
     */
    private function getOrCreatePartner(array $datas): ?Partner
    {
        if (!array_key_exists('nom', $datas)) {
            return null;
        }

        // Check if the partner already exists
        $partner = $this->checkIfDataExists('partnerService', 'getPartnerByName', $datas['nom']);

        if ($partner) {
            return $partner;
        }

        return $this->partnerMapper->mapFromApiData($datas);
    }

    /**
     * Function to add or create hard skill.
     */
    private function addOrCreateHardSkill(Offer $offer, array $datas): void
    {
        if (array_key_exists('competences', $datas)) {
            foreach ($datas['competences'] as $data) {
                $hardSkill = $this->getOrCreateHardSkill($data);
                $offer->addHardSkill($hardSkill);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * Function to get or create hard skill.
     */
    private function getOrCreateHardSkill(array $datas): ?HardSkill
    {
        if (!array_key_exists('libelle', $datas)) {
            return null;
        }

        // Check if the hard skill already exists
        $hardSkill = $this->checkIfDataExists('hardSkillService', 'getHardSkillByName', $datas['libelle']);

        if ($hardSkill) {
            return $hardSkill;
        }

        return $this->hardSkillMapper->mapFromApiData($datas);
    }

    /**
     * Function to add or create soft skill.
     */
    private function addOrCreateSoftSkill(Offer $offer, array $datas): void
    {
        if (array_key_exists('qualitesProfessionnelles', $datas)) {
            foreach ($datas['qualitesProfessionnelles'] as $data) {
                $softSkill = $this->getOrCreateSoftSkill($data);
                $offer->addSoftSkill($softSkill);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * Function to get or create soft skill.
     */
    private function getOrCreateSoftSkill(array $datas): ?SoftSkill
    {
        if (!array_key_exists('libelle', $datas)) {
            return null;
        }

        // Check if the soft skill already exists
        $softSkill = $this->checkIfDataExists('softSkillService', 'getSoftSkillByName', $datas['libelle']);

        if ($softSkill) {
            return $softSkill;
        }

        return $this->softSkillMapper->mapFromApiData($datas);
    }

    /**
     * Function to add or create language.
     */
    private function addOrCreateLanguage(Offer $offer, array $datas): void
    {
        if (array_key_exists('langues', $datas)) {
            foreach ($datas['langues'] as $data) {
                $language = $this->getOrCreateLanguage($data);
                $offer->addLanguage($language);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * Function to get or create language.
     */
    private function getOrCreateLanguage(array $datas): ?Language
    {
        if (!array_key_exists('libelle', $datas)) {
            return null;
        }

        // Check if the language already exists
        $language = $this->checkIfDataExists('languageService', 'getLanguageByName', $datas['libelle']);

        if ($language) {
            return $language;
        }

        return $this->languageMapper->mapFromApiData($datas);
    }

    /**
     * Function to add or create formation.
     */
    private function addTags(Offer $offer): void
    {
        $this->tagService->getAllTagsInOffer($offer);
    }

    /**
     * Function to add or create formation.
     */
    private function addOrCreateFormation(Offer $offer, array $datas): void
    {
        if (array_key_exists('formations', $datas)) {
            foreach ($datas['formations'] as $data) {
                $formation = $this->getOrCreateFormation($data);
                $offer->addFormation($formation);
                $this->entityManager->persist($offer);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * Function to get or create formation.
     */
    private function getOrCreateFormation(array $datas): ?Formation
    {
        if (!array_key_exists('niveauLibelle', $datas)) {
            return null;
        }

        // Check if the formation already exists
        $formation = $this->checkIfDataExists('formationService', 'getFormationByName', $datas['niveauLibelle']);

        if ($formation) {
            return $formation;
        }

        return $this->formationMapper->mapFromApiData($datas);
    }
}
