<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\City;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;

class CityService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private readonly CityRepository $cityRepository,
    ) {
    }

    /**
     * Function to remove cities.
     */
    public function removeCities(): void
    {
        $departments = $this->cityRepository->findAll();

        foreach ($departments as $department) {
            $this->entityManager->remove($department);
        }
        $this->entityManager->flush();
    }

    /**
     * Function to get city by zip code.
     */
    public function getCityByZipCode(string $zipCode): ?City
    {
        return $this->cityRepository->findCityByZipCode($zipCode);
    }

    /**
     * Function to get the city name.
     */
    public function getCityLabel(string $libelle): string
    {
        $cityName = 'Non Renseigné';

        if (!empty($libelle)) {
            $cityName = ucfirst(strtolower(trim(substr($libelle, strpos($libelle, '-') + 1))));
            $cityName = $this->modifyCityLabel($cityName);
        }

        return $cityName;
    }

    /**
     * Function to get city by label.
     */
    public function getCityByLabel(string $label): ?City
    {
        return $this->cityRepository->findCityByLabel($label);
    }

    /**
     * Function to get city by name.
     */
    public function getCityByName(string $name): ?City
    {
        $name = $this->modifyCityLabel($name);

        return $this->cityRepository->findCityByName($name);
    }

    /**
     * Function to modify the city label.
     */
    public function modifyCityLabel(string $label): string
    {
        return ucwords(strtolower(str_replace('-', ' ', $label)));
    }

    public function getCityByNameAndZipCode(string $name, string $zipCode): ?City
    {
        $name = $this->modifyCityLabel($name);

        return $this->cityRepository->findCityByNameAndZipCode($name, $zipCode);
    }
}
