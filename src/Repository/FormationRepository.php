<?php

namespace App\Repository;

use App\Entity\Formation;
use App\Model\AdminSearchFormationData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Formation>
 */
class FormationRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Formation::class);
    }

    /**
     * Function to get all formations.
     */
    public function findBySearch(AdminSearchFormationData $adminSearchFormationData): PaginationInterface
    {
        $data = $this->createQueryBuilder('h')
            ->orderBy('h.name', 'ASC');

        if ($adminSearchFormationData->q) {
            $data = $data
                ->andWhere('LOWER(h.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchFormationData->q . '%');
        }

        $data = $data->getQuery()->getResult();

        $formations = $this->paginatorInterface->paginate(
            $data,
            $adminSearchFormationData->page,
            20,
        );

        return $formations;
    }

    /**
     * Function to get all formations.
     */
    public function findAllFormations(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('f')
            ->orderBy('f.name', 'ASC')
            ->getQuery()
            ->getResult();

        $formations = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $formations;
    }

    /**
     * Function to get formation by name.
     */
    public function findFormationByName(string $name): ?Formation
    {
        return $this->createQueryBuilder('f')
            ->where('LOWER(f.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
