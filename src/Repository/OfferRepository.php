<?php

namespace App\Repository;

use App\Constant\ParameterConstants;
use App\DTO\OfferDTO;
use App\Entity\Offer;
use App\Model\AdminSearchOfferData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Offer>
 *
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
        private readonly ParameterRepository $parameterRepository,
    ) {
        parent::__construct($registry, Offer::class);
    }

    /**
     * Function to count total offers.
     */
    public function countTotalOffers(): int
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get offers by active status.
     */
    public function getOffersActive(): array
    {
        return $this->createQueryBuilder('o')
            ->where('o.active = :active')
            ->setParameter('active', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get offers by tag.
     */
    public function getOffersByTag(): array
    {
        return $this->createQueryBuilder('o')
            ->select('t.name AS name, COUNT(t.name) AS number_offers')
            ->leftJoin('o.tags', 't')
            ->where('o.active = :active')
            ->setParameter('active', true)
            ->groupBy('t.name')
            ->orderBy('number_offers', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get offers by department.
     */
    public function getOffersByDepartment(): array
    {
        return $this->createQueryBuilder('o')
            ->select('d.code AS department_code, d.name AS department_name, COUNT(d.code) AS number_offers')
            ->leftJoin('o.city', 'c')
            ->leftJoin('c.department', 'd')
            ->where('o.active = :active')
            ->setParameter('active', true)
            ->groupBy('d.code, d.name')
            ->orderBy('number_offers', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get all offers.
     */
    public function findAllOffers(int $page): PaginationInterface
    {
        $datas = $this->createQueryOffer()->orderBy('o.createdAt', 'ASC')->getQuery()->getResult();

        $offers = $this->paginatorInterface->paginate(
            $datas,
            $page,
            $this->parameterRepository->getValue(ParameterConstants::OFFERS_COUNT),
        );

        return $offers;
    }

    /**
     * Function to get offers on the homepage with the most candidacies.
     */
    public function offersOnHomepageWithTheMostLikes(): PaginationInterface
    {
        $pagination = $this->parameterRepository->getValue(ParameterConstants::HOMEPAGE_OFFERS_COUNT);

        $datas = $this->createQueryOffer()
            ->orderBy('COUNT(ol.id)', 'DESC')
            ->setMaxResults($pagination)
            ->getQuery()
            ->getResult();

        $offers = $this->paginatorInterface->paginate(
            $datas,
            1,
            $pagination,
        );

        return $offers;
    }

    /**
     * Function to create query for entity Offer.
     */
    private function createQueryOffer(): object
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.offerCandidacy', 'oc')
            ->leftJoin('o.offerLikes', 'ol')
            ->leftJoin('o.city', 'c')
            ->leftJoin('c.department', 'd')
            ->leftJoin('d.region', 'r')
            ->leftJoin('o.tags', 't')
            ->leftJoin('o.partners', 'p')
            // ->where("o.active = :active")
            // ->andWhere("o.validation = :validation")
            // ->setParameter("active", true)
            // ->setParameter("validation", true)
            ->groupBy('o.id');
    }

    /**
     * Function to search for offers.
     */
    public function findBySearch(AdminSearchOfferData $adminSearchOfferData): PaginationInterface
    {
        $query = $this->createQueryBuilder('o')
            ->leftJoin('o.city', 'c')
            ->leftJoin('c.department', 'd')
            ->orderBy('o.createdAt', 'ASC');

        if (!empty($adminSearchOfferData->q)) {
            $query->andWhere(
                'LOWER(o.description) LIKE LOWER(:search) OR '
                . 'LOWER(o.title) LIKE LOWER(:search) OR '
                . 'LOWER(o.reference) LIKE LOWER(:search)',
            )
                ->setParameter('search', '%' . $adminSearchOfferData->q . '%');
        }

        if (!empty($adminSearchOfferData->tags)) {
            $query->leftJoin('o.tags', 't')
                ->andWhere('t.id IN (:tags)')
                ->setParameter('tags', $adminSearchOfferData->tags);
        }

        if (!empty($adminSearchOfferData->department)) {
            $query->andWhere('d.code = :department')
                ->setParameter('department', $adminSearchOfferData->department);
        }

        if (!empty($adminSearchOfferData->contract)) {
            $query->andWhere('LOWER(o.contract) = LOWER(:contract)')
                ->setParameter('contract', $adminSearchOfferData->contract);
        }

        if (!is_null($adminSearchOfferData->active)) {
            $query->andWhere('o.active = :active')
                ->setParameter('active', $adminSearchOfferData->active);
        }

        if (!is_null($adminSearchOfferData->validation)) {
            $query->andWhere('o.validation = :validation')
                ->setParameter('validation', $adminSearchOfferData->validation);
        }

        $data = $query->getQuery()->getResult();

        $offers = $this->paginatorInterface->paginate(
            $data,
            $adminSearchOfferData->page,
            $this->parameterRepository->getValue(ParameterConstants::OFFERS_COUNT),
        );

        return $offers;
    }

    /**
     * Function to search tag in title or description in offers if tag is empty.
     */
    public function searchTagInOneOffer(int $id, string $tag): bool
    {
        $query = $this->createQueryBuilder('o')
            ->where('o.id = :id')
            ->andWhere('LOWER(o.title) LIKE LOWER(:tag) OR LOWER(o.description) LIKE LOWER(:tag)')
            ->setParameter('id', $id)
            ->setParameter('tag', '%' . $tag . '%')
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $query) {
            return false;
        }

        return true;
    }

    /**
     * Function to find inactive offers without offer candidacy.
     */
    public function findInactiveOffersWithoutOfferCandidacy(): array
    {
        return $this->createQueryBuilder('o')
            ->select('o.id, o.title, o.createdAt, o.active')
            ->where('o.active = :active')
            ->setParameter('active', false)
            ->andWhere('o.offerCandidacy IS EMPTY')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to delete one offer.
     */
    public function delete(int $id): void
    {
        $offer = $this->find($id);
        $this->_em->remove($offer);
        $this->_em->flush();
    }

    /**
     * Function to delete all offers.
     */
    public function deleteAllOffers(): void
    {
        $this->createQueryBuilder('o')
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Function to get offers without tags and is validation.
     */
    public function getOffersWithoutTagsAndIsValidation(): array
    {
        return $this->createQueryBuilder('o')
            ->where('o.validation = :validation')
            ->leftJoin('o.tags', 't')
            ->andWhere('t.id IS NULL')
            ->setParameter('validation', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get one offer without tags and is validation.
     */
    public function firstgetOfferWithoutTagsAndIsValidation(): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->where('o.validation = :validation')
            ->leftJoin('o.tags', 't')
            ->andWhere('t.id IS NULL')
            ->setParameter('validation', true)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to count offers without tags.
     */
    public function countOffersWithoutTags(): int
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->leftJoin('o.tags', 't')
            ->where('o.validation = :validation')
            ->andWhere('t.id IS NULL')
            ->setParameter('validation', true)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get offers without city.
     */
    public function findOneOfferBySlug(string $slug): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->where('o.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllOffersByTag(string $tag, int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('o')
            ->leftJoin('o.tags', 't')
            ->where('t.name = :tag')
            ->setParameter('tag', $tag)
            ->orderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        $offers = $this->paginatorInterface->paginate(
            $data,
            $page,
            $this->parameterRepository->getValue(ParameterConstants::OFFERS_COUNT),
        );

        return $offers;
    }

    public function findAllOffersByPartner(string $partner, int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('o')
            ->leftJoin('o.partners', 'p')
            ->where('p.slug = :partner')
            ->setParameter('partner', $partner)
            ->orderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        $offers = $this->paginatorInterface->paginate(
            $data,
            $page,
            $this->parameterRepository->getValue(ParameterConstants::OFFERS_COUNT),
        );

        return $offers;
    }

    /**
     * Function to find offer by reference.
     */
    public function findOfferByref(string $ref): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->where('o.reference = :ref')
            ->setParameter('ref', $ref)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to save offer.
     */
    public function save(OfferDTO $offer): void
    {
        $this->_em->persist($offer);
        $this->_em->flush();
    }
}
