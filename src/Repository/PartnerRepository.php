<?php

namespace App\Repository;

use App\Constant\ParameterConstants;
use App\Entity\Partner;
use App\Model\AdminSearchPartnerData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Partner>
 *
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
        private readonly ParameterRepository $parameterRepository,
    ) {
        parent::__construct($registry, Partner::class);
    }

    /**
     * Function to count total partners.
     */
    public function countTotalPartners(): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get all partners.
     *
     * @return PaginationInterface<Partner>
     */
    public function findAllPartners(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('p')
            ->orderBy('p.countOffer', 'DESC')
            ->getQuery()
            ->getResult();

        $partners = $this->paginatorInterface->paginate(
            $data,
            $page,
            $this->parameterRepository->getValue(ParameterConstants::PARTNERS_COUNT),
        );

        return $partners;
    }

    /**
     * Function to get partners by active status order by position.
     */
    public function getSelectionActivePartnersByCount(): PaginationInterface
    {
        $pagination = $this->parameterRepository->getValue(ParameterConstants::HOMEPAGE_PARTNERS_COUNT);

        $datas = $this->createQueryBuilder('p')
            ->where('p.active = :active')
            ->setParameter('active', true)
            ->orderBy('p.countOffer', 'DESC')
            ->setMaxResults($pagination)
            ->getQuery()
            ->getResult();

        $partners = $this->paginatorInterface->paginate(
            $datas,
            1,
            $pagination,
        );

        return $partners;
    }

    /**
     * Function to search partner.
     *
     * @return PaginationInterface<Partner>
     */
    public function findBySearch(AdminSearchPartnerData $adminSearchPartnerData): PaginationInterface
    {
        $datas = $this->createQueryBuilder('p')
            ->orderBy('p.position', 'ASC');

        if ($adminSearchPartnerData->q) {
            $datas = $datas
                ->andWhere('LOWER(p.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchPartnerData->q . '%');
        }

        $datas = $datas
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $datas,
            $adminSearchPartnerData->page,
            $this->parameterRepository->getValue(ParameterConstants::HOMEPAGE_PARTNERS_COUNT),
        );

        return $tags;
    }

    /**
     * Function to get all partners.
     */
    public function getPartners(): array
    {
        $partners = $this->createQueryBuilder('p')
            ->getQuery()
            ->getResult();

        return $partners;
    }

    /**
     * Function to get all positions.
     */
    public function getAllPosition(): array
    {
        $positions = [];
        for ($i = 1; $i <= $this->countPosition(); ++$i) {
            $positions[$i] = $i;
        }

        return $positions;
    }

    /**
     * Function to count position.
     */
    public function countPosition(): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get partners without URL.
     */
    public function getPartnersWithoutUrl(): array
    {
        $partners = $this->createQueryBuilder('p')
            ->where('p.url IS NULL')
            ->getQuery()
            ->getResult();

        return $partners;
    }

    /**
     * Function to get first partner without URL.
     */
    public function getFirstPartnerWithoutUrl(): ?Partner
    {
        return $this->createQueryBuilder('p')
            ->where('p.url IS NULL')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to count partners without URL.
     */
    public function countPartnersWithoutUrl(): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.url IS NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to search if partner exist.
     */
    public function searchIfPartnerExist(string $name): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('LOWER(p.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get active partners.
     */
    public function getActivePartners(): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = :active')
            ->setParameter('active', true)
            ->orderBy('p.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function countTotalOffersByPartner(string $name): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(o.id)')
            ->leftJoin('p.offers', 'o')
            ->where('p.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to find partner by name.
     */
    public function findPartnerByName(string $name): ?Partner
    {
        return $this->createQueryBuilder('p')
            ->where('p.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to get max position.
     */
    public function findMaxPosition(): ?int
    {
        $maxPosition = $this->createQueryBuilder('p')
            ->select('MAX(p.position)')
            ->getQuery()
            ->getSingleScalarResult();

        return $maxPosition ? (int) $maxPosition : 0;
    }
}
