<?php

namespace App\Repository;

use App\Constant\PaginationConstants;
use App\Constant\ParameterConstants;
use App\Entity\Parameter;
use App\Model\AdminSearchParameterData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Parameter>
 *
 * @method Parameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Parameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Parameter[]    findAll()
 * @method Parameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParameterRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Parameter::class);
    }

    public function findBySearch(
        AdminSearchParameterData $adminSearchTagData,
        int $number = PaginationConstants::TWENTY_FOUR,
    ): PaginationInterface {
        $data = $this->createQueryBuilder('p')
            ->orderBy('p.section', 'ASC')
            ->addOrderBy('p.type', 'ASC')
            ->addOrderBy('p.key', 'ASC');

        if ($adminSearchTagData->q) {
            $data = $data
                ->andWhere('LOWER(p.key) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchTagData->q . '%');
        }

        $data = $data
            ->getQuery()
            ->getResult();

        $parameters = $this->paginatorInterface->paginate(
            $data,
            $adminSearchTagData->page,
            $number,
        );

        return $parameters;
    }

    /**
     * Function to get all parameters.
     *
     * @return PaginationInterface<Parameter>
     */
    public function findAllParameters(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('p')
            ->orderBy('p.section', 'ASC')
            ->addOrderBy('p.type', 'ASC')
            ->addOrderBy('p.key', 'ASC')
            ->getQuery()
            ->getResult();

        $parameters = $this->paginatorInterface->paginate(
            $data,
            $page,
            $this->getValue(ParameterConstants::PARAMETER_COUNT),
        );

        return $parameters;
    }

    /**
     * Function to get parameter value.
     */
    public function getValue(string $key): int|string|bool|null
    {
        $parameter = $this->findOneBy(['key' => $key]);

        return $parameter ? $parameter->getValue() : ParameterConstants::PAGINATION_DEFAULT;
    }

    /**
     * Function to get pagination default.
     */
    public function getPaginationDefault(): int
    {
        return $this->getValue(ParameterConstants::PAGINATION_DEFAULT) ?? 24;
    }

    /**
     * Function to count parameter by type.
     */
    public function countParameterByType(string $type): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to update value by key.
     */
    public function updateValueByKey(string $key, string $value): void
    {
        $this->createQueryBuilder('p')
            ->update()
            ->set('p.value', ':value')
            ->where('p.key = :key')
            ->setParameter('key', $key)
            ->setParameter('value', $value)
            ->getQuery()
            ->execute();
    }
}
