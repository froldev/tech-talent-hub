<?php

namespace App\Repository;

use App\Entity\Post;
use App\Model\AdminSearchPostData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Post::class);
    }

    /**
     * Function to count total posts.
     */
    public function countTotalPosts(?string $type = null): int
    {
        $datas = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)');

        if (null !== $type) {
            $datas->where('p.type = :type')
                ->setParameter('type', $type);
        }

        return $datas
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get all posts.
     *
     * @return PaginationInterface<Post>
     */
    public function findAllPosts(int $page, ?string $type = null, ?int $number = 20): PaginationInterface
    {
        $data = $this->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC');

        if (null !== $type) {
            $data->andWhere('p.type = :type')
                ->setParameter('type', $type);
        }

        $data = $data->getQuery()
            ->getResult();

        $posts = $this->paginatorInterface->paginate(
            $data,
            $page,
            $number,
        );

        return $posts;
    }

    public function findBySearch(AdminSearchPostData $adminSearchPostData): PaginationInterface
    {
        $data = $this->createQueryBuilder('p')
            ->orderBy('p.title', 'DESC');

        if (!empty($adminSearchPostData->q)) {
            $data = $data
                ->andWhere('LOWER(p.title) LIKE LOWER(:search) OR LOWER(p.website) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchPostData->q . '%');
        }

        if (!empty($adminSearchPostData->type)) {
            $data = $data
                ->andWhere('p.type = :type')
                ->setParameter('type', $adminSearchPostData->type);
        }

        $posts = $this->paginatorInterface->paginate(
            $data,
            $adminSearchPostData->page,
            20,
        );

        return $posts;
    }
}
