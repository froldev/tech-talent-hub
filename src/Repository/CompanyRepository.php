<?php

namespace App\Repository;

use App\Entity\Company;
use App\Model\AdminSearchCompanyData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Company>
 *
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Company::class);
    }

    /**
     * Function to count total companies.
     */
    public function countTotalCompanies(): int
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to find all companies.
     *
     * @return PaginationInterface<Company>
     */
    public function findAllCompanies(int $page = 1): PaginationInterface
    {
        $data = $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();

        $users = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $users;
    }

    /**
     * Function to find companies by search.
     *
     * @return PaginationInterface<Company>
     */
    public function findBySearch(AdminSearchCompanyData $adminSearchCompanyData): PaginationInterface
    {
        $query = $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC');

        if (!empty($adminSearchCompanyData->q)) {
            $query->andWhere('LOWER(c.name) LIKE LOWER(:search)')
                ->orWhere('LOWER(c.activitySector) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchCompanyData->q . '%');
        }

        $data = $query->getQuery()->getResult();

        return $this->paginatorInterface->paginate(
            $data,
            $adminSearchCompanyData->page,
            20,
        );
    }

    /**
     * Function to find one company by name.
     */
    public function findOneCompanyByName(string $name): ?Company
    {
        return $this->createQueryBuilder('c')
            ->where('c.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
