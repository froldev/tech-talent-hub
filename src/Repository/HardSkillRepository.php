<?php

namespace App\Repository;

use App\Entity\HardSkill;
use App\Model\AdminSearchHardSkillData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<HardSkill>
 *
 * @method HardSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardSkill[]    findAll()
 * @method HardSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardSkillRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, HardSkill::class);
    }

    /**
     * Function to find all hard skills.
     *
     * @return PaginationInterface<HardSkill>
     */
    public function findAllHardSkills(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('h')
            ->orderBy('h.name', 'ASC')
            ->getQuery()
            ->getResult();

        $hardSkills = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $hardSkills;
    }

    /**
     * Function to search tags.
     *
     * @return PaginationInterface<HardSkill>
     */
    public function findBySearch(AdminSearchHardSkillData $adminSearchHardSkillData): PaginationInterface
    {
        $data = $this->createQueryBuilder('h')
            ->orderBy('h.name', 'ASC');

        if ($adminSearchHardSkillData->q) {
            $data = $data
                ->andWhere('LOWER(h.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchHardSkillData->q . '%');
        }

        $data = $data->getQuery()->getResult();

        $users = $this->paginatorInterface->paginate(
            $data,
            $adminSearchHardSkillData->page,
            20,
        );

        return $users;
    }

    /**
     * Function to find hard skill by name.
     */
    public function findHardSkillByName(string $name): ?HardSkill
    {
        return $this->createQueryBuilder('h')
            ->where('h.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
