<?php

namespace App\Repository;

use App\Entity\SoftSkill;
use App\Model\AdminSearchSoftSkillData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<SoftSkill>
 *
 * @method SoftSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method SoftSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method SoftSkill[]    findAll()
 * @method SoftSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoftSkillRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, SoftSkill::class);
    }

    /**
     * Function to find all softskills.
     *
     * @return PaginationInterface<SoftSkill>
     */
    public function findAllSoftSkills(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('s')
            ->orderBy('s.name', 'ASC')
            ->getQuery()
            ->getResult();

        $softSkills = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $softSkills;
    }

    /**
     * Function to search softskills.
     *
     * @return PaginationInterface<SoftSkill>
     */
    public function findBySearch(AdminSearchSoftSkillData $adminSearchSoftSkillData): PaginationInterface
    {
        $data = $this->createQueryBuilder('h')
            ->orderBy('h.name', 'ASC');

        if ($adminSearchSoftSkillData->q) {
            $data = $data
                ->andWhere('LOWER(h.name) LIKE LOWER(:search)')
                ->orWhere('LOWER(h.description) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchSoftSkillData->q . '%');
        }

        $data = $data->getQuery()->getResult();

        $users = $this->paginatorInterface->paginate(
            $data,
            $adminSearchSoftSkillData->page,
            20,
        );

        return $users;
    }

    /**
     * Function to find soft skill by name.
     */
    public function findSoftSkillByName(string $name): ?SoftSkill
    {
        return $this->createQueryBuilder('s')
            ->where('s.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
