<?php

namespace App\Repository;

use App\Entity\City;
use App\Model\AdminSearchCityData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<City>
 *
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, City::class);
    }

    /**
     * Function to count total cities.
     */
    public function countTotalCities(): int
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get all cities.
     *
     * @return PaginationInterface<City>
     */
    public function findAllCities(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('c')
            ->orderBy('c.label', 'ASC')
            ->getQuery()
            ->getResult();

        $cities = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $cities;
    }

    public function findBySearch(AdminSearchCityData $adminSearchCityData): PaginationInterface
    {
        $query = $this->createQueryBuilder('c')
            ->orderBy('c.label', 'ASC');

        if (!empty($adminSearchCityData->q)) {
            $query->andWhere('LOWER(c.label) LIKE LOWER(:q)')
                ->setParameter('q', '%' . $adminSearchCityData->q . '%');
        }

        if (!empty($adminSearchCityData->department)) {
            $query->andWhere('c.departmentCode LIKE :department')
                ->setParameter('department', '%' . $adminSearchCityData->department . '%');
        }

        $data = $query->getQuery()->getResult();

        $cities = $this->paginatorInterface->paginate(
            $data,
            $adminSearchCityData->page,
            20,
        );

        return $cities;
    }

    /**
     * Function to get all departments.
     */
    public function getDepartments(): array
    {
        return $this->createQueryBuilder('c')
            ->select('c.department, c.departmentCode AS department_code, c.region, c.regionCode AS region_code')
            ->distinct()
            ->orderBy('c.departmentCode', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get all cities.
     */
    public function getAllCities(): array
    {
        return $this->createQueryBuilder('c')
            ->select('c.departmentCode AS department_code, c.department, c.regionCode AS region_code, c.region')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find city by zip code and name.
     */
    public function findCityByZipCode(string $zipCode): ?City
    {
        return $this->createQueryBuilder('c')
            ->where('c.zipCode = :zipCode')
            ->setParameter('zipCode', $zipCode)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to find city by name.
     */
    public function findCityByName(string $name): ?City
    {
        return $this->createQueryBuilder('c')
            ->where('LOWER(c.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to find city by label.
     */
    public function findCityByLabel(string $label): ?City
    {
        return $this->createQueryBuilder('c')
            ->where('LOWER(c.label) = LOWER(:label)')
            ->setParameter('label', $label)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findCityByNameAndZipCode(string $name, string $zipCode): ?City
    {
        return $this->createQueryBuilder('c')
            ->where('LOWER(c.name) = LOWER(:name) AND c.zipCode = :zipCode')
            ->setParameter('name', $name)
            ->setParameter('zipCode', $zipCode)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
