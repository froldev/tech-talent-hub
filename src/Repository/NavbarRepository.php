<?php

namespace App\Repository;

use App\Entity\Navbar;
use App\Model\AdminSearchNavbarData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Navbar>
 *
 * @method Navbar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Navbar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Navbar[]    findAll()
 * @method Navbar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NavbarRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Navbar::class);
    }

    /**
     * Function to find all navbars.
     *
     * @return PaginationInterface<Navbar>
     */
    public function findAllNavbars(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('n')
            ->orderBy('n.position', 'ASC')
            ->getQuery()
            ->getResult();

        $navbars = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $navbars;
    }

    /**
     * @return array<int>
     */
    public function getPositions(): array
    {
        $positions = $this->createQueryBuilder('n')
            ->select('COUNT(n.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $positions = array_map(fn ($position) => $position, range(0, $positions));

        return $positions;
    }

    /**
     * Function to search navbars.
     *
     * @return PaginationInterface<Navbar>
     */
    public function findBySearch(AdminSearchNavbarData $adminSearchNavbarData): PaginationInterface
    {
        $query = $this->createQueryBuilder('n');

        if ($adminSearchNavbarData->q) {
            $query
                ->andWhere('LOWER(n.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchNavbarData->q . '%');
        }

        $data = $query
            ->orderBy('n.position', 'ASC')
            ->getQuery()
            ->getResult();

        $navbars = $this->paginatorInterface->paginate(
            $data,
            $adminSearchNavbarData->page,
            20,
        );

        return $navbars;
    }
}
