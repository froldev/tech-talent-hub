<?php

namespace App\Repository;

use App\Entity\OfferCandidacy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OfferCandidacy>
 *
 * @method OfferCandidacy|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferCandidacy|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferCandidacy[]    findAll()
 * @method OfferCandidacy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferCandidacyRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, OfferCandidacy::class);
    }
}
