<?php

namespace App\Repository;

use App\Entity\Language;
use App\Model\AdminSearchLanguageData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Language>
 *
 * @method Language|null find($id, $lockMode = null, $lockVersion = null)
 * @method Language|null findOneBy(array $criteria, array $orderBy = null)
 * @method Language[]    findAll()
 * @method Language[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LanguageRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Language::class);
    }

    /**
     * Function to find all languages.
     *
     * @return PaginationInterface<Language>
     */
    public function findAllLanguages(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('l')
            ->orderBy('l.name', 'ASC')
            ->getQuery()
            ->getResult();

        $languages = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $languages;
    }

    /**
     * Function to search languages.
     *
     * @return PaginationInterface<Language>
     */
    public function findBySearch(AdminSearchLanguageData $AdminSearchLanguageData): PaginationInterface
    {
        $query = $this->createQueryBuilder('l');

        if ($AdminSearchLanguageData->q) {
            $query->andWhere('LOWER(l.name) LIKE LOWER(:name)')
                ->setParameter('name', '%' . $AdminSearchLanguageData->q . '%');
        }

        $query = $query->orderBy('l.name', 'ASC')->getQuery();

        $languages = $this->paginatorInterface->paginate(
            $query,
            $AdminSearchLanguageData->page,
            20,
        );

        return $languages;
    }

    /**
     * Function to get language by name.
     */
    public function findLanguageByName(string $name): ?Language
    {
        return $this->createQueryBuilder('l')
            ->where('l.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
