<?php

namespace App\Repository;

use App\Constant\UserConstants;
use App\Entity\User;
use App\Model\AdminSearchUserData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    /**
     * Function to count total users.
     */
    public function countTotalUsers(): int
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get all users.
     *
     * @return PaginationInterface<User>
     */
    public function findAllUsers(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('u')
            ->orderBy('u.fullname', 'ASC')
            ->getQuery()
            ->getResult();

        $users = $this->paginatorInterface->paginate(
            $data,
            $page,
            20,
        );

        return $users;
    }

    /**
     * Function to search tags.
     *
     * @return PaginationInterface<User>
     */
    public function findBySearch(AdminSearchUserData $adminSearchUserData): PaginationInterface
    {
        $data = $this->createQueryBuilder('u')
            ->orderBy('u.fullname', 'ASC');

        if ($adminSearchUserData->q) {
            $data = $data
                ->andWhere('LOWER(u.fullname) LIKE LOWER(:search)')
                ->orWhere('LOWER(u.email) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchUserData->q . '%');
        }

        $data = $data->getQuery()->getResult();

        $users = $this->paginatorInterface->paginate(
            $data,
            $adminSearchUserData->page,
            20,
        );

        return $users;
    }

    /**
     * Function to search admin.
     */
    public function searchAdmin(): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', UserConstants::ADMIN_EMAIL)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to count total candidacies.
     */
    public function countOfferCandidacy(int $userId): int
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(c.id)')
            ->leftJoin('u.offerCandidacy', 'c')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to count total likes.
     */
    public function countOfferLikes(int $userId): int
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(o.id)')
            ->leftJoin('u.offerLikes', 'o')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to count total searches.
     */
    public function countOfferSearch(int $userId): int
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(s.id)')
            ->leftJoin('u.search', 's')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get offer candidacies.
     */
    public function getOfferCandidacies(int $userId): array
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.offerCandidacy', 'c')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get offer likes.
     */
    public function getOfferLikes(int $userId): array
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.offerLikes', 'o')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get offer searches.
     */
    public function getOfferSearch(int $userId): array
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.search', 's')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }
}
