<?php

namespace App\Repository;

use App\Entity\OfferLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OfferLike>
 *
 * @method OfferLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferLike[]    findAll()
 * @method OfferLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferLikeRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, OfferLike::class);
    }
}
