<?php

namespace App\Repository;

use App\Constant\ParameterConstants;
use App\Entity\Tag;
use App\Model\AdminSearchTagData;
use App\Model\AdminSearchTagTypeData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Tag>
 *
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
        private readonly ParameterRepository $parameterRepository,
    ) {
        parent::__construct($registry, Tag::class);
    }

    /**
     * Function to count total tags.
     */
    public function countTotalTags(): int
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get offers by tag.
     *
     * @return array<Tag>
     */
    public function getOffersByTag(): array
    {
        return $this->createQueryBuilder('t')
            ->leftJoin('t.offer', 'o')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find all children tags.
     *
     * @return PaginationInterface<Tag>
     */
    public function findAllChildrenTags(int $page): PaginationInterface
    {
        $data = $this->createQueryBuilder('t')
            ->where('t.parent IS NOT NULL')
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $data,
            $page,
            $this->parameterRepository->getValue(ParameterConstants::TAGS_COUNT),
        );

        return $tags;
    }

    /**
     * Function to search children tags.
     *
     * @return PaginationInterface<Tag>
     */
    public function findBySearch(AdminSearchTagData $adminSearchTagData): PaginationInterface
    {
        $data = $this->createQueryBuilder('t')
            ->leftJoin('t.parent', 'p')
            ->where('t.parent IS NOT NULL')
            ->addOrderBy('t.name', 'ASC')
            ->addOrderBy('p.id', 'ASC');

        if ($adminSearchTagData->q) {
            $data = $data
                ->andWhere('LOWER(t.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchTagData->q . '%');
        }

        if ($adminSearchTagData->parent) {
            $data = $data
                ->andWhere('p.name = :parent')
                ->setParameter('parent', $adminSearchTagData->parent);
        }

        if (null !== $adminSearchTagData->active) {
            $data = $data
                ->andWhere('t.active = :active')
                ->setParameter('active', $adminSearchTagData->active);
        }

        $data = $data
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $data,
            $adminSearchTagData->page,
            $this->parameterRepository->getValue(ParameterConstants::TAGS_COUNT),
        );

        return $tags;
    }

    /**
     * Function to search parent tags.
     *
     * @return PaginationInterface<Tag>
     */
    public function findParameterBySearch(AdminSearchTagTypeData $adminSearchTagTypeData): PaginationInterface
    {
        $data = $this->createQueryBuilder('t')
            ->leftJoin('t.parent', 'p')
            ->where('t.parent IS NULL')
            ->orderBy('t.name', 'ASC');

        if ($adminSearchTagTypeData->q) {
            $data = $data
                ->andWhere('LOWER(t.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchTagTypeData->q . '%');
        }

        $data = $data
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $data,
            $adminSearchTagTypeData->page,
            $this->parameterRepository->getValue(ParameterConstants::TAGS_COUNT),
        );

        return $tags;
    }

    /**
     * Function to get all active tags.
     *
     * @return PaginationInterface<Tag>
     */
    public function getSelectionActiveTagsByCount(): PaginationInterface
    {
        $pagination = $this->parameterRepository->getValue(ParameterConstants::HOMEPAGE_TAGS_COUNT);

        $data = $this->createQueryBuilder('t')
            ->where('t.active = :active')
            ->setParameter('active', true)
            ->orderBy('t.countOffer', 'DESC')
            ->setMaxResults($pagination)
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $data,
            1,
            $pagination,
        );

        return $tags;
    }

    /**
     * Function to get all active parent tags.
     *
     * @return array<Tag>
     */
    public function getParentChoices(): array
    {
        $tags    = $this->findBy(['parent' => null, 'active' => true], ['id' => 'ASC']);
        $choices = [];

        foreach ($tags as $tag) {
            $choices[$tag->getName()] = $tag;
        }

        return $choices;
    }

    public function getParent(): PaginationInterface
    {
        $pagination = $this->parameterRepository->getValue(ParameterConstants::HOMEPAGE_TAGS_COUNT);

        $data = $this->createQueryBuilder('t')
            ->where('t.parent IS NULL')
            ->getQuery()
            ->getResult();

        $tags = $this->paginatorInterface->paginate(
            $data,
            1,
            $pagination,
        );

        return $tags;
    }

    /**
     * Function to find all active child tags.
     */
    public function findAllChildTags(): array
    {
        return $this->createQueryBuilder('t')
            ->where('t.active = true')
            ->andWhere('t.parent IS NOT NULL')
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find one tag by name.
     */
    public function findTagByName(string $name): ?Tag
    {
        $tag = $this->createQueryBuilder('t')
            ->where('LOWER(t.name) = LOWER(:name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();

        return $tag;
    }

    public function findChildTagByName(string $name): ?Tag
    {
        $tag = $this->createQueryBuilder('t')
            ->where('LOWER(t.name) = LOWER(:name)')
            ->andWhere('t.parent IS NOT NULL')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();

        return $tag;
    }

    /**
     * Function to find all active child tags.
     */
    public function findAllChildActiveTagsQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->where('t.active = true')
            ->andWhere('t.parent IS NOT NULL')
            ->orderBy('t.name', 'ASC');
    }
}
