<?php

namespace App\Repository;

use App\Constant\JobConstants;
use App\Constant\TagConstants;
use App\Entity\Job;
use App\Model\AdminSearchJobData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Job>
 *
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
    ) {
        parent::__construct($registry, Job::class);
    }

    /**
     * Function to count total jobs.
     */
    public function countTotalJobs(): int
    {
        return $this->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to get all jobs.
     *
     * @return PaginationInterface<Job>
     */
    public function findAllJobs(int $page, int $number = 20): PaginationInterface
    {
        $data = $this->createQueryBuilder('j')
            ->where('j.parent IS NOT NULL')
            ->orderBy('j.parent', 'DESC')
            ->getQuery()
            ->getResult();

        $jobs = $this->paginatorInterface->paginate(
            $data,
            $page,
            $number,
        );

        return $jobs;
    }

    /**
     * Function to search jobs.
     *
     * @return PaginationInterface<Job>
     */
    public function findBySearch(AdminSearchJobData $adminSearchJobData): PaginationInterface
    {
        $data = $this->createQueryBuilder('j')
            ->where('j.parent IS NOT NULL')
            ->orderBy('j.parent', 'DESC');

        if (!empty($adminSearchJobData->q)) {
            $data = $data
                ->andWhere('LOWER(j.name) LIKE LOWER(:search)')
                ->setParameter('search', '%' . $adminSearchJobData->q . '%');
        }

        if (!empty($adminSearchJobData->parent)) {
            $data = $data
                ->leftJoin('j.parent', 'p')
                ->andWhere('j.parent IS NOT NULL')
                ->andWhere('LOWER(p.name) = LOWER(:search)')
                ->setParameter('search', $adminSearchJobData->parent);
        }

        if (!is_null($adminSearchJobData->active)) {
            $data = $data
                ->andWhere('j.active = :active')
                ->setParameter('active', $adminSearchJobData->active);
        }

        $data = $data
            ->getQuery()
            ->getResult();

        $jobs = $this->paginatorInterface->paginate(
            $data,
            $adminSearchJobData->page,
            20,
        );

        return $jobs;
    }

    public function getParentsWithoutToBeDefinedOrToExclude(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent IS NULL')
            ->andWhere('j.name NOT IN (:excluded)')
            ->setParameter('excluded', ['A définir'])
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to get job parent.
     *
     * @return array<Job>
     */
    public function getJobParent(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent IS NULL')
            ->orderBy('j.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find children.
     *
     * @return array<Job>
     */
    public function findChildren(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent IS NOT NULL')
            ->orderBy('j.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find jobs active.
     *
     * @return array<Job>
     */
    public function findJobsActive(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.active = true')
            ->andWhere('j.parent IS NOT NULL')
            ->orderBy('j.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find jobs active.
     *
     * @return array<Job>
     */
    public function findActiveChildren(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent IS NOT NULL')
            ->andWhere('j.active = true')
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find jobs active.
     *
     * @return array<Job>
     */
    public function findAllJobAboutParent(Job $parent): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent = :parent')
            ->setParameter('parent', $parent)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find the first job about parent.
     */
    public function findTheFirstJobAboutParent(Job $parent): ?Job
    {
        return $this->createQueryBuilder('j')
            ->where('j.parent = :parent')
            ->setParameter('parent', $parent)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Function to count job to define.
     */
    public function countJobWithParentIsToDefine(): int
    {
        $parentToDefine = $this->findOneBy(['name' => JobConstants::TO_DEFINE]);

        return $this->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.parent IS NOT NULL')
            ->andWhere('j.parent = :parent')
            ->setParameter('parent', $parentToDefine)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to count active jobs.
     */
    public function countActiveJobs(): int
    {
        return $this->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.active = true')
            ->andWhere('j.parent IS NOT NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Function to find active children with parent is not tag exclude.
     */
    public function findActiveChildrenWithParentIsNotTagExclude(): array
    {
        return $this->createQueryBuilder('j')
            ->leftJoin('j.parent', 'p')
            ->where('p.name != :name OR p IS NULL')
            ->setParameter('name', TagConstants::EXCLUDE)
            ->getQuery()
            ->getResult();
    }

    /**
     * Function to find job by name.
     */
    public function findJobByName(string $name): ?Job
    {
        return $this->createQueryBuilder('j')
            ->where('j.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
