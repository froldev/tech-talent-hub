<?php

namespace App\Components;

use App\Entity\Parameter;
use App\Form\AdminParameterType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

class AdminParameterComponent
{
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public ?Parameter $parameter = null;

    private FormFactoryInterface $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function getForm(): FormInterface
    {
        return $this->formFactory->create(AdminParameterType::class, $this->parameter);
    }
}
