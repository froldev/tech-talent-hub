<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\PostConstants;

class PostChoice
{
    public function getListOfTypes(): array
    {
        return [
            PostConstants::SALARY => PostConstants::SALARY,
            PostConstants::TEST   => PostConstants::TEST,
        ];
    }
}
