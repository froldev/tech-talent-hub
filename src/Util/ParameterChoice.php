<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\ParameterConstants;

class ParameterChoice
{
    public static function getListOfParameters(): array
    {
        return [
            ParameterConstants::PARAMETER_STRING   => ParameterConstants::PARAMETER_STRING,
            ParameterConstants::PARAMETER_BOOLEAN  => ParameterConstants::PARAMETER_BOOLEAN,
            ParameterConstants::PARAMETER_INTEGER  => ParameterConstants::PARAMETER_INTEGER,
            ParameterConstants::PARAMETER_DATETIME => ParameterConstants::PARAMETER_DATETIME,
            ParameterConstants::PARAMETER_TIME     => ParameterConstants::PARAMETER_TIME,
        ];
    }

    public static function getListOfSections(): array
    {
        return [
            ParameterConstants::SECTION_PAGINATION => ParameterConstants::SECTION_PAGINATION,
            ParameterConstants::SECTION_API        => ParameterConstants::SECTION_API,
            ParameterConstants::SECTION_SCHEDULER  => ParameterConstants::SECTION_SCHEDULER,
            ParameterConstants::SECTION_MODULE     => ParameterConstants::SECTION_MODULE,
        ];
    }
}
