<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\FranceTravailConstants;
use App\Constant\OfferConstants;

class OfferChoice
{
    public function getSelectChannels(): array
    {
        return [
            FranceTravailConstants::NAME          => FranceTravailConstants::NAME,
            OfferConstants::APEC                  => OfferConstants::APEC,
            OfferConstants::INDEED                => OfferConstants::INDEED,
            OfferConstants::MONSTER               => OfferConstants::MONSTER,
            OfferConstants::HELLOWORK             => OfferConstants::HELLOWORK,
            OfferConstants::METEOJOB              => OfferConstants::METEOJOB,
            OfferConstants::LINKEDIN              => OfferConstants::LINKEDIN,
            OfferConstants::WE_LOVE_DEVS          => OfferConstants::WE_LOVE_DEVS,
            OfferConstants::WELCOME_TO_THE_JUNGLE => OfferConstants::WELCOME_TO_THE_JUNGLE,
            OfferConstants::TELEPHONE             => OfferConstants::TELEPHONE,
            OfferConstants::RESEAU                => OfferConstants::RESEAU,
            OfferConstants::AUTRE                 => OfferConstants::AUTRE,
        ];
    }

    public function getSelectOfContracts(): array
    {
        return [
            'Contrat à durée indéterminée' => 'CDI',
            'Contrat à durée déterminée'   => 'CDD',
            'Profession libérale'          => 'LIB',
            'Franchise'                    => 'FRA',
            'Mission intérimaire'          => 'MIS',
        ];
    }
}
