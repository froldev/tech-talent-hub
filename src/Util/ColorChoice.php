<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\ColorConstants;

class ColorChoice
{
    public function getColors(): array
    {
        return [
            'Bleu'     => ColorConstants::BLUE,
            'Cyan'     => ColorConstants::CYAN,
            'Noir'     => ColorConstants::DARK,
            'Emeraude' => ColorConstants::EMERALD,
            'Vert'     => ColorConstants::GREEN,
            'Indigo'   => ColorConstants::INDIGO,
            'Clair'    => ColorConstants::LIGHT,
            'Orange'   => ColorConstants::ORANGE,
            'Violet'   => ColorConstants::PURPLE,
            'Rouge'    => ColorConstants::RED,
            'Gris'     => ColorConstants::GRAY,
            'Ciel'     => ColorConstants::SKY,
            'Jaune'    => ColorConstants::YELLOW,
        ];
    }
}
