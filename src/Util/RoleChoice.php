<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\RoleConstants;

class RoleChoice
{
    public function getRoles(): array
    {
        return RoleConstants::ROLES;
    }
}
