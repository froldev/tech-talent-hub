<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\PaginationConstants;

class PaginationChoice
{
    public function getListOfPagination(): array
    {
        return [
            PaginationConstants::ONE          => PaginationConstants::ONE,
            PaginationConstants::TWO          => PaginationConstants::TWO,
            PaginationConstants::THREE        => PaginationConstants::THREE,
            PaginationConstants::FOUR         => PaginationConstants::FOUR,
            PaginationConstants::FIVE         => PaginationConstants::FIVE,
            PaginationConstants::SIX          => PaginationConstants::SIX,
            PaginationConstants::SEVEN        => PaginationConstants::SEVEN,
            PaginationConstants::EIGHT        => PaginationConstants::EIGHT,
            PaginationConstants::NINE         => PaginationConstants::NINE,
            PaginationConstants::TEN          => PaginationConstants::TEN,
            PaginationConstants::TWELVE       => PaginationConstants::TWELVE,
            PaginationConstants::FOURTEEN     => PaginationConstants::FOURTEEN,
            PaginationConstants::SIXTEEN      => PaginationConstants::SIXTEEN,
            PaginationConstants::EIGHTEEN     => PaginationConstants::EIGHTEEN,
            PaginationConstants::TWENTY       => PaginationConstants::TWENTY,
            PaginationConstants::TWENTY_FOUR  => PaginationConstants::TWENTY_FOUR,
            PaginationConstants::FOURTY_EIGHT => PaginationConstants::FOURTY_EIGHT,
        ];
    }
}
