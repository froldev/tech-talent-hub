<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\JobConstants;

class JobChoice
{
    public function getListOfKeywordsMapping(): array
    {
        return [
            'analyste'       => JobConstants::DATA,
            'jeux vidéo'     => JobConstants::VIDEO_GAMES,
            'testeur'        => JobConstants::TESTER,
            'chef de projet' => JobConstants::PROJECT_MANAGER,
            'scrum master'   => JobConstants::AGILITY,
            'product owner'  => JobConstants::AGILITY,
            'développeur'    => JobConstants::DEVELOPER,
            'développement'  => JobConstants::DEVELOPER,
            'data'           => JobConstants::DATA,
            'homologateur'   => JobConstants::EXCLUDE,
        ];
    }

    public function getSelectOfParent(): array
    {
        return [
            JobConstants::DEVELOPER   => JobConstants::DEVELOPER,
            JobConstants::DATA        => JobConstants::DATA,
            JobConstants::VIDEO_GAMES => JobConstants::VIDEO_GAMES,
            JobConstants::TESTER      => JobConstants::TESTER,
            JobConstants::AGILITY     => JobConstants::AGILITY,
        ];
    }

    public function getSelectOfParentForAdmin(): array
    {
        return [
            JobConstants::TO_DEFINE       => JobConstants::TO_DEFINE,
            JobConstants::EXCLUDE         => JobConstants::EXCLUDE,
            JobConstants::DEVELOPER       => JobConstants::DEVELOPER,
            JobConstants::DATA            => JobConstants::DATA,
            JobConstants::VIDEO_GAMES     => JobConstants::VIDEO_GAMES,
            JobConstants::TESTER          => JobConstants::TESTER,
            JobConstants::AGILITY         => JobConstants::AGILITY,
            JobConstants::PROJECT_MANAGER => JobConstants::PROJECT_MANAGER,
        ];
    }

    public function getListOfParent(): array
    {
        return [
            JobConstants::DEVELOPER,
            JobConstants::DATA,
            JobConstants::VIDEO_GAMES,
            JobConstants::TESTER,
            JobConstants::AGILITY,
            JobConstants::PROJECT_MANAGER,
            JobConstants::TO_DEFINE,
            JobConstants::EXCLUDE,
        ];
    }

    public function getListOfChildren(): array
    {
        return [
            'Scrum Master',
            'Product Owner',
            'Chef de projet étude et développement informatique',
            'Chef de projet maîtrise d\'œuvre informatique',
            'Ingénieur(e) analyste-programmeur(se)',
            'Analyste décisionnel - Business Intelligence',
            'Analyste d\'étude informatique',
            'Analyste développeur / développeuse',
            'Webmaster développeur / développeuse',
            'Responsable d\'application informatique',
            'Programmeur / Programmeuse d\'applications',
            'Ingénieur informaticien / Ingénieure informaticienne',
            'Ingénieur / Ingénieure en développement d\'applications',
            'Ingénieur / Ingénieure développement logiciel informatique',
            'Ingénieur / Ingénieure de développement informatique',
            'Ingénieur / Ingénieure d\'étude informatique',
            'Ingénieur / Ingénieure d\'étude et développement informatique',
            'Informaticien / Informaticienne de développement',
            'Développeur / Développeuse web mobile',
            'Développeur / Développeuse web',
            'Développeur / Développeuse logiciel de réalité augmentée',
            'Développeur / Développeuse informatique',
            'Développeur / Développeuse full-stack',
            'Développeur / Développeuse front-end',
            'Développeur / Développeuse d\'application',
            'Développeur / Développeuse back-end',
            'Programmeur / Programmeuse - jeux vidéo',
            'Lead programmeur / programmeuse - jeux vidéo',
            'Testeur / Testeuse informatique',
        ];
    }
}
