<?php

declare(strict_types=1);

namespace App\Util;

use App\Constant\TypeConstants;

class TypeChoice
{
    public function getListOfCheckbox(): array
    {
        return [
            TypeConstants::ACTIVE   => TypeConstants::ACTIVE,
            TypeConstants::INACTIVE => TypeConstants::INACTIVE,
        ];
    }
}
